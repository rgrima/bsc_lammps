TRACE      ?= NO
EXT        ?= 
OMPSS_VERS ?=1

ifeq ($(CC),icc)
    $(info "**** INTEL compilation" )
    # IS INTEL
    MCXX         = imcxx
    CXX          = mpiicpc
    CXXFLAGS     = -g -O3
    #-k
    OMP_FLAGS    = -qopenmp
    #EFLAGS       = 
    EFLAGS       = -restrict
    PFLAGS       = 
    LDFLAGS      = 
    OMPSS_FLAGS += --cxx=mpiicpc --cpp=mpiicpc --ld=mpiicpc --Wn,-restrict
    # -k
    # --Wn,-xHOST,-no-prec-div,-restrict
    #,-fp-model fast=2
    KOKKOS_FLAGS = -qopenmp -Ikokkos
else ifeq ($(shell which mpiFCC 2> /dev/null > /dev/null && echo YES || echo NO),YES)
    $(info "**** Fujitsu compilation" )
    MCXX         = mcxx
    CXX          = mpiFCC
    #CXXFLAGS     = -g -Nclang -Nlibomp -O3 -ffast-math -ffj-lst=t -ffj-src
    CXXFLAGS     = -g -Nclang -Nlibomp -O3
    OMP_FLAGS    = -fopenmp
    EFLAGS       = 
    PFLAGS       = OMPI_CXX=${MCXX}
    LDFLAGS      = -ldl
    OMPSS_FLAGS += --cxx=mpiFCC --cpp=mpiFCC --ld=mpiFCC --Wn,-Nclang --Wp,-Nclang --Wl,-Nclang
    # -k
    KOKKOS_FLAGS = -fopenmp -Ikokkos -fpermissive
else
    $(info "**** GCC compilation" )
    MCXX         = mcxx
    CXX          = mpicxx
    CXXFLAGS     = -g -O3
    OMP_FLAGS    = -fopenmp
    EFLAGS       = 
    PFLAGS       = OMPI_CXX=${MCXX}
    LDFLAGS      = -ldl
    OMPSS_FLAGS += --cxx=mpicxx --cpp=mpicxx --ld=mpicxx
    # -k
    KOKKOS_FLAGS = -fopenmp -Ikokkos -fpermissive
endif

DEFINES     = -DLAMMPS_GZIP -DLAMMPS_MEMALIGN=4096 -DMPICH_SKIP_MPICXX -DOMPI_SKIP_MPICXX=1

INSTRU       = -finstrument-functions

OMPSS=$(shell which $(MCXX) 2>&1 > /dev/null > /dev/null && echo YES || echo NO)
ifneq ($(OMPSS),YES)
      $(info Cannot find mcxx)
      $(error ================)
endif

OMPSS1_FLAGS += $(OMPSS_FLAGS) --ompss --variable=commons_as_weaks:1 --no-copy-deps
OMPSS1_DEFINES=-D_OMPSS=1

OMPSS2_FLAGS += $(OMPSS_FLAGS) --ompss-2
OMPSS2_DEFINES=-D_OMPSS=2

OMPSS1_INSTRU += --instrument
