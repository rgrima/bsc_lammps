#!/bin/bash

NAME=$( basename $( pwd ) )
### mpi, mpo, omp2, omp ompss
MODES="mpi"
SCALE="1 4 6"
NTHREADS="1 4 6 8 12"
LPROCS="1 4 8 12 24 36 48 96 192 384 768 1536 2304 3072 3840 4608"
[ "${NAME}" == "chute" ] && SCALE="1 8 15"

TRACE=true

MODES="omp omp2 ompss"
SCALE="1"
[ "${NAME}" == "chute" ] && [ "${SCALE}" == "4" ] && SCALE="8"
NTHREADS="1 4"
LPROCS="48"

declare -A EXES=(
    ["mpi"]="lmp_omp"
    ["mpo"]="lmp_omp"
    ["omp2"]="lmp_omp_2"
    ["ompss"]="lmp_ompss_1"
    ["omp"]="lmp_omp" )
declare -A EXESPARS=(
    ["mpi"]=""
    ["mpo"]="-sf omp -pk omp \${nthr}"
    ["omp2"]="-pk ompss -sf ompss"
    ["ompss"]="-pk ompss -sf ompss"
    ["omp"]="-sf omp -pk omp \${nthr}" )

declare -A TIMES=(
    ["chain"]=2.13
    ["chute"]=1.22
    ["eam"]=3.23
    ["lj"]=2.98
    ["rhodo"]=12.64
    )

input_f=$( ls in.* )
data_f=""
[ -f data.* ] && data_f="data.*"
[ -f Cu_u3.eam ] && data_f="Cu_u3.eam"

topdir=$(realpath ../../)
QU="#!/bin/bash
#PJM -N lmp
#PJM -L rscgrp=def_grp
#PJM -L rscunit=rscunit_ft02
#PJM -L node=_NODES_
#PJM -L elapse=0:_MIN_:00
#PJM --mpi proc=_PROCS_
#PJM --no-stging
#PJM -j

#make -C ${topdir} _EXE_
EXTRAE_LOAD 5
#mpiexec run.sh
OMP_NUM_THREADS=_NTHREADS_ mpiexec _TRACE_ ${topdir}/_EXE_ _EXEPARS_ -i ${input_f}
"

RU="#!/bin/bash
OMP_NUM_THREADS=_NTHREADS_ _TRACE_ ${topdir}/_EXE_ _EXEPARS_ -i ${input_f}"

for mode in ${MODES}; do
    for scale in ${SCALE}; do
        [ ${NAME} == "chute" ] &&
            T="${TIMES[${NAME}]}*(${scale}^2)" ||
            T="${TIMES[${NAME}]}*(${scale}^3)"
        TimeF=$( echo $T | bc -l )
        for nthr in ${NTHREADS}; do
            if [[ "${mode}" == "mpi" || "${mode}" == "mpo" ]]; then
                [ ${nthr} -gt 1 ] && continue
            else
                [ "${mode}" != "omp" ] && [ ${nthr} -eq 1 ] && continue
            fi
            ${TRACE} && wdir=$( printf "PRV-${mode}-%d-%02d" ${scale} ${nthr} ) ||
                        wdir=$( printf "STR-${mode}-%d-%02d" ${scale} ${nthr} )
            echo $wdir
            [ -d ${wdir} ] && rm -rf ${wdir}; mkdir ${wdir}

            EXE=${EXES[${mode}]}
            [ ${mode} == "ompss" ] && EXE=${EXE}_prv
            #RUN="${RU/_EXE_/${EXE}}"
            #${TRACE} && RUN="${RUN/_TRACE_/./trace.sh}" || RUN="${RUN/_TRACE_/}"
            #RUN="${RUN/_NTHREADS_/${nthr}}"
            #PARS=$( eval "echo ${EXESPARS[${mode}]}" )
            #RUN="${RUN/_EXEPARS_/${PARS}}"
            for procs in ${LPROCS}; do
                [ "${scale}" -eq 1 ] && [ "${procs}" -gt 768 ] && continue
                [ "${procs}" -lt "${nthr}" ] && continue
                Eff="1-0.75*(${procs}-1)/4607"
                Min="(200+(${Eff})*${TimeF}/${procs})/60"
                Min=$( echo $Min | bc )
                ${TRACE} && Min=$(( Min+10 ))
                Min=$( printf "%02d" $Min )
                WDIR=$( printf "${wdir}/str-%04d" ${procs} )
                mkdir ${WDIR}
                cp ${input_f} ${data_f} trace.sh extrae.xml ${WDIR}
                pushd ${WDIR} &> /dev/null
                txt=$(<${input_f})
                echo "${txt}" | sed -e "s/index 1/index ${scale}/g" > ${input_f}
                QQ="${QU//_PROCS_/$(( procs/nthr ))}"
                QQ="${QQ//_MIN_/${Min}}"
                QQ="${QQ//_NODES_/$(( (procs+47)/48 ))}"
                QQ="${QQ//_EXE_/${EXE}}"
                ${TRACE} && QQ="${QQ//_TRACE_/./trace.sh}" || QQ="${QQ//_TRACE_/}"
                QQ="${QQ//_NTHREADS_/${nthr}}"
                PARS=$( eval "echo ${EXESPARS[${mode}]}" )
                QQ="${QQ//_EXEPARS_/${PARS}}"
                echo "${QQ}" > qu.sh && chmod +x qu.sh
                #echo "${RUN}" > run.sh && chmod +x run.sh
                while [ $( pjstat | wc -l ) -gt 20 ]; do sleep 2; printf "*"; done
                pjsub qu.sh
                popd &> /dev/null
            done
        done
    done
done
echo 'for f in STR-omp*/*/*out; do TIME=$( grep -i TIME_total $f | cut -d"=" -f 2 ); MEM=$( grep MEMO_total $f | rev | cut -d" " -f 2 | rev ) NAME=$( echo $f | cut -d"/" -f 2 | cut -d"-" -f 2 ); echo $NAME $TIME $MEM; done'
exit

for mode in ${MODES}; do
    for procs in ${LPROCS}; do
        [ "${procs}" -lt "${nthr}" ] && continue
        WDIR=$( printf "${wdir}/${mode}_%04d" ${procs} )
        mkdir ${WDIR}
        cp extrae.xml trace.sh ${input_f} ${data_f} ${WDIR}
        pushd ${WDIR} &> /dev/null
        txt=$(<${input_f})
        echo "${txt}" | sed -e "s/index 1/index ${scale}/g" > ${input_f}
        #export MEMAVAILABLE=$( grep MemAvailable /proc/meminfo | tr -s ' ' | cut -d ' ' -f 2 )
        #echo OMP_NUM_THREADS=$NT mpiexec $TRACE $EXE > outfile
        #OMP_NUM_THREADS=$NT mpiexec $TRACE $EXE 2>&1 | tee -a outfile
        QQ="${QU/_PROCS_/$(( procs/nthr ))}"
        QQ="${QQ/_NODES_/$(( (procs+47)/48 ))}"
        QQ="${QQ/_EXE_/${EXES[${mode}]}}"
        echo "${QQ}" > qu.sh && chmod +x qu.sh
        echo "${RUN}" > run.sh && chmod +x run.sh
        pjsub qu.sh
        popd &> /dev/null
    done
done

exit

#!/bin/bash
#PJM -N lmp
#PJM -L rscgrp=q48node
#PJM -L rscunit=rscunit_ft01
#PJM -L node=1
#PJM -L elapse=0:30:00
#PJM --mpi proc=4
#PJM --no-stging
#PJM -j

wdir=A64FX_prv
input_f="in.*"
data_f=""
[ -f data.* ] && data_f="data.*"
[ -f Cu_u3.eam ] && data_f="Cu_u3.eam"

NAME=$( basename $( pwd ) )

topdir=$(realpath ../../)
script=./lammps.sh

if false; then
    TEST="omp omp2 ompss_1 ompss_2"
else
    TEST="ompss_1"
fi

if false; then
    [ "${NAME}" == "chute" ] && SCALE="1 7 13 19" || SCALE="1 3 5 7"
else
    SCALE=4
    #[[ "${NAME}" == "chain" || "${NAME}" == "eam" ]] && SCALE=5
    [[ "${NAME}" == "eam" ]] && SCALE=5
    [ "${NAME}" == "chute" ] && SCALE=15
fi
true && TRACE="./trace.sh" || TRACE=""
false && export XOS_MMM_L_HPAGE_TYPE=disable

[ -d ${wdir} ] && rm -rf ${wdir}; mkdir ${wdir}

NT=$(( 48/PJM_PROC_BY_NODE ))
for t in ${TEST}; do
    [ "${t}" == "omp" ] && {
        make -j 48 -l 48 -C ../.. lmp_omp || exit
        EXE="${topdir}/lmp_omp -sf omp -pk omp ${NT} -i ${input_f}"; }
    [ "${t}" == "omp2" ] && {
        make -j 48 -l 48 -C ../.. lmp_omp_2 || exit
        EXE="${topdir}/lmp_omp_2 -pk ompss -sf ompss -i ${input_f}"; }
    [ "${t}" == "ompss_2" ] && {
        make -j 48 -l 48 OMPSS_VERS=2 -C ../.. lmp_ompss_2 || exit
        EXE="${topdir}/lmp_ompss_2 -pk ompss -sf ompss -i ${input_f}"; }
    [ "${t}" == "ompss_1" ] && {
        if [ -f ${TRACE} ]; then
            make -j 48 -l 48 TRACE=YES EXT=_prv OMPSS_VERS=1 -C ../.. lmp_ompss_1_prv || exit
            EXE="${topdir}/lmp_ompss_1_prv -pk ompss -sf ompss -i ${input_f}"
        else
            make -j 48 -l 48 -C OMPSS_VERS=1 ../.. lmp_ompss_1 || exit
            EXE="${topdir}/lmp_ompss_1_prv -pk ompss -sf ompss -i ${input_f}"
        fi; }

     for s in ${SCALE}; do
        cdir=$( printf "${wdir}/%d_${t}" $s )
        mkdir ${cdir}
        cp extrae.xml trace.sh ${input_f} ${data_f} ${cdir}
        pushd ${cdir} &> /dev/null
        txt=$(<${input_f})
        echo "${txt}" | sed -e "s/index 1/index ${s}/g" > ${input_f}
        export MEMAVAILABLE=$( grep MemAvailable /proc/meminfo | tr -s ' ' | cut -d ' ' -f 2 )
        echo OMP_NUM_THREADS=$NT mpiexec $TRACE $EXE > outfile
        OMP_NUM_THREADS=$NT mpiexec $TRACE $EXE 2>&1 | tee -a outfile
        popd &> /dev/null
    done
done

