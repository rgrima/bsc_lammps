#!/bin/bash

SCALES="1 2 6 8"
NODES="1 2 3 4 5 6 7 8"
MODES="OMPSS MPI"
NTHREAD=12
TAxNO=$(( 48/NTHREAD ))

wdir=Strong

input_f="in.chute.scaled"
data_f="data.chute"
topdir=$(realpath ../../)
basdir=$(realpath ./)
script=./lammps.sh

[ -d ${wdir} ] && rm -rf ${wdir}; mkdir ${wdir}

QUEUE="#!/bin/bash
#SBATCH --job-name=test_parallel
#SBATCH --workdir=.
#SBATCH --output=mpi_%j.out
#SBATCH --error=mpi_%j.err
#SBATCH --ntasks=_NNODES_
#SBATCH --cpus-per-task=48
#SBATCH --tasks-per-node=1
#SBATCH --time=00:20:00
#SBATCH --qos=debug


for SCALE in ${SCALES}; do
    for MODE in ${MODES}; do
        WDIR=\$( printf \"\${MODE}_%02d_%02d\" _NNODES_ \${SCALE} )
        mkdir \${WDIR}
        for f in ${input_f} ${data_f} extrae.xml; do
            cp ${basdir}/\${f} \${WDIR}
        done
        pushd \${WDIR} &> /dev/null
        txt=\$(<${input_f})
        echo \"\${txt}\" | sed -e \"s/index 1/index \${SCALE}/g\" > ${input_f}

        src=\"#!/bin/bash\n\"
        if [ \${MODE} == OMPSS ]; then
            src+=\"export OMP_NUM_THREADS=${NTHREAD}\n\"
            src+=\"${topdir}/lmp_ompss -pk ompss -sf ompss -i ${input_f}\n\"
            NTASK=\$(( _NNODES_*${NTHREAD} ))
            export SLURM_NPROCS=\"\${NTASKS}\"
            export SLURM_NTASKS=\"\${NTASKS}\"
            taxno=${TAxNO}
        else
            src+=\"unset OMP_NUM_THREADS\n\"
            src+=\"${topdir}/lmp_omp -i ${input_f}\n\"
            taxno=48
        fi
        echo -e \"\${src}\" > ${script}
        chmod +x ${script}
        [ _NNODES_ -eq 1 ] &&
            export SLURM_TASKS_PER_NODE=\"\${NTASKS}\" ||
            export SLURM_TASKS_PER_NODE=\"\${taxno}(x_NNODES_)\"
        for line in \$( scontrol show hostnames \${SLURM_JOB_NODELIST} ); do
            echo \"\${line}:\${taxno}\"
        done > hfile

        mpirun -machine hfile ${script}  2>&1 | tee output

        popd  &> /dev/null
    done
done
"
cd ${wdir}
for NODE in ${NODES}; do
    sc_queue=queue_${NODE}.sh
    echo "${QUEUE}"| sed -e "s/_NNODES_/${NODE}/g" > ${sc_queue}
    chmod +x ${sc_queue}
    sbatch < ${sc_queue}
done
