#!/bin/bash
COMPILE=true
SCALES="1 4 7"
NTHREADS="1 4 8 12 24"
MODES="OMPSS OMP"
# KOKKOS
input_f="in.rhodo.scaled"
data_f="data.rhodo"
wdir=TEST
IS_INTEL=true

NPROCS=6; [ $# -ge 1 ] && [ $1 -eq $1 ] &> /dev/null && [ $1 -gt 0 ] && NPROCS=$1
NTHREA=8; [ $# -ge 2 ] && [ $2 -eq $2 ] &> /dev/null && [ $2 -gt 0 ] && NTHREA=$2

topdir=$(realpath ../../)

if ${COMPILE}; then
    pushd ${topdir}
    make -j 48 || exit
    popd
fi

[ -d ${wdir} ] && rm -rf ${wdir}; mkdir ${wdir}

for SCALE in ${SCALES}; do
    for NTHREA in ${NTHREADS}; do
        NPROCS=$(( 48/NTHREA ))
        for MODE in ${MODES}; do
            printf "SCALE= %d np=%2d nt=%2d\n" $SCALE $NPROCS $NTHREA
            WDIR=$( printf "${wdir}/%d_%02d_%02d_${MODE}" ${SCALE} ${NPROCS} ${NTHREA} )
            mkdir ${WDIR}
            cp ${input_f} ${data_f} ${WDIR}
            pushd ${WDIR} &> /dev/null
            txt=$(<${input_f})
            echo "${txt}" | sed -e "s/index 1/index ${SCALE}/g" > ${input_f}

            script=./lammps.sh
            if [ "${MODE}" == "OMPSS" ]; then
                EXE=${topdir}/lmp_ompss
                src="#!/bin/bash
                export OMP_NUM_THREADS=${NTHREA}
                #export NX_ARGS=\"--smp-workers=${NTHREA} --summary --disable-ut\"
                #valgrind
                ${EXE} -pk ompss -sf ompss -i ${input_f}"
            fi
            if [ "${MODE}" == "OMP" ]; then
                EXE=${topdir}/lmp_omp
                src="#!/bin/bash
                export OMP_NUM_THREADS=${NTHREA}
                ${EXE} -sf omp -pk omp ${NTHREA} -i ${input_f}"
            fi
            if [ "${MODE}" == "KOKKOS" ]; then
                EXE=${topdir}/lmp_kokkos
                src="#!/bin/bash
                export OMP_NUM_THREADS=${NTHREA}
                ${EXE} -sf kk -k on t ${NTHREA} -i ${input_f}"
            fi
            echo "${src}" > ${script}
            chmod +x ${script}
            mpirun -np ${NPROCS} ${script} 2>&1 | tee output

            popd &> /dev/null
        done
    done
done
