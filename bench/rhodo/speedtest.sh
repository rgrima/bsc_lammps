#!/bin/bash

bdir=$(realpath ./)
tdir=$(realpath ../)

EXES="lmp_ompss lmp_omp"
PROCS="8"
SIZES="5"
wdir=Speed
[ -d ${wdir} ] && rm -rf ${wdir}; mkdir ${wdir}

InputF=in.rhodo.scaled
ExtraF=data.rhodo
txt=$(<${InputF})
CMEM=$(realpath ./MemCheck.py)
cd ${wdir}

IS_INTEL=false
mpirun --version | grep -i intel &> /dev/null && IS_INTEL=true
${IS_INTEL} && HFILE_FORMAT=":" || HFILE_FORMAT=" slots="
if [ ! -z "${SLURM_NODELIST}" ]; then
    for line in $( scontrol show hostnames $SLURM_JOB_NODELIST ); do
        echo "$line${HFILE_FORMAT}48"
    done > hfile
else
    [ -z ${NPROCS} ] && NPROCS=48
    echo "localhost${HFILE_FORMAT}${NPROCS}" > hfile
fi

scr="#!/bin/bash
export OMP_NUM_THREADS=NTHREA
EXE -i ${InputF}"

for i in ${SIZES}; do
    for exe in ${EXES}; do
        tag=${exe#*_}
        EXE=${tdir}/lmp_${tag}
        [ "${tag}" == "omp" ] && EXE="${EXE} -sf omp -pk omp NTHREA"
        for p in ${PROCS}; do
            t=$(( 48/p ))
            sdir=$( printf "%d_${tag}_%02d_%02d\n" $i $p $t )
            mkdir ${sdir}
            pushd ${sdir}  &> /dev/null
            ln -s ../hfile
            for f in ${ExtraF}; do cp ${bdir}/$f .; done
            echo "${txt}" | sed -e "s/index [0-9]\+/index ${i}/g" > ${InputF}
            echo "${scr}" | sed -e "s*EXE*${EXE}*g" | sed -e "s*NTHREA*${t}*g" > lammps.sh
            chmod +x lammps.sh
            ${CMEM} meminfo .log &
            sleep 0.5
            mpirun -np ${p} --hostfile hfile ./lammps.sh 2>&1 | tee output
            rm .log
            wait
            popd &> /dev/null
        done
    done
done

