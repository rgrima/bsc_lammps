#!/bin/bash

input_f="in.rhodo.scaled"
data_f="data.rhodo"
topdir=$(realpath ../../)
original_files=$( for f in ${input_f} ${data_f}; do realpath $f; done )

GNU=NO
TEST_DIR=StrongScaling
#rm -rf ${TEST_DIR} &> /dev/null

if [ -d ${TEST_DIR} ]; then
  i=1
  tt=$( printf "${TEST_DIR}-%02d" "${i}" )
  while [ -d ${tt} ]; do i=$(( i+1 )); tt=$( printf "${TEST_DIR}-%02d" "${i}" ); done
  mv ${TEST_DIR} ${tt}
fi
mkdir ${TEST_DIR}; cd ${TEST_DIR}

for NNODES in 1 2 4 6 8; do
    NCPUS=$(( NNODES*48 ))
    dir=$( printf "cpus_%04d" "${NCPUS}" )
    [ -d ${dir} ] && rm -rf ${dir}; mkdir ${dir}
    pushd ${dir} &> /dev/null
    cp ${original_files} .
    echo -e "%s/x index 1/x index 4/g\n" \
            "%s/y index 1/y index 4/g\n" \
            "%s/z index 1/z index 4/g\nw\nq\n" | ex ${input_f}
    cat << EOF > lammps.sh
#!/bin/bash
#SBATCH --job-name=LAMMPS
#SBATCH --workdir=.
#SBATCH --output=LAMMPS.out
#SBATCH --error=LAMMPS.err
#SBATCH --ntasks=$(( 48*${NNODES} ))
#SBATCH --cpus-per-task=1
#SBATCH --time=00:60:00
${QOS}

OMP="${topdir}/lmp_omp"
OMPSS="${topdir}/lmp_ompss"
KOKKOS="${topdir}/lmp_kokkos"
for threads in 1 2 4 6 8 12 24; do
    procs=\$(( ${NCPUS} / \${threads} ))
    export SLURM_CPUS_PER_TASK=\${threads}
    export SLURM_NPROCS=\${procs}
    export SLURM_NTASKS=\${procs}
    export SLURM_TASKS_PER_NODE="\$(( \${procs}/${NNODES} ))(x${NNODES})"
    for MOD in "OMPSS" "OMP" "KOKKOS"; do
        fn=\$( printf "\${MOD}-%04d-%02d" "\${procs}" "\${threads}" )
        script=\${fn}.sh
        ofile=\${fn}.out
        EXEC=\${!MOD}
        EXTRA_FLAGS=""
        if [ \${threads} -gt 1 ]; then
            [ \${MOD} == "OMP" ]    && EXTRA_FLAGS="-sf omp -pk omp \${threads}"
            [ \${MOD} == "KOKKOS" ] && EXTRA_FLAGS="-k on t \${threads} -sf kk" && export OMP_PROC_BIND=true
        fi
cat << EOOF > \${script}
#!/bin/bash
export OMP_NUM_THREADS=\${threads}
\${EXEC} \${EXTRA_FLAGS} -i ${input_f}
EOOF
        chmod +x \${script}
        srun ./\${script} > \${ofile}
    done
done
EOF
        chmod +x lammps.sh
        sbatch lammps.sh
        popd &> /dev/null
done
