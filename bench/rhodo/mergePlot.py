#!/usr/bin/env python
from os.path  import isdir, join, isfile, basename
from os       import listdir, getcwd

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter

PNAME = basename( getcwd() ).capitalize()

WDIR    = "ScanSize"
st_tags = [ "omp", "ompss", "kokkos" ]
#SIZES   = range(6)
SIZES   = [ 0, 5 ]

plot_bx = [ 121, 122 ]
ax      = [ None, None ]
fntsiz1 = 18
fntsiz2 = 16
fntsiz3 = 12

MEM_INFO=1
MEM_OUTP=2
MODE=MEM_OUTP
SHOW=False
OFILE   = "output"
#APP=[ "lj", "cycle" ]
APP=[ "cycle" ]

SCA=1.5
FIGSIZE = [ SCA*6.4, SCA*4.8 ]
COLORS  = [ "b", "r", "g" ]
if not isdir(WDIR) : exit()

sdirs= [ d for d in listdir(WDIR) if isdir(join(WDIR,d)) ]

for i in SIZES :
    COL    = { }
    natoms = None
    for t in st_tags :
        COL[t] = sorted([ d for d in sdirs if d.startswith( "%d_%s_"%( i+1, t ) ) ])
    fig = plt.figure(figsize=FIGSIZE)
    names = None
    natoms = None
    for ind,t in enumerate(st_tags) :
        VALS = [ ]
        names = [ name.split("_")[2:] for name in COL[t] ]
        #names = [ "%d_pr %d_th"%( int(a[0]), int(a[1]) ) for a in names ][::-1]
        names = [ "%dp/%dt"%( int(a[0]), int(a[1]) ) for a in names ][::-1]
        for bn in COL[t] :
            f=join(WDIR,bn,OFILE)
            natoms = [ l for l in open(f,"r").readlines( ) if l.startswith( "Loop time" ) ][0].split()[-2]
            value = [ float(l.split("=")[1].split()[0]) for l in open(f,"r").readlines( ) if l.startswith( "TIME_cycle" ) ][2]
            VALS = [ value ]+VALS
        line, = plt.plot( names, np.array(VALS), COLORS[ind], label=t )
        plt.plot( names, np.array(VALS), COLORS[ind]+'o' )
        plt.ylabel('Time', fontsize=fntsiz2, labelpad=5, fontweight='bold')
        plt.xticks(fontsize=fntsiz3)
        plt.yticks(fontsize=fntsiz3)
        plt.suptitle(PNAME+'('+natoms+' atoms): Time x Iteration (scaled=%d)'%( i+1 ), fontsize=fntsiz1, fontweight='bold')
        #plt.set_ylim(bottom=0)
    plt.axis(ymin=0)
    plt.legend()
    fn="mergedTime-%02d.png"%( i+1 )
    fig.savefig( fn, dpi=100 )
    print( fn )

for i in SIZES :
    COL    = { }
    natoms = None
    for t in st_tags :
        COL[t] = sorted([ d for d in sdirs if d.startswith( "%d_%s_"%( i+1, t ) ) ])
    fig, ax = plt.subplots( figsize=FIGSIZE )
    names = None
    natoms = None
    for ind,t in enumerate(st_tags) :
        VALS = [ ]
        names = [ name.split("_")[2:] for name in COL[t] ]
        #names = [ "%d_pr %d_th"%( int(a[0]), int(a[1]) ) for a in names ][::-1]
        names = [ "%dp/%dt"%( int(a[0]), int(a[1]) ) for a in names ][::-1]
        for bn in COL[t] :
            f=join(WDIR,bn,OFILE)
            natoms = [ l for l in open(f,"r").readlines( ) if l.startswith( "Loop time" ) ][0].split()[-2]
            value = [ int(l.split()[-1]) for l in open(f,"r").readlines( ) if l.startswith( "MEMORY            ITER" ) ][2]
            VALS = [ value ]+VALS
        line, = plt.plot( names, np.array(VALS), COLORS[ind], label=t )
        plt.plot( names, np.array(VALS), COLORS[ind]+'o' )
        plt.ylabel('Memory', fontsize=fntsiz2, labelpad=5, fontweight='bold')
        plt.xticks(fontsize=fntsiz3)
        plt.yticks(fontsize=fntsiz3) # rotation=45
        plt.suptitle(PNAME+'('+natoms+' atoms): Memory Use (scaled=%d)'%( i+1 ), fontsize=fntsiz1, fontweight='bold')
    ax.set_ylim(bottom=0)
    ax.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))
    plt.legend()
    fn="mergedMemory-%02d.png"%( i+1 )
    fig.savefig( fn, dpi=100 )
    print( fn )

