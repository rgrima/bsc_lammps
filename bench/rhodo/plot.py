#!/usr/bin/env python
from os.path  import isdir, join, isfile
from os       import listdir

import numpy as np
import matplotlib.pyplot as plt

MSG="lj (32.000.000 atoms) "

WDIR    = "Memory"
OFILE   = "meminfo"
st_tags = [ "omp_", "ompss_" ]
plot_bx = [ 121, 122 ]
ax      = [ None, None ]

if not isdir(WDIR) : exit()

sdirs= listdir(WDIR)
fig = plt.figure()
ymax = 0
xmax = 0
for j in range(2) :
    tag = st_tags[j]
    msg=MSG+tag[:-1].upper()

    ax[j] = fig.add_subplot(plot_bx[j])
    ax[j].set_title( msg )
    ax[j].set_ylabel( "Memory usage" )
    ax[j].set_xlabel( "Check every 0.1 s" )

    files = sorted( [ join(WDIR,s,OFILE) for s in sdirs if s.startswith(tag) ] )
    files = [ f for f in files if isfile(f) ]
    lines=[]
    xm = 0
    for f in files :
        p, t = [ int(v) for v in f.split("/")[1].split("_")[1:] ]
        y=np.array( [ int(i) for i in open(f).readlines() ] )
        print( f, y.max() )
        ymax = max(ymax,y.max())
        xmax = max(xmax,y.size)
        line, = ax[j].plot( y, label="%d mpi %d threads"%( p, t ) )
        lines.append( line )
    ax[j].legend( loc='lower right' )
xmax = 50
ymax = 20000000
for j in range(2) :
    ax[j].set_ylim( [0,ymax*1.01] )
    ax[j].set_xlim( [0,xmax] )
plt.show( )
plt.close( )
