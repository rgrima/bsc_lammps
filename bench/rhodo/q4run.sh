#!/bin/bash
#SBATCH --job-name=Q4x4
#SBATCH --workdir=.
#SBATCH --output=Q4run/out
#SBATCH --error=Q4run/err
#SBATCH --ntasks=4
#SBATCH --cpus-per-task=4
#SBATCH --time=12:00:00
#SBATCH --qos=bsc_case

#debug?

#echo "*** Loading modules"
#module rm intel impi &> /dev/null
#module load gcc/8.1.0 &> /dev/null
#module load openmpi/4.0.2 &> /dev/null
#module load ompss &> /dev/null

SCALE=3
wdir=Q4run

NPROCS=${SLURM_NTASKS}
NTHREA=$(( SLURM_CPUS_PER_TASK ))

input_f="in.rhodo.scaled"
data_f="data.rhodo"

topdir=$(realpath ../../)

[ -d ${wdir} ] && rm -rf ${wdir}; mkdir ${wdir}

cp ${input_f} ${data_f} extrae.xml ${wdir}

cd ${wdir}

txt=$(<${input_f})
echo "${txt}" | sed -e "s/index 1/index ${SCALE}/g" > ${input_f}

script=./lammps.sh

EXE=${topdir}/lmp_ompss
src="#!/bin/bash
export OMP_NUM_THREADS=${NTHREA}
export NX_ARGS=\"--smp-workers=${NTHREA} --summary --disable-ut\"
valgrind  --leak-check=full --show-reachable=yes --error-limit=no --gen-suppressions=all ${EXE} -pk ompss -sf ompss -i ${input_f}"
# --suppressions=nanos.supp  --log-file=rgt.log
echo "${src}" > ${script}
chmod +x ${script}
srun ${script} 2>&1 | tee kk1
