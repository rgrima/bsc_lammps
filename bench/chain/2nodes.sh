#!/bin/bash

SCALES="1 2 6 8"
NTHREADS="1 2 3 4  6 8 12 16 24 48"
MODES="OMPSS OMP MPI"

wdir=2Nodes

input_f="in.chain.scaled"
data_f="data.chain"

topdir=$(realpath ../../)
script=./lammps.sh

[ -d ${wdir} ] && rm -rf ${wdir}; mkdir ${wdir}

function makeScript( ) {
    mode_=$1
    file_=$2
    nthr_=$3
    src="#!/bin/bash\n"
    [ "${mode_}" == "OMPSS" ] &&
        src+="export OMP_NUM_THREADS=${nthr_}\n" &&
        src+="${topdir}/lmp_ompss_prv -pk ompss -sf ompss -i ${input_f}\n"
    [ "${mode_}" == "OMP" ] &&
        src+="export OMP_NUM_THREADS=${nthr_}\n" &&
        src+="${topdir}/lmp_omp_prv -sf omp -pk omp ${nthr_} -i ${input_f}\n"
    [ "${mode_}" == "MPI" ] &&
        src+="unset OMP_NUM_THREADS\n" &&
        src+="${topdir}/lmp_omp_prv -i ${input_f}\n"
    echo -e "${src}" > ${file_}
    chmod +x ${file_}
}

function makeHostFile( ) {
    NTASK=$1
    TAxNO=$(( NTASK/SLURM_NNODES ))
    NP=$(( SLURM_NNODES*48 ))
    export SLURM_NPROCS="${NTASKS}"
    export SLURM_NTASKS="${NTASKS}"
    [ ${SLURM_NNODES} -eq 1 ] &&
        export SLURM_TASKS_PER_NODE="${NTASKS}" ||
        export SLURM_TASKS_PER_NODE="${TAxNO}(x${SLURM_NNODES})"
    for line in $( scontrol show hostnames ${SLURM_JOB_NODELIST} ); do
        echo "${line}:${TAxNO}"
    done > hfile
}

for SCALE in ${SCALES}; do
    for NTHREA in ${NTHREADS}; do
        for MODE in ${MODES}; do
            PROCS=$(( 96/NTHREA ))
            WDIR=$( printf "${wdir}/${MODE}_%02d_%02d" ${SCALE} ${PROCS} )
            mkdir ${WDIR}
            cp ${input_f} ${data_f} extrae.xml ${WDIR}
            pushd ${WDIR} &> /dev/null

            txt=$(<${input_f})
            echo "${txt}" | sed -e "s/index 1/index ${SCALE}/g" > ${input_f}

            [ "${MODE}" == "MPI" ] && np=96 || np=${PROCS}

            makeHostFile ${np}
            
            makeScript ${MODE} ${script} ${NTHREA}

            mpirun -machine hfile ${script}  2>&1 | tee output

            popd &> /dev/null
        done
    done
done
