#!/bin/bash

SCALE=5
TRACE=true
OMPSS=true
OMP=true
KOKKOS=false

wdir=work
IS_INTEL=true

NPROCS=6; [ $# -ge 1 ] && [ $1 -eq $1 ] &> /dev/null && [ $1 -gt 0 ] && NPROCS=$1
NTHREA=8; [ $# -ge 2 ] && [ $2 -eq $2 ] &> /dev/null && [ $2 -gt 0 ] && NTHREA=$2

input_f="in.eam"
data_f="Cu_u3.eam"
topdir=$(realpath ../../)

pushd ${topdir}
if ${TRACE}; then
    make -j 48 TRACE=YES EXT=_prv || exit
else
    make -j 48 || exit
fi
popd

[ -d ${wdir} ] && rm -rf ${wdir}; mkdir ${wdir}

cp ${input_f} ${data_f} extrae.xml ${wdir}

cd ${wdir}

txt=$(<${input_f})
echo "${txt}" | sed -e "s/index 1/index ${SCALE}/g" > ${input_f}

script=./lammps.sh

if ${TRACE}; then
    if ${OMPSS}; then
        EXE="${topdir}/lmp_ompss_prv"
        EXTRAE_HOME=/apps/BSCTOOLS/extrae/latest/impi_2017_4
        src="#!/bin/bash
            export OMP_NUM_THREADS=${NTHREA}
            export LD_PRELOAD=${EXTRAE_HOME}/lib/libnanosmpitrace.so
            export EXTRAE_CONFIG_FILE=extrae.xml
            export NX_ARGS=\"--pes ${NTHREA} --instrument-default=user --instrument-disable=num-ready --instrument-disable=graph-size  --instrument-disable=api --instrument-disable=state\"
            export NX_INSTRUMENTATION=extrae
            ${EXE} -pk ompss -sf ompss -i ${input_f}"
        echo "${src}" > ${script}
        chmod +x ${script}
        mpirun -np ${NPROCS} ${script} 2>&1 | tee kk1
    fi
    if ${OMP}; then
        EXE="${topdir}/lmp_omp_prv"
        EXTRAE_HOME=/apps/BSCTOOLS/extrae/latest/impi_2017_4
        [ ${NTHREA} -gt 1 ] && OMP_F="-sf omp -pk omp ${NTHREA}" || OMP_F=
        MSG=$( nm -a ${EXE} )
        funcs="_ZN9LAMMPS_NS6Verlet3runEi
               _ZN9LAMMPS_NS6Verlet5setupEi
               _ZN9LAMMPS_NS6Verlet11force_clearEv
               _ZN9LAMMPS_NS26NPairHalfBinAtomonlyNewton5buildEPNS_9NeighListE
               _ZN9LAMMPS_NS29NPairHalfBinAtomonlyNewtonOmp5buildEPNS_9NeighListE
               _ZN9LAMMPS_NS6FixNVE15final_integrateEv
               _ZN9LAMMPS_NS9FixNVEOMP15final_integrateEv
               _ZN9LAMMPS_NS6FixNVE17initial_integrateEi
               _ZN9LAMMPS_NS9FixNVEOMP17initial_integrateEi
               _ZN9LAMMPS_NS9CommBrick8exchangeEv
               _ZN9LAMMPS_NS9CommBrick7bordersEv
               "
               kk="_ZN9LAMMPS_NS10PairEAMOMP7computeEii
               _ZN9LAMMPS_NS7PairEAM7computeEii
               "
        for f in ${funcs}; do
            line=( $( echo "$MSG" | grep -w "${f}$" ) )
            echo "${line[0]}#${line[2]}" >> functions.dat
        done

        src="#!/bin/bash
            export OMP_NUM_THREADS=${NTHREA}
            export LD_PRELOAD=${EXTRAE_HOME}/lib/libompitrace.so
            export EXTRAE_CONFIG_FILE=extrae.xml
            ${EXE} ${OMP_F} -i ${input_f} 2>&1 | tee kk2"
        echo "${src}" > ${script}
        chmod +x ${script}
        mpirun -np ${NPROCS} ${script} 2>&1 | tee kk2
    fi
    if ${KOKKOS}; then
        EXE="${topdir}/lmp_kokkos_prv"
        EXTRAE_HOME=/apps/BSCTOOLS/extrae/latest/impi_2017_4
        src="#!/bin/bash
            export OMP_NUM_THREADS=${NTHREA}
            export LD_PRELOAD=${EXTRAE_HOME}/lib/libompitrace.so
            export EXTRAE_CONFIG_FILE=extrae.xml
            ${EXE} -sf kk -k on t ${NTHREA} -i ${input_f}"
        echo "${src}" > ${script}
        chmod +x ${script}
        mpirun -np ${NPROCS} ${script} 2>&1 | tee kk3
    fi
else
    if ${OMPSS}; then
        EXE=${topdir}/lmp_ompss
        src="#!/bin/bash
        export OMP_NUM_THREADS=${NTHREA}
        #export NX_ARGS=\"--smp-workers=${NTHREA} --summary --disable-ut\"
        #valgrind
        ${EXE} -pk ompss -sf ompss -i ${input_f}"
        echo "${src}" > ${script}
        chmod +x ${script}
        #MPI_FLAG="--map-by socket:pe=${NTHREA}"
        mpirun ${MPI_FLAG} -np ${NPROCS} ${script} 2>&1 | tee kk1
        #mpirun -np ${NPROCS} ${scriptF} > kk1
        # | tee kk1
    fi
    if ${OMP}; then
        EXE=${topdir}/lmp_omp
        [ ${NTHREA} -gt 1 ] && OMP_F="-sf omp -pk omp ${NTHREA}" || OMP_F=
        src="#!/bin/bash
        export OMP_NUM_THREADS=${NTHREA}
        ${EXE} ${OMP_F} -i ${input_f}"
        #${EXE} -i ${input_f}"
        echo "${src}" > ${script}
        chmod +x ${script}
        mpirun -np ${NPROCS} ${script} | tee kk2
    fi
    if ${KOKKOS}; then
        EXE=${topdir}/lmp_kokkos
        src="#!/bin/bash
        export OMP_NUM_THREADS=${NTHREA}
        ${EXE} -sf kk -k on t ${NTHREA} -i ${input_f}"
        echo "${src}" > ${script}
        chmod +x ${script}
        mpirun -np ${NPROCS} ${script} > kk3
    fi
fi

