#!/bin/bash

mkdir .lock &> /dev/null && MASTER=true || MASTER=false

if [ $(arch) == "aarch64" ]; then
    if [ -z "${EXTRAE_HOME}" ]; then
      [ -d ${HOME}/Projects/extrae-3.8.2 ] && export EXTRAE_HOME=${HOME}/Projects/extrae-3.8.2/
      [ -d ${APPS_PATH}/extrae-3.8.3_backtrace ] && export EXTRAE_HOME=${APPS_PATH}/extrae-3.8.3_backtrace
    fi
    [ -z "${OMP_NUM_THREADS}" ] && { export OMP_NUM_THREADS=1; [ ! -z "${PJM_PROC_BY_NODE}" ] && export OMP_NUM_THREADS=$(( 48/PJM_PROC_BY_NODE )); }
else if [ ! -z "${BSC_MACHINE}" ]; then
    export EXTRAE_HOME=/apps/BSCTOOLS/extrae/latest/impi_2017_4/
    [ -z "${OMP_NUM_THREADS}" ] && { export OMP_NUM_THREADS=1; [ ! -z "${SLURM_CPUS_PER_TASK}" ] && export OMP_NUM_THREADS=${SLURM_CPUS_PER_TASK}; }
else
    export EXTRAE_HOME=/opt/extrae-3.8.1
    [ -z "${OMP_NUM_THREADS}" ] && export OMP_NUM_THREADS=1
fi; fi

f=${EXTRAE_HOME}/etc/extrae.sh
[ ! -f ${f} ] && { echo "File '$f' does not exist"; exit; }
source ${f}

ldd $1 | grep -e libmpi_cxx -e libmpicxx &> /dev/null && MPIC=true || MPIC=false
ldd $1 | grep -i libmpi_mpifh &> /dev/null && MPIF=true || MPIF=false
ldd $1 | grep -e libgomp -e libiomp5 -e jomp &> /dev/null && OMP=true || OMP=false
ldd $1 | grep -i libnanox  &> /dev/null && OMPSS=true || OMPSS=false
ldd $1 | grep -i libnanos6  &> /dev/null && OMPSS2=true || OMPSS2=false

if $MPIC; then
    MODE=mpitrace
    [ "${OMP_NUM_THREADS}" -gt 1 ] && $OMP    && MODE=ompitrace && export EXTRAE_ON=1
    $OMPSS  && MODE=nanosmpitrace && export NX_INSTRUMENTATION=extrae &&
  	    export NX_ARGS="--instrumentation=extrae --pes ${NTHREA} --instrument-default=user --instrument-disable=num-ready --instrument-disable=graph-size  --instrument-disable=api --instrument-disable=state"
    #   export NX_ARGS="--pes ${NTHREA} --instrument-default=user --instrument-disable=num-ready --instrument-disable=graph-size  --instrument-disable=api --instrument-disable=state"
    $OMPSS2 && export NANOS6=extrae && export NANOS6_EXTRAE_AS_THREADS=1 && MODE=nanosmpitrace
#    $OMPSS2 && export NANOS6=extrae && MODE=nanosmpitrace
else if $MPIF; then
    echo MPIF; exit
else
    $OMP    && MODE=omptrace && export EXTRAE_ON=1
    $OMPSS  && MODE=nanostrace && export NX_ARGS="--instrumentation=extrae"
    $OMPSS2 && unset MODE && export NANOS6=extrae && export NANOS6_EXTRAE_AS_THREADS=1 && MODE=nanostrace
#    $OMPSS2 && unset MODE && export NANOS6=extrae && MODE=nanostrace
fi; fi

# Needed to use PAPI and perf
xospastop

[ ! -z $MODE ] && export LD_PRELOAD=${EXTRAE_HOME}/lib/lib${MODE}.so
[ ${MODE} == "mpitrace" ] && unset OMP_NUM_THREADS
export EXTRAE_CONFIG_FILE=extrae.xml

$MASTER && {
  [ ! -z "${LD_PRELOAD}" ]      && echo "LD_PRELOAD      = ${LD_PRELOAD}"
  [ ! -z "${OMP_NUM_THREADS}" ] && echo "OMP_NUM_THREADS = ${OMP_NUM_THREADS}"
  [ ! -z "${NX_ARGS}" ]         && echo "NX_ARGS         = ${NX_ARGS}"
  [ ! -z "${NANOS6}" ]          && echo "NANOS6          = ${NANOS6}"; }

$*

unset LD_PRELOAD

$MASTER && rm -rf .lock
