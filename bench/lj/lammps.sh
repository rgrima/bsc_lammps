#!/bin/bash
            export OMP_NUM_THREADS=12
            export LD_PRELOAD=/apps/BSCTOOLS/extrae/latest/impi_2017_4/lib/libnanosmpitrace.so
            export EXTRAE_CONFIG_FILE=extrae.xml
            export NX_ARGS="--pes 12 --instrument-default=user --instrument-disable=num-ready --instrument-disable=graph-size  --instrument-disable=api --instrument-disable=state"
            export NX_INSTRUMENTATION=extrae
            /gpfs/projects/bsc21/bsc21017/bsc_lammps/lmp_ompss_prv -pk ompss -sf ompss -i in_200.lj
