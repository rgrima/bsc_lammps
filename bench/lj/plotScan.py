#!/usr/bin/env python
from os.path  import isdir, join, isfile, basename
from os       import listdir, getcwd

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter

PNAME = basename( getcwd() ).capitalize()

WDIR    = "ScanSize"
st_tags = [ "omp", "ompss", "kokkos" ]
plot_bx = [ 121, 122 ]
ax      = [ None, None ]

MEM_INFO=1
MEM_OUTP=2
MODE=MEM_OUTP
SHOW=False
if   MODE==MEM_INFO : OFILE   = "meminfo"
elif MODE==MEM_OUTP : OFILE   = "output"

SCA=2.5
FIGSIZE = [ SCA*6.4, SCA*4.8 ]
COLORS  = [ "b", "r", "g" ]
if not isdir(WDIR) : exit()

sdirs= [ d for d in listdir(WDIR) if isdir(join(WDIR,d)) ]

for i in [ 0, 5 ] :
    COL    = [ ]
    natoms = None
    for t in st_tags :
        COL.append( sorted([ d for d in sdirs if d.startswith( "%d_%s_"%( i+1, t ) ) ]) )
    print( "COL", COL )
    r=1
    c=1
    n=len(COL[0])
    for j in range(1,n) :
        a=n/j
        if a < n/a : break
        if n//j==a :
            c=n//j
            r=n//c
    fig = plt.figure(figsize=FIGSIZE)
    gs = fig.add_gridspec( r, c )
    mmax = 0.0
    AX=[]
    for j in range(n) :
        lr = j//c
        lc = j%c
        ax=fig.add_subplot(gs[lr,lc])
#        ax.set_ylabel( "Time" )
#        ax.set_xlabel( "Iteration" )

        MSG = None
        yy = [ ]
        for it, C in enumerate(COL) :
            f=join(WDIR,C[j],OFILE)
            if not natoms :
                for l in open(f,"r").readlines( ) :
                    if l.startswith( "Loop time" ) :
                        natoms=l.split()[-2]
                        break
                fig.suptitle(PNAME+'('+natoms+' atoms) : Memory use (kb) x Iteration (scaled=%d)'%( i+1 ), fontsize=16)
            if not MSG :
                pr, th = [ int(a) for a in C[j].split("_")[-2:] ]
                MSG="%2d Procs %2d Threads"%( pr, th )
                ax.set_title( MSG )
                lines=[]
            if   MODE==MEM_INFO :
                y=np.array( [ int(i) for i in open(f).readlines() ] )
            elif MODE==MEM_OUTP :
                y=np.array( [ int(a.split()[-1]) for a in open(f).readlines() if a.startswith( "MEMORY            ITER" ) ] )
            yy += [ y ]
            line, = ax.plot( y, label=C[j].split("_")[1]+" lj", color=COLORS[it] )
            #lines.append( line )
            #ax.fill_between( x, 0, y, alpha=0.3, color=COLORS[it] )
            mmax = max(mmax,y.max())
#            y = np.array( [ float(a.split("=")[1].split()[0]) for a in open(f).readlines() if "TIME_cycle=" in a ] )
#            line, = ax.plot( y, label=C[j].split("_")[1]+" cycle" )
#            lines.append( line )
#            mmax = max(mmax,y.max())
        x = np.arange( y.size )
        #ax.fill_between( x, yy[0], yy[1], alpha=0.3, facecolor=COLORS[0], where=(yy[0]-yy[1]>=0), interpolate=True )
        #ax.fill_between( x, yy[0], yy[1], alpha=0.3, facecolor=COLORS[1], where=(yy[0]-yy[1]<=0), interpolate=True )
        ax.set_ylim(bottom=0)
        if lc==0 : ax.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))
        else :     ax.yaxis.set_major_locator( plt.NullLocator() )

        #ax.legend( loc='lower right' )
        ax.legend( loc='best' )
        AX.append( ax )
    for ax in AX :
        ax.set_ylim( [0,mmax*1.01] )
    #plt.show( )
    fn="memo-%02d.png"%( i+1 )
    fig.savefig( fn, dpi=100 )
