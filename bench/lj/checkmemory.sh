#!/bin/bash
CFILE=meminfo; [ $# -ge 1 ] && CFILE=$1
LFILE=.log; [ $# -ge 2 ] && LFILE=$2
touch ${LFILE}
vals=( $( cat /proc/meminfo | grep Mem[FT] | tr -s " "  | cut -d" " -f 2 ) )
REFMEM=$(( ${vals[0]} - ${vals[1]} ))
MSG=""
i=0
while [ -f "${LFILE}" ]; do
    i=$(( i+1 ))
    sleep 0.5
    vals=( $( cat /proc/meminfo | grep Mem[FT] | tr -s " "  | cut -d" " -f 2 ) )
    MSG="${MSG}$(( ${vals[0]} - ${vals[1]} - ${REFMEM} ))\n"
done
echo -e "${MSG}" > ${CFILE}
