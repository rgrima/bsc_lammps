#!/usr/bin/env python
from __future__ import print_function
import numpy as np
from os.path  import isfile
from sys      import argv

DEF_FN='rgt'
TAGS='COMM_BORDERS COMM_EXCHANGE COMM_FORWARD COMM_REVERSE'.split()

SRC=argv.pop( 0 )
fn = argv.pop( 0 ) if argv else DEF_FN

if not isfile(fn) : exit( "Could not find file '"+fn+"'" )

tag='COMM_BORDERS'

lines=open(fn).readlines()
n=len(lines)
i=0
while i<n :
    if "SCALE" in lines[i] :
        msg=lines[i][:-1]
        i = i+1
        t={}
        while i<n and "SCALE" not in lines[i] :
            for tag in TAGS :
                if tag in lines[i] :
                    val = float(lines[i].split('=')[1])
                    #print( "Found", lines[i], end='' )
                    #print( "    val=", val )
                    if tag in t :
                        t[tag] = t[tag] + [ val ]
                    else : t[tag] = [ val ]
                    #print( "    t["+tag+"]=", t[tag] )
                    break
            i = i+1
        if (len(t)>0) :
            for tag in TAGS :
                msg = msg+" %8lf"%( np.average(np.array(t[tag])) if tag in t else 0.0 )
            print( msg )
#            print( msg+" %8lf"%( np.average(np.array(t))) )
    else : i = i+1
    
