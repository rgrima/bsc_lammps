#!/bin/bash
#PJM -N lmp
#PJM -L rscgrp=q48node
#PJM -L rscunit=rscunit_ft01
#PJM -L node=1
#PJM -L elapse=0:30:00
#PJM --mpi proc=4
#PJM --no-stging
#PJM -j

wdir=A64FX
input_f="in.*"
data_f=""
[ -f data.* ] && data_f="${data_f} data.*"
[ -f ffield.* ] && data_f="${data_f} ffield.*"
[ -f lib.* ] && data_f="${data_f} lib.*"
[ -f Cu_u3.eam ] && data_f="${data_f} Cu_u3.eam"
[ -f CH.airebo ] && data_f="${data_f} CH.airebo"

NAME=$( basename $( pwd ) )

topdir=$(realpath ../../)
script=./lammps.sh

if false; then
    TEST="mpi omp omp2 omp3 ompss_1"
else
    TEST="omp ompss_1"
fi

if false; then
    [ "${NAME}" == "chute" ] && SCALE="1 7 13 19" || SCALE="1 3 5 7"
else
    SCALE=1
    #[[ "${NAME}" == "chain" || "${NAME}" == "eam" ]] && SCALE=5
    [[ "${NAME}" == "eam" ]] && SCALE=5
    [ "${NAME}" == "chute" ] && SCALE=15
fi
false && { TRACE="./trace.sh"; wdir="${wdir}_prv"; } || TRACE=""
false && export XOS_MMM_L_HPAGE_TYPE=disable

[ -d ${wdir} ] && rm -rf ${wdir}; mkdir ${wdir}

NCPUS=$( grep ^cpu\\scores /proc/cpuinfo | uniq |  awk '{print $4}' )

[ -z ${PJM_PROC_BY_NODE} ] && NT=2 ||
  NT=$(( NCPUS/PJM_PROC_BY_NODE ))
for t in ${TEST}; do

    [ "${t}" == "omp" ] && {
        make -j ${NCPUS} -l ${NCPUS} -C ../.. lmp_omp || exit
        EXE="${topdir}/lmp_omp -sf omp -pk omp ${NT} -i ${input_f}"; }
    [ "${t}" == "omp3" ] && {
        make -j ${NCPUS} -l ${NCPUS} -C ../.. lmp_omp_3 || exit
        EXE="${topdir}/lmp_omp_3 -sf omp -pk omp ${NT} -i ${input_f}"; }
    [ "${t}" == "omp2" ] && {
        make -j ${NCPUS} -l ${NCPUS} -C ../.. lmp_omp_2 || exit
        EXE="${topdir}/lmp_omp_2 -pk ompss -sf ompss -i ${input_f}"; }
    [ "${t}" == "mpi" ] && {
        NT=1
        make -j ${NCPUS} -l ${NCPUS} -C ../.. lmp_mpi || exit
        EXE="${topdir}/lmp_mpi -pk ompss -sf ompss -i ${input_f}"; }
#        EXE="${topdir}/lmp_mpi -i ${input_f}"; }
    [ "${t}" == "mpio" ] && {
        NT=1
        make -j ${NCPUS} -l ${NCPUS} -C ../.. lmp_mpio || exit
        EXE="${topdir}/lmp_mpio -sf omp -pk omp 1 -i ${input_f}"; }
    [ "${t}" == "ompss_2" ] && {
        make -j ${NCPUS} -l ${NCPUS} OMPSS_VERS=2 -C ../.. lmp_ompss_2 || exit
        EXE="${topdir}/lmp_ompss_2 -pk ompss -sf ompss -i ${input_f}"; }
    [ "${t}" == "ompss_1" ] && {
        if [ -f "${TRACE}" ]; then
            make -j ${NCPUS} -l ${NCPUS} TRACE=YES EXT=_prv OMPSS_VERS=1 -C ../.. lmp_ompss_1_prv || exit
            EXE="${topdir}/lmp_ompss_1_prv -pk ompss -sf ompss -i ${input_f}"
        else
            make -j ${NCPUS} -l ${NCPUS} OMPSS_VERS=1 -C ../.. lmp_ompss_1 || exit
            EXE="${topdir}/lmp_ompss_1 -pk ompss -sf ompss -i ${input_f}"
        fi; }
     for s in ${SCALE}; do
        cdir=$( printf "${wdir}/%d_${t}" $s )
        mkdir ${cdir}
        cp extrae.xml trace.sh ${input_f} ${data_f} ${cdir}
        pushd ${cdir} &> /dev/null
        txt=$(<${input_f})
        echo "${txt}" | sed -e "s/index 1/index ${s}/g" > ${input_f}
        export MEMAVAILABLE=$( grep MemAvailable /proc/meminfo | tr -s ' ' | cut -d ' ' -f 2 )
        echo OMP_NUM_THREADS=$NT mpiexec $TRACE $EXE > outfile
        OMP_NUM_THREADS=$NT mpiexec $TRACE $EXE 2>&1 | tee -a outfile
        popd &> /dev/null
    done
done

