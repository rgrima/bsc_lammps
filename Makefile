
include arch.make

mpiSRCDIR=./ompss
mpiDESTDIR=./.mpi
$(shell mkdir -p $(mpiDESTDIR) )
mpiSRCS=$(sort $(wildcard $(mpiSRCDIR)/*.cpp))
mpiOBJS=$(patsubst $(mpiSRCDIR)/%.cpp,$(mpiDESTDIR)/%.o,$(mpiSRCS))
MPI_EXE=lmp_mpi
EXES=$(MPI_EXE)

mpioSRCDIR=./omp
mpioDESTDIR=./.mpio
$(shell mkdir -p $(mpioDESTDIR) )
mpioSRCS=$(sort $(wildcard $(mpioSRCDIR)/*.cpp))
mpioOBJS=$(patsubst $(mpioSRCDIR)/%.cpp,$(mpioDESTDIR)/%.o,$(mpioSRCS))
MPIO_EXE=lmp_mpio
EXES+=$(MPIO_EXE)

ompSRCDIR=./omp
ompDESTDIR=./.omp
$(shell mkdir -p $(ompDESTDIR) )
ompSRCS=$(sort $(wildcard $(ompSRCDIR)/*.cpp))
ompOBJS=$(patsubst $(ompSRCDIR)/%.cpp,$(ompDESTDIR)/%.o,$(ompSRCS))
OMP_EXE=lmp_omp
EXES+=$(OMP_EXE)

omp2SRCDIR=./ompss
omp2DESTDIR=./.omp2
$(shell mkdir -p $(omp2DESTDIR) )
omp2SRCS=$(sort $(wildcard $(omp2SRCDIR)/*.cpp))
omp2OBJS=$(patsubst $(omp2SRCDIR)/%.cpp,$(omp2DESTDIR)/%.o,$(omp2SRCS))
OMP2_EXE=lmp_omp_2
EXES+=$(OMP2_EXE)

ifeq (YES, $(shell test -d omp3 && echo YES))
    omp3SRCDIR=./omp3
    omp3DESTDIR=./.omp3
    $(shell mkdir -p $(omp3DESTDIR) )
    omp3SRCS=$(sort $(wildcard $(omp3SRCDIR)/*.cpp))
    omp3OBJS=$(patsubst $(omp3SRCDIR)/%.cpp,$(omp3DESTDIR)/%.o,$(omp3SRCS))
    OMP3_EXE=lmp_omp_3
    EXES+=$(OMP3_EXE)
endif

ifeq (1,$(OMPSS_VERS))
    ompss1SRCDIR=./ompss
    ompss1DESTDIR=./.ompss_1
    $(shell mkdir -p $(ompss1DESTDIR) )
    ompss1SRCS=$(sort $(wildcard $(ompss1SRCDIR)/*.cpp))
    ompss1OBJS=$(patsubst $(ompss1SRCDIR)/%.cpp,$(ompss1DESTDIR)/%.o,$(ompss1SRCS))
    OMPSS1_EXE=lmp_ompss_1
    EXES+=$(OMPSS1_EXE)
endif

ifeq (1,$(OMPSS_VERS))
    ompss1SRCDIR_prv=./ompss
    ompss1DESTDIR_prv=./.ompss_1_prv
    $(shell mkdir -p $(ompss1DESTDIR_prv) )
    ompss1SRCS_prv=$(sort $(wildcard $(ompss1SRCDIR_prv)/*.cpp))
    ompss1OBJS_prv=$(patsubst $(ompss1SRCDIR_prv)/%.cpp,$(ompss1DESTDIR_prv)/%.o,$(ompss1SRCS_prv))
    OMPSS1_EXE_prv=lmp_ompss_1_prv
    EXES+=$(OMPSS1_EXE_prv)
endif

ifeq (2,$(OMPSS_VERS))
    ompss2SRCDIR=./ompss
    ompss2DESTDIR=./.ompss_2
    $(shell mkdir -p $(ompss2DESTDIR) )
    ompss2SRCS=$(sort $(wildcard $(ompss2SRCDIR)/*.cpp))
    ompss2OBJS=$(patsubst $(ompss2SRCDIR)/%.cpp,$(ompss2DESTDIR)/%.o,$(ompss2SRCS))
    OMPSS2_EXE=lmp_ompss_2
    EXES+=$(OMPSS2_EXE)
endif

kokkosSRCDIR=./kokkos
kokkosDESTDIR=./.kokkos
$(shell mkdir -p $(kokkosDESTDIR) )
kokkosSRCS=$(sort $(wildcard $(kokkosSRCDIR)/*.cpp))
kokkosOBJS=$(patsubst $(kokkosSRCDIR)/%.cpp,$(kokkosDESTDIR)/%.o,$(kokkosSRCS))
KOKKOS_EXE=lmp_kokkos
EXES+=$(KOKKOS_EXE)

instr=verlet comm_brick \
      fix_nve fix_nve_omp \
      npair_half_bin_atomonly_newton npair_half_bin_atomonly_newton_omp

      
instrOBJS=$(patsubst %,$(ompDESTDIR)/%.o,$(instr))


ompssSpe= \
    accelerator_ompss \
    angle_charmm_ompss \
    atom \
    bond_fene_ompss \
    bond_harmonic_ompss \
    comm_brick_ompss \
    compute_erotate_sphere \
    compute_temp \
    create_atoms \
    dihedral_charmm_ompss \
    domain \
    fft3d \
    fix_freeze \
    fix_gravity \
    fix_langevin_ompss \
    fix_neigh_history_ompss \
    fix_nh \
    fix_nve_ompss \
    fix_nve_sphere \
    fix_shake_ompss \
    group \
    improper_harmonic_ompss \
    kspace \
    modify \
    nbin_standard \
    neighbor \
    npair_half_bin_atomonly_newton_ompss \
    npair_half_bin_newton_ompss \
    npair_half_size_bin_newtoff_ompss \
    ntopo_angle_partial_ompss \
    ntopo_bond_all_ompss \
    ntopo_bond_partial_ompss \
    ntopo_dihedral_all_ompss \
    ntopo_improper_all_ompss \
    pair_eam_ompss \
    pair_gran_hooke_history_ompss \
    pair_lj_charmm_coul_long_ompss \
    pair_lj_cut_ompss \
    pppm_ompss \
    remap \ \
    replicate \
    thr_ompss \
    velocity \
    verlet

ompss1SpeObj=$(patsubst %,$(ompss1DESTDIR)/%.o,$(ompssSpe))
ompss2SpeObj=$(patsubst %,$(ompss2DESTDIR)/%.o,$(ompssSpe))
ompss1SpeObj_prv=$(patsubst %,$(ompss1DESTDIR_prv)/%.o,$(ompssSpe))

ompSpe=finish input kissfft comm
ompSpeObj=$(patsubst %,$(ompss1DESTDIR)/%.o,$(ompSpe))
ompSpeObj=$(patsubst %,$(ompss2DESTDIR)/%.o,$(ompSpe))
ompSpeObj=$(patsubst %,$(ompss1DESTDIR_prv)/%.o,$(ompSpe))

.PHONY: directories

#bin: directories $(OMP_EXE) $(OMP2_EXE)
bin: directories $(EXES)

$(ompOBJS):  DEFINES += -DLMP_USER_OMP
$(omp3OBJS): DEFINES += -DLMP_USER_OMP
$(mpioOBJS): DEFINES += -DLMP_USER_OMP

$(ompss1OBJS): DEFINES += ${OMPSS1_DEFINES}
$(ompss2OBJS): DEFINES += ${OMPSS2_DEFINES}
$(ompss1OBJS_prv): DEFINES += ${OMPSS1_DEFINES}
$(instrOBJS): CXXFLAGS += $(INSTRU)
$(kokkosOBJS): DEFINES += -DLMP_KOKKOS -DLMP_USER_OMP
$(kokkosOBJS): CXXFLAGS += -std=c++11


#$(ompssSpeObj): CXX := $(PFLAGS) $(CXX)
$(ompss1SpeObj): CXX := $(MCXX)
$(ompss2SpeObj): CXX := $(MCXX)
$(ompss1SpeObj_prv): CXX := $(MCXX)
$(ompss1SpeObj): CXXFLAGS += $(OMPSS1_FLAGS)
$(ompss2SpeObj): CXXFLAGS += $(OMPSS2_FLAGS)
$(ompss1SpeObj_prv): CXXFLAGS += $(OMPSS1_FLAGS) $(OMPSS1_INSTRU)
$(ompSpeObj):   CXXFLAGS += $(OMP_FLAGS)

$(ompss1DESTDIR)/pair_comb.o: MCXX := $(CXX)
$(ompss2DESTDIR)/pair_comb.o: MCXX := $(CXX)
$(ompss1DESTDIR_prv)/pair_comb.o: MCXX := $(CXX)

$(MPI_EXE): $(mpiOBJS)
	$(CXX) $(CXXFLAGS) -D_OMPSS=0 -o $@ $^ $(LDFLAGS)
$(mpiDESTDIR)/%.o: $(mpiSRCDIR)/%.cpp
	$(CXX) $(CXXFLAGS) -D_OMPSS=0 $(DEFINES) -c -o $@ $<

$(MPIO_EXE): $(mpioOBJS)
	$(CXX) $(CXXFLAGS) -o $@ $^ $(LDFLAGS)
$(mpioDESTDIR)/%.o: $(mpioSRCDIR)/%.cpp
	$(CXX) $(CXXFLAGS) $(DEFINES) -c -o $@ $<

$(OMP_EXE): $(ompOBJS)
	$(CXX) $(OMP_FLAGS) $(EFLAGS) $(CXXFLAGS) -o $@ $^ $(LDFLAGS)
$(ompDESTDIR)/%.o: $(ompSRCDIR)/%.cpp
	$(CXX) $(OMP_FLAGS) $(EFLAGS) $(CXXFLAGS) $(DEFINES) -c -o $@ $<

$(OMP2_EXE): $(omp2OBJS)
	$(CXX) $(OMP_FLAGS) $(EFLAGS) $(CXXFLAGS) -o $@ $^ $(LDFLAGS)
$(omp2DESTDIR)/%.o: $(omp2SRCDIR)/%.cpp
	$(CXX) $(OMP_FLAGS) $(EFLAGS) $(CXXFLAGS) $(DEFINES) -c -o $@ $<

$(OMP3_EXE): $(omp3OBJS)
	$(CXX) $(OMP_FLAGS) $(EFLAGS) $(CXXFLAGS) -o $@ $^ $(LDFLAGS)
$(omp3DESTDIR)/%.o: $(omp3SRCDIR)/%.cpp
	$(CXX) $(OMP_FLAGS) $(EFLAGS) $(CXXFLAGS) $(DEFINES) -c -o $@ $<

$(OMPSS1_EXE): $(ompss1OBJS)
	$(MCXX) $(OMPSS1_FLAGS) $(CXXFLAGS) -o $@ $^ $(LDFLAGS)
$(ompss1DESTDIR)/%.o: $(ompss1SRCDIR)/%.cpp
	$(CXX) $(CXXFLAGS) $(EFLAGS) $(DEFINES) -c -o $@ $<

$(OMPSS1_EXE_prv): $(ompss1OBJS_prv)
	$(MCXX) $(OMPSS1_FLAGS) $(OMPSS1_INSTRU) $(CXXFLAGS) -o $@ $^ $(LDFLAGS)
$(ompss1DESTDIR_prv)/%.o: $(ompss1SRCDIR)/%.cpp
	$(CXX) $(CXXFLAGS) $(EFLAGS) $(DEFINES) -c -o $@ $<

$(OMPSS2_EXE): $(ompss2OBJS)
	$(MCXX) $(OMPSS2_FLAGS) $(CXXFLAGS) -o $@ $^ $(LDFLAGS)
$(ompss2DESTDIR)/%.o: $(ompss1SRCDIR)/%.cpp
	$(CXX) $(CXXFLAGS) $(EFLAGS) $(DEFINES) -c -o $@ $<

$(KOKKOS_EXE): $(kokkosOBJS)
	$(CXX) $(KOKKOS_FLAGS) $(EFLAGS) $(CXXFLAGS) -o $@ $^ $(LDFLAGS)
$(kokkosDESTDIR)/%.o: $(kokkosSRCDIR)/%.cpp
	@echo HOLA 
	$(CXX) $(KOKKOS_FLAGS) $(EFLAGS) $(CXXFLAGS) $(DEFINES) -c -o $@ $<

clean:
	rm -rf $(ompDESTDIR) $(OMP_EXE)
	rm -rf $(ompss1DESTDIR) $(OMPSS1_EXE)
	rm -rf $(ompss2DESTDIR) $(OMPSS2_EXE)
	rm -rf $(ompss1DESTDIR_prv) $(OMPSS1_EXE_prv)
	rm -rf $(kokkosDESTDIR) $(KOKKOS_EXE)
	rm -rf mcxx_*.cpp


verlet.o: verlet.h
style_integrate.h: verlet.h
update.o: style_integrate.h
lampss.o: style_integrate.h

-include .dep
