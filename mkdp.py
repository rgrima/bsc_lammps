#!/usr/bin/env python

from __future__ import print_function
from os         import listdir
from os.path    import isfile, join
from sys        import stderr

IGNORE=['erf.h','sleep.h','algorithm','mkl_dfti.h',
        'sfftw.h','fftw.h','fftw3.h','dfftw.h',
        'restart_mpiio.h','jpeglib.h','python_impl.h','omp.h']
NONOMP=['fix_omp.h','thr_data.h']


def CheckHeaders( fn, mode ) :
    lista=[]
    FD = open( fn )
    ind = 0
    while True :
        ind+=1
        try:
            l = FD.readline( )
        except:
            stderr.write( "Error reding file '%s', line %d\n"%( fn, ind ) )
            continue
        if not l : break
        if "#" in l and "include" in l :
            val = l.split('"')
            if len(val) != 3 or val[1] in IGNORE or "/" in val[1] : continue
            if mode=="ompss" and val[1] in NONOMP : continue
            if mode in [ "ompss", "omp", "omp3" ] and val[1].endswith('kokkos.h') : continue
            lista += [ val[1] ]
    return lista
#    try:
#        LINES = open(fn,errors='ignore').readlines( )
#    except:
#        exit( "Error trying to read file "+fn )
#    for l in LINES :
#        if "#" in l and "include" in l :
#            val = l.split('"')
#            if len(val) != 3 or val[1] in IGNORE or "/" in val[1] : continue
#            if mode=="ompss" and val[1] in NONOMP : continue
#            if mode in [ "ompss", "omp", "omp3" ] and val[1].endswith('kokkos.h') : continue
#            lista += [ val[1] ]
#    return lista

#for src in [ "ompss", "omp", "kokkos", "omp3" ] :

for mode in [ "ompss","kokkos","omp","omp2","mpi" ] : # "omp3"
    if mode in [ "kokkos","omp", "omp3" ] :
        src = mode
        dst ="."+src
    elif mode == "ompss" :
        src = mode
        dst =".ompss_1"
    elif mode == "omp2" :
        src = "ompss"
        dst = ".omp2"
    elif mode == "mpi" :
        src = "ompss"
        dst = ".mpi"

    srcf=[ f for f in listdir( src ) if isfile(src+"/"+f) ]
    HEADERS={}

    for f in srcf :
        if f.endswith(".h") : HEADERS[f]=CheckHeaders( join(src,f), src )

    for f in srcf :
        if not f.endswith(".cpp") : continue
        lista = CheckHeaders( join(src,f), src )
        n=len(lista)
        i = 0
        while i<n :
            hi=lista[i]
            if hi in HEADERS :
                for h in HEADERS[hi] :
                    if h not in lista :
                        lista += [ h ]
            i += 1
        for expand in [ "" ] : # "_prv"
            o=join(dst+expand,f.rsplit(".",1)[0]+".o")
            d=[ join(src,i) for i in [f] + lista ]
            print( o+": "+" ".join( d ) )
