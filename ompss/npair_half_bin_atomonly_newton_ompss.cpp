/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#include "npair_half_bin_atomonly_newton_ompss.h"
#include "neighbor.h"
#include "neigh_list.h"
#include "atom.h"
#include "atom_vec.h"
#include "molecule.h"
#include "domain.h"
#include "my_page.h"
#include "error.h"
#include "comm.h"

using namespace LAMMPS_NS;

/* ---------------------------------------------------------------------- */

NPairHalfBinAtomonlyNewtonOmpss::NPairHalfBinAtomonlyNewtonOmpss(LAMMPS *lmp) : NPair(lmp) {}

/* ----------------------------------------------------------------------
   binned neighbor list construction with full Newton's 3rd law
   each owned atom i checks its own bin and other bins in Newton stencil
   every pair stored exactly once by some processor
------------------------------------------------------------------------- */

void NPairHalfBinAtomonlyNewtonOmpss::build(NeighList *list)
{
  int nlocal = (includegroup) ? atom->nfirst : atom->nlocal;
  int ntask = comm->nworkers;

# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) \
  shared(ntask,nlocal,list)
# elif   _OMPSS == 2
# pragma oss task for default(none) \
  shared(ntask,nlocal,list)
# endif
  for (int t=0; t < ntask; t++)
  {
    int di = nlocal/ntask;
    int mo = nlocal%ntask;
    int ini = t*di + ( t < mo ? t : mo );
    int end = ini + di + ( t < mo ? 1 : 0 );

    double **x = atom->x;
    int *type = atom->type;
    int *mask = atom->mask;
    tagint *molecule = atom->molecule;

    int *ilist = list->ilist;
    int *numneigh = list->numneigh;
    int **firstneigh = list->firstneigh;

    // each thread has its own page allocator
    MyPage<int> *ipage = new MyPage<int>( &(list->ipage[t]) );
    ipage->reset();

    for (int i = ini; i < end; i++) {
      int *neighptr = ipage->vget();

      int    itype = type[i];
      double  xtmp = x[i][0];
      double  ytmp = x[i][1];
      double  ztmp = x[i][2];
      int        n = 0;
      // loop over rest of atoms in i's bin, ghosts are at end of linked list
      // if j is owned atom, store it, since j is beyond i in linked list
      // if j is ghost, only store if j coords are "above and to the right" of i

      for (int j = bins[i]; j >= 0; j = bins[j]) {
        if (j >= nlocal) {
          if (x[j][2] < ztmp) continue;
          if (x[j][2] == ztmp) {
            if (x[j][1] < ytmp) continue;
            if (x[j][1] == ytmp && x[j][0] < xtmp) continue;
          }
        }
        int jtype = type[j];
        if ( exclude && exclusion( i, j, itype, jtype, mask, molecule ) ) continue;

        double delx = xtmp - x[j][0];
        double dely = ytmp - x[j][1];
        double delz = ztmp - x[j][2];
        double rsq = delx*delx + dely*dely + delz*delz;

        if (rsq <= cutneighsq[itype][jtype]) neighptr[n++] = j;
      }
      // loop over all atoms in other bins in stencil, store every pair
      int ibin = atom2bin[i];
      for (int k = 0; k < nstencil; k++) {
        for (int j = binhead[ibin+stencil[k]]; j >= 0; j = bins[j]) {
          int jtype = type[j];
          if ( exclude && exclusion( i, j, itype, jtype, mask, molecule ) ) continue;
          double delx = xtmp - x[j][0];
          double dely = ytmp - x[j][1];
          double delz = ztmp - x[j][2];
          double rsq = delx*delx + dely*dely + delz*delz;

          if (rsq <= cutneighsq[itype][jtype]) neighptr[n++] = j;
        }
      }
      numneigh[i] = n;
      ilist[i] = i;
      firstneigh[i] = neighptr;
      ipage->vgot(n);
      if (ipage->status())
        error->one(FLERR,"Neighbor list overflow, boost neigh_modify one");
    }
    delete ipage;
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif
  list->inum = nlocal;
}
