/* -*- c++ -*- ----------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#ifdef KSPACE_CLASS

KSpaceStyle(pppm/ompss,PPPMOMPSS)

#else

#ifndef LMP_PPPM_OMPSS_H
#define LMP_PPPM_OMPSS_H

#include "pppm.h"
#include "thr_ompss.h"

namespace LAMMPS_NS {

class PPPMOMPSS : public PPPM, public ThrOMPSS {
 public:
  PPPMOMPSS(class LAMMPS *, int, char **);
  ~PPPMOMPSS();
  virtual void setup( );

 protected:
  virtual void allocate();
  virtual void deallocate();
  virtual void compute( int, int );
  virtual void compute_gf_ik();
  virtual void particle_map( );
  virtual void make_rho();
  virtual void brick2fft();
  virtual void poisson_ik();
  virtual void fieldforce_ik();
  void compute_rho1d_thr( const FFT_SCALAR &, const FFT_SCALAR &,
                          const FFT_SCALAR &, FFT_SCALAR ** );

  FFT_SCALAR ***th_rho1d;
  FFT_SCALAR   *th_density;
  int           nworkers;
  int           nx_size;
  int           nz_plane;
};

}

#endif
#endif
