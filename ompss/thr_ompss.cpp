/* -*- c++ -*- ----------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#include <string.h>

#include "thr_ompss.h"
#if _OMPSS == 1 || defined(_OPENMP)
#include "omp.h"
#elif _OMPSS == 2
#include "nanos6/debug.h"
#endif
#include "atom.h"
#include "pair.h"
#include "bond.h"
#include "angle.h"
#include "dihedral.h"
#include "improper.h"
#include "pppm.h"
#include "compute.h"
#include "modify.h"
#include "error.h"
#include "comm.h"
#include "neighbor.h"
#include "accelerator_ompss.h"

#include "math_const.h"

using namespace LAMMPS_NS;
using namespace MathConst;

ThrOMPSS::ThrOMPSS( LAMMPS *ptr ) : lmp(ptr)
{
  // register fix ompss with this class
  int ifix = lmp->modify->find_fix("package_ompss");
  if (ifix < 0)
    lmp->error->all(FLERR,"The 'package ompss' command is required for /ompss styles");
  fix = static_cast<FixOMPSS *>(lmp->modify->fix[ifix]);
}

template <class T>
void ThrOMPSS::init( T *pair, int nmax )
{
  _eng_vdwl = 0.0;
  _eng_coul = 0.0;
  _virial[0] = 0.0;
  _virial[1] = 0.0;
  _virial[2] = 0.0;
  _virial[3] = 0.0;
  _virial[4] = 0.0;
  _virial[5] = 0.0;
# if _OMPSS == 1 || defined(_OPENMP)
  _id = omp_get_thread_num();
# elif _OMPSS == 2
  _id = nanos6_get_current_virtual_cpu( );
# else
  _id = 0;
# endif
  if (nmax) {
    int ind = nmax*_id;
    _eatom = pair->eatom ? pair->eatom + ind : pair->eatom;
    _vatom = pair->vatom ? pair->vatom + ind : pair->vatom;
  } else {
    _eatom = pair->eatom;
    _vatom = pair->vatom;
  }
}
template void ThrOMPSS::init<Pair>( Pair *, int nmax );
template void ThrOMPSS::init<Bond>( Bond *, int nmax );
template void ThrOMPSS::init<Angle>( Angle *, int nmax );
template void ThrOMPSS::init<Dihedral>( Dihedral *, int nmax );
template void ThrOMPSS::init<Improper>( Improper *, int nmax );

void ThrOMPSS::init( Fix *fix, int nmax )
{
  _virial[0] = 0.0;
  _virial[1] = 0.0;
  _virial[2] = 0.0;
  _virial[3] = 0.0;
  _virial[4] = 0.0;
  _virial[5] = 0.0;
# if _OMPSS == 1 || defined(_OPENMP)
  _id = omp_get_thread_num();
# elif _OMPSS == 2
  _id = nanos6_get_current_virtual_cpu( );
# else
  _id = 0;
# endif
  if (nmax) {
    int ind = nmax*_id;
    _vatom = fix->vatom ? fix->vatom + ind : fix->vatom;
  } else {
    _vatom = fix->vatom;
  }
}


/* helper functions */
static void v_tally(double * const vout, const double * const vin)
{
  vout[0] += vin[0];
  vout[1] += vin[1];
  vout[2] += vin[2];
  vout[3] += vin[3];
  vout[4] += vin[4];
  vout[5] += vin[5];
}

static void v_tally(double * const vout, const double scale, const double * const vin)
{
  vout[0] += scale*vin[0];
  vout[1] += scale*vin[1];
  vout[2] += scale*vin[2];
  vout[3] += scale*vin[3];
  vout[4] += scale*vin[4];
  vout[5] += scale*vin[5];
}

void ThrOMPSS::ev_tally_thr( Pair * const pair, const int i, const int j, const int nlocal,
                             const int newton_pair, const double evdwl, const double ecoul,
                             const double fpair, const double delx, const double dely,
                             const double delz )
{

  if (pair->eflag_either)
    e_tally_thr( pair, i, j, nlocal, newton_pair, evdwl, ecoul );

  if (pair->vflag_either) {
    double v[6];
    v[0] = delx*delx*fpair;
    v[1] = dely*dely*fpair;
    v[2] = delz*delz*fpair;
    v[3] = delx*dely*fpair;
    v[4] = delx*delz*fpair;
    v[5] = dely*delz*fpair;

    v_tally_thr( pair, i, j, nlocal, newton_pair, v );
  }

  if (pair->num_tally_compute > 0) {
    // ev_tally callbacks are not thread safe and thus have to be protected
#   if _OMPSS == 1 || defined(_OPENMP)
#   pragma omp critical
#   elif _OMPSS == 2
#   pragma oss critical
#   endif
    {
      for (int k=0; k < pair->num_tally_compute; ++k) {
        Compute *c = pair->list_tally_compute[k];
        c->pair_tally_callback( i, j, nlocal, newton_pair,
                               evdwl, ecoul, fpair, delx, dely, delz );
      }
    }
  }
}

void ThrOMPSS::ev_tally_thr( Bond * const bond, const int i, const int j, const int nlocal,
                             const int newton_pair, const double ebond,
                             const double fpair, const double delx, const double dely,
                             const double delz )
{

  if (bond->eflag_either)
    e_tally_thr( bond, i, j, nlocal, newton_pair, ebond );

  if (bond->vflag_either) {
    double v[6];
    v[0] = delx*delx*fpair;
    v[1] = dely*dely*fpair;
    v[2] = delz*delz*fpair;
    v[3] = delx*dely*fpair;
    v[4] = delx*delz*fpair;
    v[5] = dely*delz*fpair;

    v_tally_thr( bond, i, j, nlocal, newton_pair, v );
  }
}


void ThrOMPSS::ev_tally_thr(Angle * const angle, const int i, const int j, const int k,
                           const int nlocal, const int newton_bond, const double eangle,
                           const double * const f1, const double * const f3,
                           const double delx1, const double dely1, const double delz1,
                           const double delx2, const double dely2, const double delz2 )
{
  if (angle->eflag_either) {
    const double eanglethird = THIRD*eangle;
    if (newton_bond) {
      if (angle->eflag_global)
        _eng_vdwl += eangle;
      if (angle->eflag_atom) {
        _eatom[i] += eanglethird;
        _eatom[j] += eanglethird;
        _eatom[k] += eanglethird;
      }
    } else {
      if (angle->eflag_global) {
        if (i < nlocal) _eng_vdwl += eanglethird;
        if (j < nlocal) _eng_vdwl += eanglethird;
        if (k < nlocal) _eng_vdwl += eanglethird;
      }
      if (angle->eflag_atom) {
        if (i < nlocal) _eatom[i] += eanglethird;
        if (j < nlocal) _eatom[j] += eanglethird;
        if (k < nlocal) _eatom[k] += eanglethird;
      }
    }
  }

  if (angle->vflag_either) {
    double v[6];

    v[0] = delx1*f1[0] + delx2*f3[0];
    v[1] = dely1*f1[1] + dely2*f3[1];
    v[2] = delz1*f1[2] + delz2*f3[2];
    v[3] = delx1*f1[1] + delx2*f3[1];
    v[4] = delx1*f1[2] + delx2*f3[2];
    v[5] = dely1*f1[2] + dely2*f3[2];

    if (angle->vflag_global) {
      if (newton_bond) {
        v_tally(_virial,v);
      } else {
        int cnt = 0;
        if (i < nlocal) ++cnt;
        if (j < nlocal) ++cnt;
        if (k < nlocal) ++cnt;
        v_tally(_virial,cnt*THIRD,v);
      }
    }

    if (angle->vflag_atom) {
      v[0] *= THIRD;
      v[1] *= THIRD;
      v[2] *= THIRD;
      v[3] *= THIRD;
      v[4] *= THIRD;
      v[5] *= THIRD;

      if (newton_bond) {
        v_tally(_vatom[i],v);
        v_tally(_vatom[j],v);
        v_tally(_vatom[k],v);
      } else {
        if (i < nlocal) v_tally(_vatom[i],v);
        if (j < nlocal) v_tally(_vatom[j],v);
        if (k < nlocal) v_tally(_vatom[k],v);
      }
    }
  }
}

void ThrOMPSS::ev_tally_thr( Dihedral * const dihed, const int i1, const int i2,
                              const int i3, const int i4, const int nlocal,
                              const int newton_bond, const double edihedral,
                              const double * const f1, const double * const f3,
                              const double * const f4, const double vb1x,
                              const double vb1y, const double vb1z, const double vb2x,
                              const double vb2y, const double vb2z, const double vb3x,
                              const double vb3y, const double vb3z )
{

  if (dihed->eflag_either) {
    if (dihed->eflag_global) {
      if (newton_bond) {
        _eng_vdwl += edihedral;
      } else {
        const double edihedralquarter = 0.25*edihedral;
        int cnt = 0;
        if (i1 < nlocal) ++cnt;
        if (i2 < nlocal) ++cnt;
        if (i3 < nlocal) ++cnt;
        if (i4 < nlocal) ++cnt;
        _eng_vdwl += static_cast<double>(cnt)*edihedralquarter;
      }
    }
    if (dihed->eflag_atom) {
      const double edihedralquarter = 0.25*edihedral;
      if (newton_bond) {
        _eatom[i1] += edihedralquarter;
        _eatom[i2] += edihedralquarter;
        _eatom[i3] += edihedralquarter;
        _eatom[i4] += edihedralquarter;
      } else {
        if (i1 < nlocal) _eatom[i1] +=  edihedralquarter;
        if (i2 < nlocal) _eatom[i2] +=  edihedralquarter;
        if (i3 < nlocal) _eatom[i3] +=  edihedralquarter;
        if (i4 < nlocal) _eatom[i4] +=  edihedralquarter;
      }
    }
  }

  if (dihed->vflag_either) {
    double v[6];
    v[0] = vb1x*f1[0] + vb2x*f3[0] + (vb3x+vb2x)*f4[0];
    v[1] = vb1y*f1[1] + vb2y*f3[1] + (vb3y+vb2y)*f4[1];
    v[2] = vb1z*f1[2] + vb2z*f3[2] + (vb3z+vb2z)*f4[2];
    v[3] = vb1x*f1[1] + vb2x*f3[1] + (vb3x+vb2x)*f4[1];
    v[4] = vb1x*f1[2] + vb2x*f3[2] + (vb3x+vb2x)*f4[2];
    v[5] = vb1y*f1[2] + vb2y*f3[2] + (vb3y+vb2y)*f4[2];

    if (dihed->vflag_global) {
      if (newton_bond) {
        v_tally(_virial,v);
      } else {
        int cnt = 0;
        if (i1 < nlocal) ++cnt;
        if (i2 < nlocal) ++cnt;
        if (i3 < nlocal) ++cnt;
        if (i4 < nlocal) ++cnt;
        v_tally(_virial,0.25*static_cast<double>(cnt),v);
      }
    }

    v[0] *= 0.25;
    v[1] *= 0.25;
    v[2] *= 0.25;
    v[3] *= 0.25;
    v[4] *= 0.25;
    v[5] *= 0.25;

    if (dihed->vflag_atom) {
      if (newton_bond) {
        v_tally(_vatom[i1],v);
        v_tally(_vatom[i2],v);
        v_tally(_vatom[i3],v);
        v_tally(_vatom[i4],v);
      } else {
        if (i1 < nlocal) v_tally(_vatom[i1],v);
        if (i2 < nlocal) v_tally(_vatom[i2],v);
        if (i3 < nlocal) v_tally(_vatom[i3],v);
        if (i4 < nlocal) v_tally(_vatom[i4],v);
      }
    }
  }
}

void ThrOMPSS::ev_tally_thr( Improper * const imprp, const int i1, const int i2,
                             const int i3, const int i4, const int nlocal,
                             const int newton_bond, const double eimproper,
                             const double * const f1, const double * const f3,
                             const double * const f4, const double vb1x,
                             const double vb1y, const double vb1z, const double vb2x,
                             const double vb2y, const double vb2z, const double vb3x,
                             const double vb3y, const double vb3z )
{

  if (imprp->eflag_either) {
    if (imprp->eflag_global) {
      if (newton_bond) {
        _eng_vdwl += eimproper;
      } else {
        const double eimproperquarter = 0.25*eimproper;
        int cnt = 0;
        if (i1 < nlocal) ++cnt;
        if (i2 < nlocal) ++cnt;
        if (i3 < nlocal) ++cnt;
        if (i4 < nlocal) ++cnt;
        _eng_vdwl += static_cast<double>(cnt)*eimproperquarter;
      }
    }
    if (imprp->eflag_atom) {
      const double eimproperquarter = 0.25*eimproper;
      if (newton_bond) {
        _eatom[i1] += eimproperquarter;
        _eatom[i2] += eimproperquarter;
        _eatom[i3] += eimproperquarter;
        _eatom[i4] += eimproperquarter;
      } else {
        if (i1 < nlocal) _eatom[i1] +=  eimproperquarter;
        if (i2 < nlocal) _eatom[i2] +=  eimproperquarter;
        if (i3 < nlocal) _eatom[i3] +=  eimproperquarter;
        if (i4 < nlocal) _eatom[i4] +=  eimproperquarter;
      }
    }
  }

  if (imprp->vflag_either) {
    double v[6];
    v[0] = vb1x*f1[0] + vb2x*f3[0] + (vb3x+vb2x)*f4[0];
    v[1] = vb1y*f1[1] + vb2y*f3[1] + (vb3y+vb2y)*f4[1];
    v[2] = vb1z*f1[2] + vb2z*f3[2] + (vb3z+vb2z)*f4[2];
    v[3] = vb1x*f1[1] + vb2x*f3[1] + (vb3x+vb2x)*f4[1];
    v[4] = vb1x*f1[2] + vb2x*f3[2] + (vb3x+vb2x)*f4[2];
    v[5] = vb1y*f1[2] + vb2y*f3[2] + (vb3y+vb2y)*f4[2];

    if (imprp->vflag_global) {
      if (newton_bond) {
        v_tally(_virial,v);
      } else {
        int cnt = 0;
        if (i1 < nlocal) ++cnt;
        if (i2 < nlocal) ++cnt;
        if (i3 < nlocal) ++cnt;
        if (i4 < nlocal) ++cnt;
        v_tally(_virial,0.25*static_cast<double>(cnt),v);
      }
    }

    v[0] *= 0.25;
    v[1] *= 0.25;
    v[2] *= 0.25;
    v[3] *= 0.25;
    v[4] *= 0.25;
    v[5] *= 0.25;

    if (imprp->vflag_atom) {
      if (newton_bond) {
        v_tally(_vatom[i1],v);
        v_tally(_vatom[i2],v);
        v_tally(_vatom[i3],v);
        v_tally(_vatom[i4],v);
      } else {
        if (i1 < nlocal) v_tally(_vatom[i1],v);
        if (i2 < nlocal) v_tally(_vatom[i2],v);
        if (i3 < nlocal) v_tally(_vatom[i3],v);
        if (i4 < nlocal) v_tally(_vatom[i4],v);
      }
    }
  }
}




/* ----------------------------------------------------------------------
   tally eng_vdwl and virial into global and per-atom accumulators
   called by AIREBO potential, newton_pair is always on
 ------------------------------------------------------------------------- */

void ThrOMPSS::ev_tally4_thr(Pair * const pair, const int i, const int j,
                             const int k, const int m, const double evdwl,
                             const double * const fi, const double * const fj,
                             const double * const fk, const double * const drim,
                             const double * const drjm, const double * const drkm )
{
  double v[6];

  if (pair->eflag_either) {
    if (pair->eflag_global) _eng_vdwl += evdwl;
    if (pair->eflag_atom) {
      const double epairfourth = 0.25 * evdwl;
      _eatom[i] += epairfourth;
      _eatom[j] += epairfourth;
      _eatom[k] += epairfourth;
      _eatom[m] += epairfourth;
    }
  }

  if (pair->vflag_atom) {
    v[0] = 0.25 * (drim[0]*fi[0] + drjm[0]*fj[0] + drkm[0]*fk[0]);
    v[1] = 0.25 * (drim[1]*fi[1] + drjm[1]*fj[1] + drkm[1]*fk[1]);
    v[2] = 0.25 * (drim[2]*fi[2] + drjm[2]*fj[2] + drkm[2]*fk[2]);
    v[3] = 0.25 * (drim[0]*fi[1] + drjm[0]*fj[1] + drkm[0]*fk[1]);
    v[4] = 0.25 * (drim[0]*fi[2] + drjm[0]*fj[2] + drkm[0]*fk[2]);
    v[5] = 0.25 * (drim[1]*fi[2] + drjm[1]*fj[2] + drkm[1]*fk[2]);

    v_tally(_vatom[i],v);
    v_tally(_vatom[j],v);
    v_tally(_vatom[k],v);
    v_tally(_vatom[m],v);
  }
}


void ThrOMPSS::ev_tally_xyz_thr( Pair * const pair, const int i, const int j,
                                 const int nlocal, const int newton_pair,
                                 const double evdwl, const double ecoul,
                                 const double fx, const double fy, const double fz,
                                 const double delx, const double dely, const double delz )
{

  if (pair->eflag_either)
    e_tally_thr( pair, i, j, nlocal, newton_pair, evdwl, ecoul );

  if (pair->vflag_either) {
    double v[6];
    v[0] = delx*fx;
    v[1] = dely*fy;
    v[2] = delz*fz;
    v[3] = delx*fy;
    v[4] = delx*fz;
    v[5] = dely*fz;

    v_tally_thr( pair, i, j, nlocal, newton_pair, v );
  }
}



void ThrOMPSS::e_tally_thr( Pair * const pair, const int i, const int j,
                            const int nlocal, const int newton_pair,
                            const double evdwl, const double ecoul )
{
  if (pair->eflag_global) {
    if (newton_pair) {
      _eng_vdwl += evdwl;
      _eng_coul += ecoul;
    } else {
      const double evdwlhalf = 0.5*evdwl;
      const double ecoulhalf = 0.5*ecoul;
      if (i < nlocal) {
        _eng_vdwl += evdwlhalf;
        _eng_coul += ecoulhalf;
      }
      if (j < nlocal) {
        _eng_vdwl += evdwlhalf;
        _eng_coul += ecoulhalf;
      }
    }
  }
  if (pair->eflag_atom) {
    const double epairhalf = 0.5 * (evdwl + ecoul);
    if (newton_pair || i < nlocal) _eatom[i] += epairhalf;
    if (newton_pair || j < nlocal) _eatom[j] += epairhalf;
  }
}

void ThrOMPSS::e_tally_thr( Bond * const bond, const int i, const int j,
                            const int nlocal, const int newton_pair,
                            const double ebond )
{
  if (bond->eflag_global) {
    if (newton_pair) {
      _eng_vdwl += ebond;
    } else {
      const double ebondhalf = 0.5*ebond;
      if (i < nlocal) {
        _eng_vdwl += ebondhalf;
      }
      if (j < nlocal) {
        _eng_vdwl += ebondhalf;
      }
    }
  }
  if (bond->eflag_atom) {
    const double ebondhalf = 0.5 * ebond;
    if (newton_pair || i < nlocal) _eatom[i] += ebondhalf;
    if (newton_pair || j < nlocal) _eatom[j] += ebondhalf;
  }
}

template <class T>
void ThrOMPSS::v_tally_thr( T * const pair, const int i, const int j,
                            const int nlocal, const int newton_pair, const double * const v )
{
  if (pair->vflag_global) {
    double * const va = _virial;
    if (newton_pair) {
      v_tally(va,v);
    } else {
      if (i < nlocal) v_tally(va,0.5,v);
      if (j < nlocal) v_tally(va,0.5,v);
    }
  }
  if (pair->vflag_atom) {
    if (newton_pair || i < nlocal) {
      double * const va = _vatom[i];
      v_tally(va,0.5,v);
    }
    if (newton_pair || j < nlocal) {
      double * const va = _vatom[j];
      v_tally(va,0.5,v);
    }
  }
}

template void ThrOMPSS::v_tally_thr<Pair>( Pair * const pair, const int i, const int j,
                            const int nlocal, const int newton_pair, const double * const v );
template void ThrOMPSS::v_tally_thr<Bond>( Bond * const pair, const int i, const int j,
                            const int nlocal, const int newton_pair, const double * const v );

void ThrOMPSS::v_tally_thr( int vglobal, int vatom, int n, int *list, double total, double *v )
{
  int m;

  if (vglobal) {
    double fraction = n/total;
    _virial[0] += fraction*v[0];
    _virial[1] += fraction*v[1];
    _virial[2] += fraction*v[2];
    _virial[3] += fraction*v[3];
    _virial[4] += fraction*v[4];
    _virial[5] += fraction*v[5];
  }

  if (vatom) {
    double fraction = 1.0/total;
    for (int i = 0; i < n; i++) {
      m = list[i];
      _vatom[m][0] += fraction*v[0];
      _vatom[m][1] += fraction*v[1];
      _vatom[m][2] += fraction*v[2];
      _vatom[m][3] += fraction*v[3];
      _vatom[m][4] += fraction*v[4];
      _vatom[m][5] += fraction*v[5];
    }
  }
}


/* ----------------------------------------------------------------------
   tally virial into per-atom accumulators
   called by AIREBO potential, newton_pair is always on
   fpair is magnitude of force on atom I
------------------------------------------------------------------------- */

void ThrOMPSS::v_tally2_thr(const int i, const int j, const double fpair,
                            const double * const drij)
{
  double v[6];

  v[0] = 0.5 * drij[0]*drij[0]*fpair;
  v[1] = 0.5 * drij[1]*drij[1]*fpair;
  v[2] = 0.5 * drij[2]*drij[2]*fpair;
  v[3] = 0.5 * drij[0]*drij[1]*fpair;
  v[4] = 0.5 * drij[0]*drij[2]*fpair;
  v[5] = 0.5 * drij[1]*drij[2]*fpair;

  v_tally(_vatom[i],v);
  v_tally(_vatom[j],v);
}

/* ----------------------------------------------------------------------
   tally virial into per-atom accumulators
   called by AIREBO and Tersoff potential, newton_pair is always on
------------------------------------------------------------------------- */

void ThrOMPSS::v_tally3_thr(const int i, const int j, const int k,
                            const double * const fi, const double * const fj,
                            const double * const drik, const double * const drjk)
{
  double v[6];

  v[0] = THIRD * (drik[0]*fi[0] + drjk[0]*fj[0]);
  v[1] = THIRD * (drik[1]*fi[1] + drjk[1]*fj[1]);
  v[2] = THIRD * (drik[2]*fi[2] + drjk[2]*fj[2]);
  v[3] = THIRD * (drik[0]*fi[1] + drjk[0]*fj[1]);
  v[4] = THIRD * (drik[0]*fi[2] + drjk[0]*fj[2]);
  v[5] = THIRD * (drik[1]*fi[2] + drjk[1]*fj[2]);

  v_tally(_vatom[i],v);
  v_tally(_vatom[j],v);
  v_tally(_vatom[k],v);
}

/* ----------------------------------------------------------------------
   tally virial into per-atom accumulators
   called by AIREBO potential, newton_pair is always on
------------------------------------------------------------------------- */

void ThrOMPSS::v_tally4_thr(const int i, const int j, const int k, const int m,
                            const double * const fi, const double * const fj,
                            const double * const fk, const double * const drim,
                            const double * const drjm, const double * const drkm)
{
  double v[6];

  v[0] = 0.25 * (drim[0]*fi[0] + drjm[0]*fj[0] + drkm[0]*fk[0]);
  v[1] = 0.25 * (drim[1]*fi[1] + drjm[1]*fj[1] + drkm[1]*fk[1]);
  v[2] = 0.25 * (drim[2]*fi[2] + drjm[2]*fj[2] + drkm[2]*fk[2]);
  v[3] = 0.25 * (drim[0]*fi[1] + drjm[0]*fj[1] + drkm[0]*fk[1]);
  v[4] = 0.25 * (drim[0]*fi[2] + drjm[0]*fj[2] + drkm[0]*fk[2]);
  v[5] = 0.25 * (drim[1]*fi[2] + drjm[1]*fj[2] + drkm[1]*fk[2]);

  v_tally(_vatom[i],v);
  v_tally(_vatom[j],v);
  v_tally(_vatom[k],v);
  v_tally(_vatom[m],v);
}

void ThrOMPSS::reduce_thr( Pair *pair, const int eflag, const int vflag )
{
  const int evflag = eflag | vflag;
  if (evflag) {
#   if _OMPSS == 1 || defined(_OPENMP)
#   pragma omp critical
#   elif _OMPSS == 2
#   pragma oss critical
#   endif
    {
      if (eflag & 1) {
        pair->eng_vdwl += _eng_vdwl;
        pair->eng_coul += _eng_coul;
      }
      if (vflag & 3) v_tally( pair->virial, _virial );
    }
  }
}

void ThrOMPSS::reduce_thr( Bond *bond, const int eflag, const int vflag )
{
  const int evflag = eflag | vflag;
  if (evflag) {
#   if _OMPSS == 1 || defined(_OPENMP)
#   pragma omp critical
#   elif _OMPSS == 2
#   pragma oss critical
#   endif
    {
      if (eflag & 1) {
        bond->energy += _eng_vdwl;
      }
      if (vflag & 3) v_tally( bond->virial, _virial );
    }
  }
}

void ThrOMPSS::reduce_thr( Angle *angle, const int eflag, const int vflag )
{
  const int evflag = eflag | vflag;
  if (evflag) {
#   if _OMPSS == 1 || defined(_OPENMP)
#   pragma omp critical
#   elif _OMPSS == 2
#   pragma oss critical
#   endif
    {
      if (eflag & 1) {
        angle->energy += _eng_vdwl;
      }
      if (vflag & 3) v_tally( angle->virial, _virial );
    }
  }
}

void ThrOMPSS::reduce_thr( Dihedral *dihe, const int eflag, const int vflag )
{
  const int evflag = eflag | vflag;
  if (evflag) {
#   if _OMPSS == 1 || defined(_OPENMP)
#   pragma omp critical
#   elif _OMPSS == 2
#   pragma oss critical
#   endif
    {
      if (eflag & 1) {
        dihe->energy += _eng_vdwl;
      }
      if (vflag & 3) v_tally( dihe->virial, _virial );
    }
  }
}

void ThrOMPSS::reduce_thr( Improper *impr, const int eflag, const int vflag )
{
  const int evflag = eflag | vflag;
  if (evflag) {
#   if _OMPSS == 1 || defined(_OPENMP)
#   pragma omp critical
#   elif _OMPSS == 2
#   pragma oss critical
#   endif
    {
      if (eflag & 1) {
        impr->energy += _eng_vdwl;
      }
      if (vflag & 3) v_tally( impr->virial, _virial );
    }
  }
}

void ThrOMPSS::reduce_thr( Fix *fix, const int vflag_global )
{
  if (vflag_global) {
#   if _OMPSS == 1 || defined(_OPENMP)
#   pragma omp critical
#   elif _OMPSS == 2
#   pragma oss critical
#   endif
    {
      v_tally( fix->virial, _virial );
    }
  }
}

void ThrOMPSS::reduce_arrays( Pair *pair )
{
# if _OMPSS == 1
# pragma omp taskwait
# elif _OMPSS == 2
# pragma oss taskwait
# endif
  const bool shared_mem = lmp->ompss->shared_mem;
  bool freduced = shared_mem;
  if (!shared_mem) {
    const int nlocal = lmp->atom->nlocal;
    const int nghost = lmp->atom->nghost;
    const int nall = nlocal + nghost;

    if (pair->eflag_atom) reduce_arr_thr( pair->eatom, 1 );
    if (pair->vflag_atom) reduce_arr_thr( pair->vatom[0], 6 );

    if (pair==fix->last_ompss_style) {
      double **f = lmp->atom->f;
      double **torque = lmp->atom->torque;
      if (f) {
        reduce_arr_thr( f[0], 3 );
        freduced = true;
      }
      if (torque) reduce_arr_thr( torque[0], 3 );
    }
  }
  if (pair->vflag_fdotr) {
    fdotr_compute( pair->virial, freduced );
    // prevent multiple calls to update the virial
    // when a hybrid pair style uses both a gpu and non-gpu pair style
    // or when respa is used with gpu pair styles
    pair->vflag_fdotr = 0;
  }
}

void ThrOMPSS::reduce_arrays( Bond *bond )
{
# if _OMPSS == 1
# pragma omp taskwait
# elif _OMPSS == 2
# pragma oss taskwait
# endif
  const bool shared_mem = lmp->ompss->shared_mem;
  if (!shared_mem) {
    const int nlocal = lmp->atom->nlocal;
    const int nghost = lmp->atom->nghost;
    const int nall = nlocal + nghost;

    if (bond->eflag_atom) reduce_arr_thr( bond->eatom, 1 );
    if (bond->vflag_atom) reduce_arr_thr( bond->vatom[0], 6 );

    if (bond==fix->last_ompss_style) {
      double **f = lmp->atom->f;
      double **torque = lmp->atom->torque;
      if (f) reduce_arr_thr( f[0], 3 );
      if (torque) reduce_arr_thr( torque[0], 3 );
    }
  }
}

void ThrOMPSS::reduce_arrays( Angle *angle )
{
# if _OMPSS == 1
# pragma omp taskwait
# elif _OMPSS == 2
# pragma oss taskwait
# endif
  const bool shared_mem = lmp->ompss->shared_mem;
  if (!shared_mem) {
    const int nlocal = lmp->atom->nlocal;
    const int nghost = lmp->atom->nghost;
    const int nall = nlocal + nghost;

    if (angle->eflag_atom) reduce_arr_thr( angle->eatom, 1 );
    if (angle->vflag_atom) reduce_arr_thr( angle->vatom[0], 6 );

    if (angle==fix->last_ompss_style) {
      double **f = lmp->atom->f;
      double **torque = lmp->atom->torque;
      if (f) reduce_arr_thr( f[0], 3 );
      if (torque) reduce_arr_thr( torque[0], 3 );
    }
  }
}

void ThrOMPSS::reduce_arrays( Dihedral *dihe )
{
# if _OMPSS == 1
# pragma omp taskwait
# elif _OMPSS == 2
# pragma oss taskwait
# endif
  const bool shared_mem = lmp->ompss->shared_mem;
  if (!shared_mem) {
    const int nlocal = lmp->atom->nlocal;
    const int nghost = lmp->atom->nghost;
    const int nall = nlocal + nghost;

    if (dihe->eflag_atom) reduce_arr_thr( dihe->eatom, 1 );
    if (dihe->vflag_atom) reduce_arr_thr( dihe->vatom[0], 6 );

    if (dihe==fix->last_ompss_style) {
      double **f = lmp->atom->f;
      double **torque = lmp->atom->torque;
      if (f) reduce_arr_thr( f[0], 3 );
      if (torque) reduce_arr_thr( torque[0], 3 );
    }
  }
}

void ThrOMPSS::reduce_arrays( Improper *impr )
{
# if _OMPSS == 1
# pragma omp taskwait
# elif _OMPSS == 2
# pragma oss taskwait
# endif
  const bool shared_mem = lmp->ompss->shared_mem;
  if (!shared_mem) {
    const int nlocal = lmp->atom->nlocal;
    const int nghost = lmp->atom->nghost;
    const int nall = nlocal + nghost;

    if (impr->eflag_atom) reduce_arr_thr( impr->eatom, 1 );
    if (impr->vflag_atom) reduce_arr_thr( impr->vatom[0], 6 );

    if (impr==fix->last_ompss_style) {
      double **f = lmp->atom->f;
      double **torque = lmp->atom->torque;
      if (f) reduce_arr_thr( f[0], 3 );
      if (torque) reduce_arr_thr( torque[0], 3 );
    }
  }
}

void ThrOMPSS::reduce_arrays( KSpace *space )
{
# if _OMPSS == 1
# pragma omp taskwait
# elif _OMPSS == 2
# pragma oss taskwait
# endif
  const bool shared_mem = lmp->ompss->shared_mem;
  if (!shared_mem) {
    const int nlocal = lmp->atom->nlocal;
    const int nghost = lmp->atom->nghost;
    const int nall = nlocal + nghost;

    if (space==fix->last_ompss_style) {
      double **f = lmp->atom->f;
      if (f) reduce_arr_thr( f[0], 3 );
    }
  }
}

void ThrOMPSS::reduce_arrays( Fix *fix )
{
# if _OMPSS == 1
# pragma omp taskwait
# elif _OMPSS == 2
# pragma oss taskwait
# endif
  if (!lmp->ompss->shared_mem) {
    const int nlocal = lmp->atom->nlocal;
    const int nghost = lmp->atom->nghost;
    const int nall = nlocal + nghost;
    if (fix->vflag_atom) reduce_arr_thr( fix->vatom[0], 6*nlocal, 6*fix->maxvatom );
    double **f = lmp->atom->f;
    if (f) reduce_arr_thr( f[0], 3*nall, 3*lmp->atom->nmax );
  }
}

void ThrOMPSS::reduce_arr_thr( double *arr, int nf ) {
  int nthreads = lmp->comm->nworkers;
  int ntask = nthreads;
  int nmax = lmp->atom->nmax*nf;
  int nall = lmp->atom->nlocal + lmp->atom->nghost;
  nall = nall*nf;

# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) \
  shared(nall,ntask,arr,nmax,nthreads)
# elif _OMPSS == 2
# pragma oss task for default(none) \
  shared(nall,ntask,arr,nmax,nthreads)
# endif
  for (int t=0; t< ntask; t++) {
    int di = nall/ntask;
    int mo = nall%ntask;
    int ini = t*di + ( t < mo ? t : mo );
    int end = ini + di + ( t < mo ? 1 : 0 );
    double *a = arr + nmax;
    for (int j=1; j< nthreads; j++, a+=nmax)
      for (int k=ini; k< end; k++) {
        arr[k] += a[k];
        a[k] = 0.0;
      }
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif
}

void ThrOMPSS::reduce_arr_thr( double *arr, int n, int nmax ) {
  int nthreads = lmp->comm->nworkers;

  int ntask = nthreads;
# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) \
  shared(n,ntask,arr,nmax,nthreads)
# elif _OMPSS == 2
# pragma oss task for default(none) \
  shared(n,ntask,arr,nmax,nthreads)
# endif
  for (int t=0; t< ntask; t++) {
    int di = n/ntask;
    int mo = n%ntask;
    int ini = t*di + ( t < mo ? t : mo );
    int end = ini + di + ( t < mo ? 1 : 0 );
    double *a = arr + nmax;
    for (int j=1; j< nthreads; j++, a+=nmax)
      for (int k=ini; k< end; k++) {
        arr[k] += a[k];
        a[k] = 0.0;
      }
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif
}

void ThrOMPSS::reduce_arr_thr( int nthreads, int n, FFT_SCALAR *dst, FFT_SCALAR *src ) {
  int ntask = nthreads;
# if   _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) \
  shared(n,ntask,dst,src,nthreads)
# elif _OMPSS == 2
# pragma oss task for default(none) \
  shared(n,ntask,dst,src,nthreads)
# endif
  for (int t=0; t< ntask; t++) {
    FFT_SCALAR *th_src = src;
    int di = n/ntask;
    int mo = n%ntask;
    int ini = t*di + ( t < mo ? t : mo );
    int end = ini + di + ( t < mo ? 1 : 0 );
    for (int i=0; i< nthreads-1; i++, th_src+=n)
      for (int j=ini; j< end; j++) {
        dst[j] += th_src[j];
        th_src[j] = 0.0;
      }
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif
}

void ThrOMPSS::fdotr_compute( double virial[6], bool freduced )
{
  int nthreads = lmp->comm->nworkers;
  int ntask = nthreads;
  int nlocal = lmp->atom->nlocal;
  int nall = nlocal + lmp->atom->nghost;
  double loc_virial[6] = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };

# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) reduction(+:loc_virial) \
  shared(nall,nlocal,ntask,freduced)
# elif _OMPSS == 2
# pragma oss task for default(none) reduction(+:loc_virial) \
  shared(nall,nlocal,ntask,freduced)
# endif
  for (int t=0; t< ntask; t++) {
    int nmax = lmp->atom->nmax;
    int nmid = lmp->neighbor->includegroup ? lmp->atom->nfirst : nall;
    double **x = lmp->atom->x;
    double **f = lmp->atom->f + ( freduced ? 0 : nmax*t);
    int di = nall/ntask;
    int mo = nall%ntask;
    int rini = freduced ? t*di + ( t < mo ? t : mo ) : 0;
    int rend = freduced ? rini + di + ( t < mo ? 1 : 0 ) : nall;

    int end = rend < nmid ? rend : nmid;
    for (int i = rini; i < end; i++) {
      loc_virial[0] += f[i][0]*x[i][0];
      loc_virial[1] += f[i][1]*x[i][1];
      loc_virial[2] += f[i][2]*x[i][2];
      loc_virial[3] += f[i][1]*x[i][0];
      loc_virial[4] += f[i][2]*x[i][0];
      loc_virial[5] += f[i][2]*x[i][1];
    }
    if (rend>nmid) {
      int ini = rini < nlocal ? nlocal : rini;
      for (int i = ini; i < rend; i++) {
        loc_virial[0] += f[i][0]*x[i][0];
        loc_virial[1] += f[i][1]*x[i][1];
        loc_virial[2] += f[i][2]*x[i][2];
        loc_virial[3] += f[i][1]*x[i][0];
        loc_virial[4] += f[i][2]*x[i][0];
        loc_virial[5] += f[i][2]*x[i][1];
      }
    }
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif
  virial[0] += loc_virial[0];
  virial[1] += loc_virial[1];
  virial[2] += loc_virial[2];
  virial[3] += loc_virial[3];
  virial[4] += loc_virial[4];
  virial[5] += loc_virial[5];
}

void ThrOMPSS::reset_arr_thr( int nthreads, double *arr, int nall, int nmax ) {
  int ntask = nthreads;

# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none)  \
  shared(ntask,arr,nmax,nall,nthreads)
# elif _OMPSS == 2
# pragma oss task for default(none) \
  shared(ntask,arr,nmax,nall,nthreads)
# endif
  for (int t=0; t< ntask; t++) {
    int ini, siz;
    if (nmax==0) {
      int di = nall/ntask;
      int mo = nall%ntask;
      ini = t*di + ( t < mo ? t : mo );
      siz = di + ( t < mo ? 1 : 0 );
    } else {
      ini = t*nmax;
      siz = nall;
    }
    memset( arr+ini, 0, siz*sizeof(double));
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif
}

