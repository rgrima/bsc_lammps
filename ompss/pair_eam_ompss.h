/* -*- c++ -*- ----------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#ifdef PAIR_CLASS

PairStyle(eam/ompss,PairEAMOMPSS)

#else

#ifndef LMP_PAIR_EAM_OMPSS_H
#define LMP_PAIR_EAM_OMPSS_H

#include "pair_eam.h"
#include "thr_ompss.h"

namespace LAMMPS_NS {


class PairEAMOMPSS : public PairEAM, public ThrOMPSS {
 public:
  PairEAMOMPSS(class LAMMPS *);
  virtual void compute(int, int);

 private:

  template <int NEWTON_PAIR>
  void eval_rho( const int, double * );

  template <int EFLAG,int NEWTON_PAIR>
  void eval_fp( const int, const int, ThrOMPSS * const );

  template <int EVFLAG, int EFLAG, int NEWTON_PAIR>
  void eval( const int , ThrOMPSS * const );
};

}

#endif
#endif
