/* -*- c++ -*- ----------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#ifdef BOND_CLASS

BondStyle(harmonic/ompss,BondHarmonicOMPSS)

#else

#ifndef LMP_BOND_HARMONIC_OMPSS_H
#define LMP_BOND_HARMONIC_OMPSS_H

#include "bond_harmonic.h"
#include "thr_ompss.h"

namespace LAMMPS_NS {

class BondHarmonicOMPSS : public BondHarmonic, public ThrOMPSS {
 public:
  BondHarmonicOMPSS(class LAMMPS *);
  virtual void compute(int, int);

 private:
  template <int EVFLAG, int EFLAG, int NEWTON_BOND>
  void eval( int t, ThrOMPSS * const thr );
};

}

#endif
#endif
