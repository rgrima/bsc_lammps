/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#include <mpi.h>
#include "ntopo_dihedral_all_ompss.h"
#include "atom.h"
#include "force.h"
#include "domain.h"
#include "update.h"
#include "output.h"
#include "thermo.h"
#include "memory.h"
#include "error.h"
#include "comm.h"

using namespace LAMMPS_NS;

#define DELTA 10000

/* ---------------------------------------------------------------------- */

NTopoDihedralAllOmpss::NTopoDihedralAllOmpss(LAMMPS *lmp) : NTopo(lmp)
{
  allocate_dihedral();
  coun = NULL;
  acum = NULL;
  memory->create(coun,comm->nworkers,"NTopoDihedralAllOmpss:coun");
  memory->create(acum,comm->nworkers,"NTopoDihedralAllOmpss:acum");
}

/* ---------------------------------------------------------------------- */

NTopoDihedralAllOmpss::~NTopoDihedralAllOmpss( )
{
  memory->destroy(acum);
  memory->destroy(coun);
}

/* ---------------------------------------------------------------------- */

void NTopoDihedralAllOmpss::build()
{
  double tt = MPI_Wtime( );

  int ntask = comm->nworkers;
  int nlocal = atom->nlocal;

  int *num_dihedral = atom->num_dihedral;
  int **dihedral_type = atom->dihedral_type;
  tagint **dihedral_atom1 = atom->dihedral_atom1;
  tagint **dihedral_atom2 = atom->dihedral_atom2;
  tagint **dihedral_atom3 = atom->dihedral_atom3;
  tagint **dihedral_atom4 = atom->dihedral_atom4;
  int newton_bond = force->newton_bond;
  int lostbond = output->thermo->lostbond;

  int nmissing = 0;
# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) reduction(+:nmissing) \
  shared(ntask,nlocal,num_dihedral,dihedral_type, \
  dihedral_atom1,dihedral_atom2,dihedral_atom3,dihedral_atom4,lostbond, \
  newton_bond)
# elif _OMPSS == 2
# pragma oss task for default(none) reduction(+:nmissing) \
  shared(ntask,nlocal,num_dihedral,dihedral_type, \
  dihedral_atom1,dihedral_atom2,dihedral_atom3,dihedral_atom4,lostbond, \
  newton_bond)
# endif
  for (int t=0; t< ntask; t++) {
    const int di = nlocal/ntask;
    const int mo = nlocal%ntask;
    const int ini = t*di + ( t < mo ? t : mo );
    const int end = ini + di + ( t < mo ? 1 : 0 );
    int th_ndihe = 0;
    for (int i=ini; i< end; i++) {
      for (int m = 0; m < num_dihedral[i]; m++) {
        int atom1 = atom->map(dihedral_atom1[i][m]);
        int atom2 = atom->map(dihedral_atom2[i][m]);
        int atom3 = atom->map(dihedral_atom3[i][m]);
        int atom4 = atom->map(dihedral_atom4[i][m]);
        if (atom1 == -1 || atom2 == -1 || atom3 == -1 || atom4 == -1) {
          nmissing++;
          if (lostbond == ERROR) {
            char str[128];
            sprintf(str,"Dihedral atoms "
                    TAGINT_FORMAT " " TAGINT_FORMAT " "
                    TAGINT_FORMAT " " TAGINT_FORMAT
                    " missing on proc %d at step " BIGINT_FORMAT,
                    dihedral_atom1[i][m],dihedral_atom2[i][m],
                    dihedral_atom3[i][m],dihedral_atom4[i][m],
                    me,update->ntimestep);
            error->one(FLERR,str);
          }
        } else {
          atom1 = domain->closest_image(i,atom1);
          atom2 = domain->closest_image(i,atom2);
          atom3 = domain->closest_image(i,atom3);
          atom4 = domain->closest_image(i,atom4);
          if (newton_bond || (i <= atom1 && i <= atom2 &&
              i <= atom3 && i <= atom4)) th_ndihe++;
        }
      }
    }
    coun[t] = th_ndihe;
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif

  ndihedrallist = 0;
  for (int t=0; t< ntask; t++) {
    acum[t] = ndihedrallist;
    ndihedrallist+=coun[t];
  }
  if (ndihedrallist > maxdihedral) {
    maxdihedral = DELTA*(ndihedrallist/DELTA) + DELTA;
    memory->grow(dihedrallist,maxdihedral,5,"neigh_topo:dihedrallist");
  }
# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) \
  shared(ntask,nlocal,num_dihedral,dihedral_type, \
  dihedral_atom1,dihedral_atom2,dihedral_atom3,dihedral_atom4,newton_bond)
# elif _OMPSS == 2
# pragma oss task for default(none) \
  shared(ntask,nlocal,num_dihedral,dihedral_type, \
  dihedral_atom1,dihedral_atom2,dihedral_atom3,dihedral_atom4,newton_bond)
# endif
  for (int t=0; t< ntask; t++) {
    const int di = nlocal/ntask;
    const int mo = nlocal%ntask;
    const int ini = t*di + ( t < mo ? t : mo );
    const int end = ini + di + ( t < mo ? 1 : 0 );
    int th_ndihe = acum[t];
    for (int i=ini; i< end; i++) {
      for (int m = 0; m < num_dihedral[i]; m++) {
        int atom1 = atom->map(dihedral_atom1[i][m]);
        int atom2 = atom->map(dihedral_atom2[i][m]);
        int atom3 = atom->map(dihedral_atom3[i][m]);
        int atom4 = atom->map(dihedral_atom4[i][m]);
        if (atom1 == -1 || atom2 == -1 || atom3 == -1 || atom4 == -1)
          continue;
        atom1 = domain->closest_image(i,atom1);
        atom2 = domain->closest_image(i,atom2);
        atom3 = domain->closest_image(i,atom3);
        atom4 = domain->closest_image(i,atom4);
        if (newton_bond || (i <= atom1 && i <= atom2 &&
            i <= atom3 && i <= atom4)) {
          dihedrallist[th_ndihe][0] = atom1;
          dihedrallist[th_ndihe][1] = atom2;
          dihedrallist[th_ndihe][2] = atom3;
          dihedrallist[th_ndihe][3] = atom4;
          dihedrallist[th_ndihe][4] = dihedral_type[i][m];
          th_ndihe++;
        }
      }
    }
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif

  if (cluster_check) dihedral_check(ndihedrallist,dihedrallist);
  if (lostbond == IGNORE) return;

  int all;
  MPI_Allreduce(&nmissing,&all,1,MPI_INT,MPI_SUM,world);
  if (all) {
    char str[128];
    sprintf(str,
            "Dihedral atoms missing at step " BIGINT_FORMAT,update->ntimestep);
    if (me == 0) error->warning(FLERR,str);
  }
}
