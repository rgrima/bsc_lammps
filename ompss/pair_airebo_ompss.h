/* -*- c++ -*- ----------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------
   Contributing author: Axel Kohlmeyer (Temple U)
------------------------------------------------------------------------- */

#ifdef PAIR_CLASS

PairStyle(airebo/omp,PairAIREBOOMPSS)

#else

#ifndef LMP_PAIR_AIREBO_OMPSS_H
#define LMP_PAIR_AIREBO_OMPSS_H

#include "pair_airebo.h"
#include "thr_ompss.h"

namespace LAMMPS_NS {

class PairAIREBOOMPSS : public PairAIREBO, public ThrOMPSS {

 public:
  PairAIREBOOMPSS(class LAMMPS *);

  virtual void compute(int, int);

 protected:
  double bondorder_thr(int i, int j, double rij[3], double rijmag,
                       double VA, int vflag_atom, double * const *f, ThrOMPSS *thr );
  double bondorderLJ_thr(int i, int j, double rij[3], double rijmag,
                         double VA, double rij0[3], double rijmag0,
                         int vflag_atom, double * const * f, ThrOMPSS *thr );

  void FREBO_thr( int t, int evflag, int eflag,
                 int vflag_atom, double *pv0, ThrOMPSS *thr );
  void FLJ_thr( int t, int evflag, int eflag,
               int vflag_atom, double *pv1, ThrOMPSS *thr );
  void TORSION_thr( int it, int evflag, int eflag,
                   double *pv2, ThrOMPSS *thr );
  void REBO_neigh_thr();
};

}

#endif
#endif
