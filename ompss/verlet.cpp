/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#include <string.h>
#include "verlet.h"
#include "neighbor.h"
#include "domain.h"
#include "comm.h"
#include "atom.h"
#include "atom_vec.h"
#include "force.h"
#include "pair.h"
#include "bond.h"
#include "angle.h"
#include "dihedral.h"
#include "improper.h"
#include "kspace.h"
#include "output.h"
#include "update.h"
#include "modify.h"
#include "compute.h"
#include "fix.h"
#include "timer.h"
#include "memory.h"
#include "error.h"
#include "accelerator_ompss.h"

using namespace LAMMPS_NS;

#include <stdlib.h>
#define _RGT_ "n_"
#define NMAX atom->nmax

void printForce( LAMMPS *lmp, const char *fn, int mode=7, int id=-1, int me=-1, bool multi=false ) {
  Atom *atom = lmp->atom;
  Comm *comm = lmp->comm;
  char FN[256];
  if (id==-1)
    if (me==-1) sprintf( FN, _RGT_ "%s", fn );
    else        sprintf( FN, _RGT_ "%s.%02d", fn, me );
  else
    if (me==-1) sprintf( FN, _RGT_ "%s.%03d", fn, id );
    else        sprintf( FN, _RGT_ "%s.%03d.%02d", fn, id, me );
  FILE *fp = fopen( FN, "w" );
  int nall = atom->nlocal + atom->nghost;
  double **f = atom->f;
  double **x = atom->x;
  double **v = atom->v;
  double *q = atom->q;
  double *ff = f[0];

  int nmax = NMAX;
  int nthreads = multi ? comm->nthreads : 1;
  if (mode==2) {
    for (int i=0; i< nall; i++ ) {
      int ii = 3*i;
      for (int t=0; t< nthreads; t++, ii+=3*nmax )
        fprintf( fp, "%d %24.14E %24.14E %24.14E\n", i, ff[ii], ff[ii+1], ff[ii+2] );
    }
    fclose(fp);
    return;
  }
  for (int i=0; i< nall; i+=1 ) {
    fprintf( fp, "%d:", i );
    if (mode&1) fprintf( fp, " %24.14E %24.14E %24.14E", x[i][0], x[i][1], x[i][2] );
    if (mode&2) {
      double frc[3] = { 0.0, 0.0, 0.0 };
      for (int t=0; t< nthreads; t++ ) {
        int ind = 3*(i+t*nmax);
        frc[0] += f[i+t*nmax][0];
        frc[1] += f[i+t*nmax][1];
        frc[2] += f[i+t*nmax][2];
      }
      fprintf( fp, " %24.14E %24.14E %24.14E", frc[0], frc[1], frc[2] );
    }
    if (mode&4)
      fprintf( fp, " %24.14E %24.14E %24.14E", v[i][0], v[i][1], v[i][2] );
    fprintf( fp, "\n" );
  }
  fclose(fp);
}

#define GET_TOTAL false

const char HmemStr[] = "HugetlbPages:";
const char LmemStr[] = "VmRSS:";
char *hpage_mode = NULL;
long *refMem = NULL;
long *newMem = NULL;
int   me, nprocs;

long getprocmem( )
{
  long mem=0;
  char line[256];
  FILE *file = fopen("/proc/self/status", "r");
  while (fgets(line, 256, file) != NULL) {
    if (strncmp(line, hpage_mode, strlen(hpage_mode) ) == 0) {
      sscanf( line, "%*s %ld %*s", &mem );
      break;
    }
  }
  fclose(file);
  return mem;
}

void initMemo( )
{
  MPI_Comm_rank(MPI_COMM_WORLD,&me);
  MPI_Comm_size(MPI_COMM_WORLD,&nprocs);

  char *HPAGE = getenv("XOS_MMM_L_HPAGE_TYPE");
  hpage_mode = (char *)( HPAGE && strcmp( HPAGE, "disable" ) == 0 ? LmemStr : HmemStr);
  long mem=0;
  while (!mem) {
    mem = getprocmem( );
    if (!mem) hpage_mode = (char *)LmemStr;
  }
  refMem = new long[nprocs];
  newMem = new long[nprocs];
}


void getmemory( const char *msg, int dst=0 )
{
  if (!hpage_mode) initMemo( );
  long mem=getprocmem( );
  long Tmem;
  MPI_Reduce( &mem, &Tmem, 1, MPI_LONG, MPI_SUM, dst, MPI_COMM_WORLD );
  if (me==dst) {
     if (GET_TOTAL) {
       long Gmem_ref, Gmem;
       FILE *file = fopen("/proc/meminfo", "r");
       char line[256];
       fgets(line, 256, file); // MemTotal
       char *s = getenv("MEMAVAILABLE");
       if (s) sscanf( s, "%ld", &Gmem_ref );
       else sscanf( line, "%*s %ld %*s", &Gmem_ref );
       fgets(line, 256, file); // MemFree
       fgets(line, 256, file); // MemAvailable
       sscanf( line, "%*s %ld %*s", &Gmem );
       printf( "MEMORY %20s %12ld == %12ld\n", msg, Tmem, Gmem_ref-Gmem );
       fclose(file);
    } else printf( "%s %12ld Mb  loc=%12ld\n", msg, Tmem/1024, mem );
    fflush(stdout);
  }
}
void getmemory_loc( int mode )
{
  if (!hpage_mode) initMemo( );
  long mem=getprocmem( );
  if (mode==0) {
    MPI_Gather( &mem, 1, MPI_LONG, refMem, 1, MPI_LONG, 0, MPI_COMM_WORLD );
    long Tmem;
    MPI_Reduce( &mem, &Tmem, 1, MPI_LONG, MPI_SUM, 0, MPI_COMM_WORLD );
  }
  else {
    MPI_Gather( &mem, 1, MPI_LONG, newMem, 1, MPI_LONG, 0, MPI_COMM_WORLD );
    if (!me) {
      int n=0;
      for (int i=0; i< nprocs; i++)
        if (newMem[i]!=refMem[i]) {
          printf( "CH [%d] %ld => %ld\n", i, refMem[i], newMem[i] );
          n++;
        }
      printf( "MEMORY CHANGE @ %d\n", n );
    }
  }
  return;
}

/* ---------------------------------------------------------------------- */

Verlet::Verlet(LAMMPS *lmp, int narg, char **arg) :
  Integrate(lmp, narg, arg) {}

/* ----------------------------------------------------------------------
   initialization before run
------------------------------------------------------------------------- */

void Verlet::init()
{
  Integrate::init();

  // warn if no fixes

  if (modify->nfix == 0 && comm->me == 0)
    error->warning(FLERR,"No fixes defined, atoms won't move");

  // virial_style:
  // 1 if computed explicitly by pair->compute via sum over pair interactions
  // 2 if computed implicitly by pair->virial_fdotr_compute via sum over ghosts

  if (force->newton_pair) virial_style = 2;
  else virial_style = 1;

  // setup lists of computes for global and per-atom PE and pressure

  ev_setup();

  // detect if fix omp is present for clearing force arrays

  int ifix = modify->find_fix("package_omp");
  if (ifix >= 0) external_force_clear = 1;

  // detect if fix omp is present for clearing force arrays

  ifix = modify->find_fix("package_ompss");
  if (ifix >= 0) external_force_clear = 1;

  // set flags for arrays to clear in force_clear()

  torqueflag = extraflag = 0;
  if (atom->torque_flag) torqueflag = 1;
  if (atom->avec->forceclearflag) extraflag = 1;

  // orthogonal vs triclinic simulation box

  triclinic = domain->triclinic;
}

/* ----------------------------------------------------------------------
   setup before run
------------------------------------------------------------------------- */

void Verlet::setup(int flag)
{
  if (comm->me == 0 && screen) {
    fprintf(screen,"Setting up Verlet run ...\n");
    if (flag) {
      fprintf(screen,"  Unit style    : %s\n",update->unit_style);
      fprintf(screen,"  Current step  : " BIGINT_FORMAT "\n",update->ntimestep);
      fprintf(screen,"  Time step     : %g\n",update->dt);
      timer->print_timeout(screen);
    }
  }
  if (lmp->kokkos)
    error->all(FLERR,"KOKKOS package requires run_style verlet/kk");

  update->setupflag = 1;

  // setup domain, communication and neighboring
  // acquire ghosts
  // build neighbor lists

  atom->setup();

  modify->setup_pre_exchange();
  if (triclinic) domain->x2lamda(atom->nlocal);
  domain->pbc();
  domain->reset_box();

  comm->setup();

  if (neighbor->style) neighbor->setup_bins();
  comm->exchange();

  if (atom->sortfreq > 0) atom->sort();

  comm->borders();
  if (triclinic) domain->lamda2x(atom->nlocal+atom->nghost);
  domain->image_check();
  domain->box_too_small_check();
  modify->setup_pre_neighbor();
  neighbor->build(1);
  modify->setup_post_neighbor();

  neighbor->ncalls = 0;

#if defined(_OMPSS) || defined(_OPENMP)
  if (ompss) ompss->build( );
#endif
  // compute all forces

  force->setup();
  ev_set(update->ntimestep);
  force_clear();

  modify->setup_pre_force(vflag);

  if (pair_compute_flag) force->pair->compute(eflag,vflag);
  else if (force->pair) force->pair->compute_dummy(eflag,vflag);

  if (atom->molecular) {
    if (force->bond) force->bond->compute(eflag,vflag);
    if (force->angle) force->angle->compute(eflag,vflag);
    if (force->dihedral) force->dihedral->compute(eflag,vflag);
    if (force->improper) force->improper->compute(eflag,vflag);
  }

  if (force->kspace) {
    force->kspace->setup();
    if (kspace_compute_flag) force->kspace->compute(eflag,vflag);
    else force->kspace->compute_dummy(eflag,vflag);
  }

  modify->setup_pre_reverse(eflag,vflag);
  if (force->newton) comm->reverse_comm();

  modify->setup(vflag);

  output->setup(flag);
  update->setupflag = 0;

  if (false) printForce( lmp, "setup", 7, -1, comm->me );
}

/* ----------------------------------------------------------------------
   setup without output
   flag = 0 = just force calculation
   flag = 1 = reneighbor and force calculation
------------------------------------------------------------------------- */

void Verlet::setup_minimal(int flag)
{
  update->setupflag = 1;

  // setup domain, communication and neighboring
  // acquire ghosts
  // build neighbor lists

  if (flag) {
    modify->setup_pre_exchange();
    if (triclinic) domain->x2lamda(atom->nlocal);
    domain->pbc();
    domain->reset_box();
    comm->setup();
    if (neighbor->style) neighbor->setup_bins();
    comm->exchange();
    comm->borders();
    if (triclinic) domain->lamda2x(atom->nlocal+atom->nghost);
    domain->image_check();
    domain->box_too_small_check();
    modify->setup_pre_neighbor();
    neighbor->build(1);
    modify->setup_post_neighbor();
    neighbor->ncalls = 0;
  }

  // compute all forces

  ev_set(update->ntimestep);
  force_clear();
  modify->setup_pre_force(vflag);

  if (pair_compute_flag) force->pair->compute(eflag,vflag);
  else if (force->pair) force->pair->compute_dummy(eflag,vflag);

  if (atom->molecular) {
    if (force->bond) force->bond->compute(eflag,vflag);
    if (force->angle) force->angle->compute(eflag,vflag);
    if (force->dihedral) force->dihedral->compute(eflag,vflag);
    if (force->improper) force->improper->compute(eflag,vflag);
  }

  if (force->kspace) {
    force->kspace->setup();
    if (kspace_compute_flag) force->kspace->compute(eflag,vflag);
    else force->kspace->compute_dummy(eflag,vflag);
  }

  modify->setup_pre_reverse(eflag,vflag);
  if (force->newton) comm->reverse_comm();

  modify->setup(vflag);
  update->setupflag = 0;
}

/* ----------------------------------------------------------------------
   run for N steps
------------------------------------------------------------------------- */
void Verlet::run(int n)
{
  bigint ntimestep;
  int nflag,sortflag;

  int n_post_integrate = modify->n_post_integrate;
  int n_pre_exchange = modify->n_pre_exchange;
  int n_pre_neighbor = modify->n_pre_neighbor;
  int n_post_neighbor = modify->n_post_neighbor;
  int n_pre_force = modify->n_pre_force;
  int n_pre_reverse = modify->n_pre_reverse;
  int n_post_force = modify->n_post_force;
  int n_end_of_step = modify->n_end_of_step;

  if (atom->sortfreq > 0) sortflag = 1;
  else sortflag = 0;

  double t0 = MPI_Wtime( );
  for (int i = 0; i < n; i++) {
    if (false)
    {
      char str[32];
      sprintf( str, "ITER %4d", i );
      getmemory( str );
    }
    double tt = MPI_Wtime( );
    if (timer->check_timeout(i)) {
      update->nsteps = i;
      break;
    }

    ntimestep = ++update->ntimestep;
    ev_set(ntimestep);

    // initial time integration
    timer->stamp();

    modify->initial_integrate(vflag);

    if (n_post_integrate) modify->post_integrate();
    timer->stamp(Timer::MODIFY);

    // regular communication vs neighbor list rebuild
    nflag = neighbor->decide();


    if (nflag == 0) {
      timer->stamp();
      comm->forward_comm();
      timer->stamp(Timer::COMM);
    } else {
      if (n_pre_exchange) {
        timer->stamp();
        modify->pre_exchange();
        timer->stamp(Timer::MODIFY);
      }

      if (triclinic) domain->x2lamda(atom->nlocal);

      domain->pbc();
      if (domain->box_change) {
        domain->reset_box();
        comm->setup();
        if (neighbor->style)  neighbor->setup_bins();
      }
      timer->stamp();

      comm->exchange();

      if (sortflag && ntimestep >= atom->nextsort) atom->sort();

      comm->borders();

      if (triclinic) domain->lamda2x(atom->nlocal+atom->nghost);

      timer->stamp(Timer::COMM);
      if (n_pre_neighbor) {
        modify->pre_neighbor();
        timer->stamp(Timer::MODIFY);
      }

      neighbor->build(1);
      timer->stamp(Timer::NEIGH);

      if (n_post_neighbor) {
        modify->post_neighbor();
        timer->stamp(Timer::MODIFY);
      }

#if defined(_OMPSS) || defined(_OPENMP)
      if (ompss) ompss->build( );
#endif
    }

    // force computations
    // important for pair to come before bonded contributions
    // since some bonded potentials tally pairwise energy/virial
    // and Pair:ev_tally() needs to be called before any tallying

    force_clear();
    timer->stamp();

    if (n_pre_force) {
      modify->pre_force(vflag);
      timer->stamp(Timer::MODIFY);
    }

    if (pair_compute_flag) {
      force->pair->compute(eflag,vflag);
      timer->stamp(Timer::PAIR);
    }

    if (atom->molecular) {
      if (force->bond) force->bond->compute(eflag,vflag);
      if (force->angle) force->angle->compute(eflag,vflag);
      if (force->dihedral) force->dihedral->compute(eflag,vflag);
      if (force->improper) force->improper->compute(eflag,vflag);
      timer->stamp(Timer::BOND);
    }

    if (kspace_compute_flag) {
      force->kspace->compute(eflag,vflag);
      timer->stamp(Timer::KSPACE);
    }

    if (n_pre_reverse) {
      modify->pre_reverse(eflag,vflag);
      timer->stamp(Timer::MODIFY);
    }

     // reverse communication of forces
    if (force->newton) {
      comm->reverse_comm();
      timer->stamp(Timer::COMM);
    }

     // force modifications, final time integration, diagnostics
    if (n_post_force) modify->post_force(vflag);

    modify->final_integrate();

    if (n_end_of_step) modify->end_of_step();
    timer->stamp(Timer::MODIFY);

    // all output

    if (false && !comm->me) printf( "[%6d] %s TIME_cycle=%lf\n", i, nflag==0 ? "REG" : "REB", MPI_Wtime( )-tt );
    if (ntimestep == output->next) {
      timer->stamp();
      output->write(ntimestep);
      timer->stamp(Timer::OUTPUT);
    }
    if (false && (i+1)%10==0) printForce( lmp, "iter", 3, i, comm->me );
  }
  if (false) printForce( lmp, "run", 3, -1, comm->me );
  if (!comm->me) printf( "TIME_total=%lf\n", MPI_Wtime( )-t0 );
  if (true) getmemory( "MEMO_total=" );
}

/* ---------------------------------------------------------------------- */

void Verlet::cleanup()
{
  modify->post_run();
  domain->box_too_small_check();
  update->update_time();
}

/* ----------------------------------------------------------------------
   clear force on own & ghost atoms
   clear other arrays as needed
------------------------------------------------------------------------- */

void Verlet::force_clear()
{
  size_t nbytes;

  if (external_force_clear) return;

  // clear force on all particles
  // if either newton flag is set, also include ghosts
  // when using threads always clear all forces.

  int nlocal = atom->nlocal;
  if (neighbor->includegroup == 0) {

    nbytes = sizeof(double) * nlocal;
    if (force->newton) nbytes += sizeof(double) * atom->nghost;

    if (nbytes) {
      memset(&atom->f[0][0],0,3*nbytes);
      if (torqueflag) memset( atom->torque[0], 0, 3*nbytes );
      if (extraflag) atom->avec->force_clear(0,nbytes);
    }

  // neighbor includegroup flag is set
  // clear force only on initial nfirst particles
  // if either newton flag is set, also include ghosts

  } else {
    nbytes = sizeof(double) * atom->nfirst;

    if (nbytes) {
      memset(&atom->f[0][0],0,3*nbytes);
      if (torqueflag) memset(&atom->torque[0][0],0,3*nbytes);
      if (extraflag) atom->avec->force_clear(0,nbytes);
    }

    if (force->newton) {
      nbytes = sizeof(double) * atom->nghost;

      if (nbytes) {
        memset(&atom->f[nlocal][0],0,3*nbytes);
        if (torqueflag) memset(&atom->torque[nlocal][0],0,3*nbytes);
        if (extraflag) atom->avec->force_clear(nlocal,nbytes);
      }
    }
  }
}
