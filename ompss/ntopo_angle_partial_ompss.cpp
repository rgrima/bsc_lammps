/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#include <mpi.h>
#include "ntopo_angle_partial_ompss.h"
#include "atom.h"
#include "force.h"
#include "domain.h"
#include "update.h"
#include "output.h"
#include "thermo.h"
#include "memory.h"
#include "error.h"
#include "comm.h"

using namespace LAMMPS_NS;

#define DELTA 10000

/* ---------------------------------------------------------------------- */

NTopoAnglePartialOmpss::NTopoAnglePartialOmpss(LAMMPS *lmp) : NTopo(lmp)
{
  allocate_angle();
  coun = NULL;
  acum = NULL;
  memory->create(coun,comm->nworkers,"NTopoAnglePartialOmpss:coun");
  memory->create(acum,comm->nworkers,"NTopoAnglePartialOmpss:acum");
}

/* ---------------------------------------------------------------------- */

NTopoAnglePartialOmpss::~NTopoAnglePartialOmpss( )
{
  memory->destroy(acum);
  memory->destroy(coun);
}

/* ---------------------------------------------------------------------- */

void NTopoAnglePartialOmpss::build()
{
  double tt = MPI_Wtime( );

  int ntask = comm->nworkers;
  int nlocal = atom->nlocal;

  int *num_angle = atom->num_angle;
  int **angle_type = atom->angle_type;
  tagint **angle_atom1 = atom->angle_atom1;
  tagint **angle_atom2 = atom->angle_atom2;
  tagint **angle_atom3 = atom->angle_atom3;
  int newton_bond = force->newton_bond;
  int lostbond = output->thermo->lostbond;

  int nmissing = 0;
# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) reduction(+:nmissing) \
  shared(ntask,nlocal,num_angle,angle_type, \
  angle_atom1,angle_atom2,angle_atom3,lostbond,newton_bond)
# elif _OMPSS == 2
# pragma oss task for default(none) reduction(+:nmissing) \
  shared(ntask,nlocal,num_angle,angle_type, \
  angle_atom1,angle_atom2,angle_atom3,lostbond,newton_bond)
# endif
  for (int t=0; t< ntask; t++) {
    const int di = nlocal/ntask;
    const int mo = nlocal%ntask;
    const int ini = t*di + ( t < mo ? t : mo );
    const int end = ini + di + ( t < mo ? 1 : 0 );
    int th_nangl = 0;
    for (int i=ini; i< end; i++) {
      for (int m = 0; m < num_angle[i]; m++) {
        if (angle_type[i][m] <= 0) continue;
        int atom1 = atom->map(angle_atom1[i][m]);
        int atom2 = atom->map(angle_atom2[i][m]);
        int atom3 = atom->map(angle_atom3[i][m]);
        if (atom1 == -1 || atom2 == -1 || atom3 == -1) {
          nmissing++;
          if (lostbond == ERROR) {
            char str[128];
            sprintf(str,"Angle atoms "
                    TAGINT_FORMAT " " TAGINT_FORMAT " " TAGINT_FORMAT
                    " missing on proc %d at step " BIGINT_FORMAT,
                    angle_atom1[i][m],angle_atom2[i][m],angle_atom3[i][m],
                    me,update->ntimestep);
            error->one(FLERR,str);
          }
          continue;
        }
        atom1 = domain->closest_image(i,atom1);
        atom2 = domain->closest_image(i,atom2);
        atom3 = domain->closest_image(i,atom3);
        if (newton_bond || (i <= atom1 && i <= atom2 && i <= atom3)) th_nangl++;
      }
    }
    coun[t] = th_nangl;
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif

  nanglelist = 0;
  for (int t=0; t< ntask; t++) {
    acum[t] = nanglelist;
    nanglelist+=coun[t];
  }
  if (nanglelist > maxangle) {
    maxangle = DELTA*(nanglelist/DELTA) + DELTA;
    memory->grow(anglelist,maxangle,4,"neigh_topo:anglelist");
  }
# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) reduction(+:nmissing) \
  shared(ntask,nlocal,num_angle ,angle_type, \
  angle_atom1,angle_atom2,angle_atom3,lostbond,newton_bond)
# elif _OMPSS == 2
# pragma oss task for default(none) reduction(+:nmissing) \
  shared(ntask,nlocal,num_angle ,angle_type, \
  angle_atom1,angle_atom2,angle_atom3,lostbond,newton_bond)
# endif
  for (int t=0; t< ntask; t++) {
    const int di = nlocal/ntask;
    const int mo = nlocal%ntask;
    const int ini = t*di + ( t < mo ? t : mo );
    const int end = ini + di + ( t < mo ? 1 : 0 );
    int th_nangl = acum[t];
    for (int i=ini; i< end; i++) {
      for (int m = 0; m < num_angle[i]; m++) {
        if (angle_type[i][m] <= 0) continue;
        int atom1 = atom->map(angle_atom1[i][m]);
        int atom2 = atom->map(angle_atom2[i][m]);
        int atom3 = atom->map(angle_atom3[i][m]);
        if (atom1 == -1 || atom2 == -1 || atom3 == -1) continue;
        atom1 = domain->closest_image(i,atom1);
        atom2 = domain->closest_image(i,atom2);
        atom3 = domain->closest_image(i,atom3);
        if (newton_bond || (i <= atom1 && i <= atom2 && i <= atom3)) {
          anglelist[th_nangl][0] = atom1;
          anglelist[th_nangl][1] = atom2;
          anglelist[th_nangl][2] = atom3;
          anglelist[th_nangl][3] = angle_type[i][m];
          th_nangl++;
        }
      }
    }
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif

  if (cluster_check) angle_check();
  if (lostbond == IGNORE) return;

  int all;
  MPI_Allreduce(&nmissing,&all,1,MPI_INT,MPI_SUM,world);
  if (all) {
    char str[128];
    sprintf(str,
            "Angle atoms missing at step " BIGINT_FORMAT,update->ntimestep);
    if (me == 0) error->warning(FLERR,str);
  }
}
