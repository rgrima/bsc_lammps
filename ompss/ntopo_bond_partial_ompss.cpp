/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#include <mpi.h>
#include <stdlib.h>
#include "ntopo_bond_partial_ompss.h"
#include "atom.h"
#include "force.h"
#include "domain.h"
#include "update.h"
#include "output.h"
#include "thermo.h"
#include "memory.h"
#include "error.h"
#include "comm.h"

using namespace LAMMPS_NS;

#define DELTA 10000

/* ---------------------------------------------------------------------- */

NTopoBondPartialOmpss::NTopoBondPartialOmpss(LAMMPS *lmp) : NTopoBondPartial(lmp)
{
  coun = NULL;
  acum = NULL;
  memory->create(coun,comm->nworkers,"NTopoBondPartialOmpss:coun");
  memory->create(acum,comm->nworkers,"NTopoBondPartialOmpss:acum");
}

/* ---------------------------------------------------------------------- */

NTopoBondPartialOmpss::~NTopoBondPartialOmpss( )
{
  memory->destroy(acum);
  memory->destroy(coun);
}

/* ---------------------------------------------------------------------- */

void NTopoBondPartialOmpss::build()
{
  double tt = MPI_Wtime( );

  int ntask = comm->nworkers;
  int nlocal = atom->nlocal;
  const int *num_bond = atom->num_bond;
  int newton_bond = force->newton_bond;
  int ** _noalias bond_type = atom->bond_type;
  tagint * * _noalias bond_atom = atom->bond_atom;
  int lostbond = output->thermo->lostbond;
  tagint *tag = atom->tag;

  int nmissing = 0;
# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) reduction(+:nmissing) \
  shared(ntask,nlocal,num_bond,newton_bond, \
  bond_type,bond_atom,lostbond,tag)
# elif   _OMPSS == 2
# pragma oss task for default(none) reduction(+:nmissing) \
  shared(ntask,nlocal,num_bond,newton_bond, \
  bond_type,bond_atom,lostbond,tag)
# endif
  for (int t=0; t< ntask; t++) {
    const int di = nlocal/ntask;
    const int mo = nlocal%ntask;
    const int ini = t*di + ( t < mo ? t : mo );
    const int end = ini + di + ( t < mo ? 1 : 0 );
    int th_nbond = 0;
    for (int i=ini; i< end; i++) {
      for (int m = 0; m < num_bond[i]; m++) {
        if (bond_type[i][m] <= 0) continue;
        int atom1 = atom->map(bond_atom[i][m]);
        if (atom1 == -1) {
          nmissing++;
          if (lostbond == ERROR) {
            char str[128];
            sprintf(str,"Bond atoms " TAGINT_FORMAT " " TAGINT_FORMAT
                    " missing on proc %d at step " BIGINT_FORMAT,
                    tag[i],bond_atom[i][m],me,update->ntimestep);
            error->one(FLERR,str);
          }
        } else {
          atom1 = domain->closest_image(i,atom1);
          if (newton_bond || i < atom1) th_nbond++;
        }
      }
    }
    coun[t] = th_nbond;
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif
  nbondlist = 0;
  for (int t=0; t< ntask; t++) {
    acum[t] = nbondlist;
    nbondlist+=coun[t];
  }
  if (nbondlist > maxbond) {
    maxbond = DELTA*(nbondlist/DELTA) + DELTA;
    memory->grow(bondlist,maxbond,3,"neigh_topo:bondlist");
  }
# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) \
  shared(ntask,nlocal,num_bond,newton_bond, \
  bond_type,bond_atom)
# elif _OMPSS == 2
# pragma oss task for default(none) \
  shared(ntask,nlocal,num_bond,newton_bond, \
  bond_type,bond_atom)
# endif
  for (int t=0; t< ntask; t++) {
    const int di = nlocal/ntask;
    const int mo = nlocal%ntask;
    const int ini = t*di + ( t < mo ? t : mo );
    const int end = ini + di + ( t < mo ? 1 : 0 );
    int th_nbond = acum[t];
    for (int i=ini; i< end; i++) {
      for (int m = 0; m < num_bond[i]; m++) {
        if (bond_type[i][m] <= 0) continue;
        int atom1 = atom->map(bond_atom[i][m]);
        if (atom1 == -1) continue;
        atom1 = domain->closest_image(i,atom1);
        if (newton_bond || i < atom1) {
          bondlist[th_nbond][0] = i;
          bondlist[th_nbond][1] = atom1;
          bondlist[th_nbond][2] = bond_type[i][m];
          th_nbond++;
        }
      }
    }
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif

  if (cluster_check) bond_check();
  if (lostbond == IGNORE) return;

  int all;
  MPI_Allreduce(&nmissing,&all,1,MPI_INT,MPI_SUM,world);
  if (all) {
    char str[128];
    sprintf(str,
            "Bond atoms missing at step " BIGINT_FORMAT,update->ntimestep);
    if (me == 0) error->warning(FLERR,str);
  }
}
