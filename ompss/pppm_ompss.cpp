/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------
   Contributing authors: Rogeli Grima
------------------------------------------------------------------------- */

#include "pppm_ompss.h"
#include "atom.h"
#include "comm.h"
#include "gridcomm.h"
#include "domain.h"
#include "error.h"
#include "force.h"
#include "memory.h"
#include "math_const.h"
#include "math_special.h"
#if _OMPSS == 1 || defined(_OPENMP)
#include "omp.h"
#elif _OMPSS == 2
#include "nanos6/debug.h"
#endif
#include "remap_wrap.h"
#include "fft3d_wrap.h"

#include "fix_ompss.h"
#include "accelerator_ompss.h"

#include <string.h>
#include <math.h>

#include "suffix.h"
using namespace LAMMPS_NS;
using namespace MathConst;
using namespace MathSpecial;

#define MAXORDER 7
#define OFFSET 16384
#define LARGE 10000.0
#define SMALL 0.00001
#define EPS_HOC 1.0e-7

enum{REVERSE_RHO};
enum{FORWARD_IK,FORWARD_AD,FORWARD_IK_PERATOM,FORWARD_AD_PERATOM};

#ifdef FFT_SINGLE
#define ZEROF 0.0f
#define ONEF  1.0f
#else
#define ZEROF 0.0
#define ONEF  1.0
#endif

/* ---------------------------------------------------------------------- */

PPPMOMPSS::PPPMOMPSS(LAMMPS *lmp, int narg, char **arg) :
  PPPM(lmp, narg, arg), ThrOMPSS(lmp)
{
  suffix_flag |= Suffix::OMPSS;
  th_rho1d   = NULL;
  th_density = NULL;
  nworkers   = 0;
  nx_size    = 0;
  nz_plane   = 0;
}

/* ---------------------------------------------------------------------- */

PPPMOMPSS::~PPPMOMPSS()
{
  deallocate();
}

/* ----------------------------------------------------------------------
   adjust PPPM coeffs, called initially and whenever volume has changed
------------------------------------------------------------------------- */

void PPPMOMPSS::setup()
{
  if (triclinic) {
    setup_triclinic();
    return;
  }

  // perform some checks to avoid illegal boundaries with read_data

  if (slabflag == 0 && domain->nonperiodic > 0)
    error->all(FLERR,"Cannot use nonperiodic boundaries with PPPM");
  if (slabflag) {
    if (domain->xperiodic != 1 || domain->yperiodic != 1 ||
        domain->boundary[2][0] != 1 || domain->boundary[2][1] != 1)
      error->all(FLERR,"Incorrect boundaries with slab PPPM");
  }

  double *prd;

  // volume-dependent factors
  // adjust z dimension for 2d slab PPPM
  // z dimension for 3d PPPM is zprd since slab_volfactor = 1.0

  if (triclinic == 0) prd = domain->prd;
  else prd = domain->prd_lamda;

  double xprd = prd[0];
  double yprd = prd[1];
  double zprd = prd[2];
  double zprd_slab = zprd*slab_volfactor;
  volume = xprd * yprd * zprd_slab;

  delxinv = nx_pppm/xprd;
  delyinv = ny_pppm/yprd;
  delzinv = nz_pppm/zprd_slab;

  delvolinv = delxinv*delyinv*delzinv;

  double unitkx = (MY_2PI/xprd);
  double unitky = (MY_2PI/yprd);
  double unitkz = (MY_2PI/zprd_slab);

  // fkx,fky,fkz for my FFT grid pts


# if _OMPSS == 1
# pragma omp task default(none) shared(unitkx)
# elif _OMPSS == 2
# pragma oss task default(none) shared(unitkx)
# elif defined(_OPENMP)
# pragma omp parallel for default(none) shared(unitkx)
# endif
  for (int i = nxlo_fft; i <= nxhi_fft; i++) {
    double per = i - nx_pppm*(2*i/nx_pppm);
    fkx[i] = unitkx*per;
  }

# if _OMPSS == 1
# pragma omp task default(none) shared(unitky)
# elif _OMPSS == 2
# pragma oss task default(none) shared(unitky)
# elif defined(_OPENMP)
# pragma omp parallel for default(none) shared(unitky)
# endif
  for (int i = nylo_fft; i <= nyhi_fft; i++) {
    double per = i - ny_pppm*(2*i/ny_pppm);
    fky[i] = unitky*per;
  }

# if   _OMPSS == 1
# pragma omp task default(none) shared(unitkz)
# elif _OMPSS == 2
# pragma oss task default(none) shared(unitkz)
# elif defined(_OPENMP)
# pragma omp parallel for default(none) shared(unitkz)
# endif
  for (int i = nzlo_fft; i <= nzhi_fft; i++) {
    double per = i - nz_pppm*(2*i/nz_pppm);
    fkz[i] = unitkz*per;
  }

# if   _OMPSS == 1
# pragma omp taskwait
# elif _OMPSS == 2
# pragma oss taskwait
# endif

  // virial coefficients
  int nz_fft = nzhi_fft - nzlo_fft + 1;
  int ny_fft = nyhi_fft - nylo_fft + 1;
  int nx_fft = nxhi_fft - nxlo_fft + 1;
  int niters_fft = nx_fft*ny_fft*nz_fft;
  int ntask = comm->nworkers;
  int st = niters_fft/ntask;
  int mo = niters_fft%ntask;
# if   _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) \
  shared(st,mo,ntask,nx_fft,ny_fft,nz_fft)
# elif _OMPSS == 2
# pragma oss task for default(none) \
  shared(st,mo,ntask,nx_fft,ny_fft,nz_fft)
# endif
  for (int t=0; t< ntask; t++ ) {
    int ini = t*st + ( t < mo ? t : mo );
    int size = st + ( t < mo ? 1 : 0 );
    int k = ini/(nx_fft*ny_fft) + nzlo_fft;
    int j = (ini/nx_fft)%ny_fft + nylo_fft;
    int i = ini%nx_fft + nxlo_fft;
    for (int n=ini; n< ini+size; n++) {
      double sqk = fkx[i]*fkx[i] + fky[j]*fky[j] + fkz[k]*fkz[k];
      if (sqk == 0.0) {
        vg[n][0] = 0.0;
        vg[n][1] = 0.0;
        vg[n][2] = 0.0;
        vg[n][3] = 0.0;
        vg[n][4] = 0.0;
        vg[n][5] = 0.0;
      } else {
        double vterm = -2.0 * (1.0/sqk + 0.25/(g_ewald*g_ewald));
        vg[n][0] = 1.0 + vterm*fkx[i]*fkx[i];
        vg[n][1] = 1.0 + vterm*fky[j]*fky[j];
        vg[n][2] = 1.0 + vterm*fkz[k]*fkz[k];
        vg[n][3] = vterm*fkx[i]*fky[j];
        vg[n][4] = vterm*fkx[i]*fkz[k];
        vg[n][5] = vterm*fky[j]*fkz[k];
      }
      if (++i>nxhi_fft) {
        i = nxlo_fft;
        if (++j>nyhi_fft) {
          j = nylo_fft;
          k++;
        }
      }
    }
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif

  if (differentiation_flag == 1) compute_gf_ad();
  else compute_gf_ik();

  // allocate extra memory for density_brick
  if (!ompss->shared_mem && nworkers>1) {
    th_density = new FFT_SCALAR[(nworkers-1)*ngrid ];
    ThrOMPSS::reset_arr_thr( nworkers-1, th_density, (nworkers-1)*ngrid, 0 );
  }

  nx_size  = (nxhi_out-nxlo_out+1);
  nz_plane = nx_size * (nyhi_out-nylo_out+1);
}

/* ----------------------------------------------------------------------
   allocate memory that depends on # of K-vectors and order
------------------------------------------------------------------------- */

void PPPMOMPSS::allocate()
{
  PPPM::allocate();
  nworkers = comm->nworkers;
  th_rho1d = new FFT_SCALAR**[nworkers];
  for (int i=0; i< nworkers; i++)
    memory->create2d_offset(th_rho1d[i],3,-order/2,order/2,"pppmompss::th_rho1d");
}

/* ----------------------------------------------------------------------
   deallocate memory that depends on # of K-vectors and order
------------------------------------------------------------------------- */

void PPPMOMPSS::deallocate()
{
  PPPM::deallocate();
  if (th_rho1d) {
    for (int i=0; i< nworkers; i++)
      memory->destroy2d_offset(th_rho1d[i],-order_allocated/2);
    delete [] th_rho1d;
  }
  if (th_density) delete [] th_density;
}

void PPPMOMPSS::compute( int eflag, int vflag )
{
  PPPM::compute( eflag, vflag );
  reduce_arrays( this );
}

/* ----------------------------------------------------------------------
   pre-compute modified (Hockney-Eastwood) Coulomb Green's function
------------------------------------------------------------------------- */
void PPPMOMPSS::compute_gf_ik()
{
  const double * const prd = domain->prd;

  double xprd = prd[0];
  double yprd = prd[1];
  double zprd = prd[2];
  double zprd_slab = zprd*slab_volfactor;
  double unitkx = (MY_2PI/xprd);
  double unitky = (MY_2PI/yprd);
  double unitkz = (MY_2PI/zprd_slab);


  int nbx = static_cast<int> ((g_ewald*xprd/(MY_PI*nx_pppm)) *
                                    pow(-log(EPS_HOC),0.25));
  int nby = static_cast<int> ((g_ewald*yprd/(MY_PI*ny_pppm)) *
                                    pow(-log(EPS_HOC),0.25));
  int nbz = static_cast<int> ((g_ewald*zprd_slab/(MY_PI*nz_pppm)) *
                                    pow(-log(EPS_HOC),0.25));
  int twoorder = 2*order;

  int nz = nzhi_fft - nzlo_fft + 1;
  int ny = nyhi_fft - nylo_fft + 1;
  int nx = nxhi_fft - nxlo_fft + 1;
  int niters = nx*ny*nz;
  int ntask = comm->nworkers;
  int st = niters/ntask;
  int mo = niters%ntask;
# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) \
  shared(st,mo,ntask,nx,ny,nz,unitkx, \
  unitky,unitkz,xprd,yprd,zprd_slab,nbx,nby,nbz,twoorder)
# elif _OMPSS == 2
# pragma oss task for default(none) \
  shared(st,mo,ntask,nx,ny,nz,unitkx, \
  unitky,unitkz,xprd,yprd,zprd_slab,nbx,nby,nbz,twoorder)
# endif
  for (int t=0; t< ntask; t++ ) {
    int ini = t*st + ( t < mo ? t : mo );
    int size = st + ( t < mo ? 1 : 0 );
    int m = ini/(nx*ny) + nzlo_fft;
    int l = (ini/nx)%ny + nylo_fft;
    int k = ini%nx + nxlo_fft;

    int mper = m - nz_pppm*(2*m/nz_pppm);
    double snz = square(sin(0.5*unitkz*mper*zprd_slab/nz_pppm));
    int lper = l - ny_pppm*(2*l/ny_pppm);
    double sny = square(sin(0.5*unitky*lper*yprd/ny_pppm));
    int kper = k - nx_pppm*(2*k/nx_pppm);
    double snx = square(sin(0.5*unitkx*kper*xprd/nx_pppm));

    for (int n=ini; n< ini+size; n++) {
      double sqk = square(unitkx*kper) + square(unitky*lper) + square(unitkz*mper);

      if (sqk != 0.0) {
        double numerator = 12.5663706/sqk;
        double denominator = gf_denom(snx,sny,snz);
        double sum1 = 0.0;

        for (int nx = -nbx; nx <= nbx; nx++) {
          double qx = unitkx*(kper+nx_pppm*nx);
          double sx = exp(-0.25*square(qx/g_ewald));
          double argx = 0.5*qx*xprd/nx_pppm;
          double wx = powsinxx(argx,twoorder);

          for (int ny = -nby; ny <= nby; ny++) {
            double qy = unitky*(lper+ny_pppm*ny);
            double sy = exp(-0.25*square(qy/g_ewald));
            double argy = 0.5*qy*yprd/ny_pppm;
            double wy = powsinxx(argy,twoorder);

            for (int nz = -nbz; nz <= nbz; nz++) {
              double qz = unitkz*(mper+nz_pppm*nz);
              double sz = exp(-0.25*square(qz/g_ewald));
              double argz = 0.5*qz*zprd_slab/nz_pppm;
              double wz = powsinxx(argz,twoorder);

              double dot1 = unitkx*kper*qx + unitky*lper*qy + unitkz*mper*qz;
              double dot2 = qx*qx+qy*qy+qz*qz;
              sum1 += (dot1/dot2) * sx*sy*sz * wx*wy*wz;
            }
          }
        }
        greensfn[n] = numerator*sum1/denominator;
      } else greensfn[n] = 0.0;
      // Update counters
      if (++k>nxhi_fft) {
        k = nxlo_fft;
        if (++l>nyhi_fft) {
          l = nylo_fft;
          m++;
          mper = m - nz_pppm*(2*m/nz_pppm);
          snz = square(sin(0.5*unitkz*mper*zprd_slab/nz_pppm));
        }
        lper = l - ny_pppm*(2*l/ny_pppm);
        sny = square(sin(0.5*unitky*lper*yprd/ny_pppm));
      }
      kper = k - nx_pppm*(2*k/nx_pppm);
      snx = square(sin(0.5*unitkx*kper*xprd/nx_pppm));
    }
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif
}

/* ----------------------------------------------------------------------
   find center grid pt for each of my particles
   check that full stencil for the particle will fit in my 3d brick
   store central grid pt indices in part2grid array
------------------------------------------------------------------------- */

void PPPMOMPSS::particle_map()
{
  double **x = atom->x;
  int nlocal = atom->nlocal;

  int flag = 0;

  if (!ISFINITE(boxlo[0]) || !ISFINITE(boxlo[1]) || !ISFINITE(boxlo[2]))
    error->one(FLERR,"Non-numeric box dimensions - simulation unstable");

# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) reduction(+:flag) \
  shared(nlocal,x)
# elif _OMPSS == 2
# pragma oss task for default(none) reduction(+:flag) \
  shared(nlocal,x)
#	endif
  for (int i = 0; i < nlocal; i++) {

    // (nx,ny,nz) = global coords of grid pt to "lower left" of charge
    // current particle coord can be outside global and local box
    // add/subtract OFFSET to avoid int(-0.75) = 0 when want it to be -1

    int nx = static_cast<int> ((x[i][0]-boxlo[0])*delxinv+shift) - OFFSET;
    int ny = static_cast<int> ((x[i][1]-boxlo[1])*delyinv+shift) - OFFSET;
    int nz = static_cast<int> ((x[i][2]-boxlo[2])*delzinv+shift) - OFFSET;

    part2grid[i][0] = nx;
    part2grid[i][1] = ny;
    part2grid[i][2] = nz;

    // check that entire stencil around nx,ny,nz will fit in my 3d brick

    if (nx+nlower < nxlo_out || nx+nupper > nxhi_out ||
        ny+nlower < nylo_out || ny+nupper > nyhi_out ||
        nz+nlower < nzlo_out || nz+nupper > nzhi_out)
      flag++;
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif

  if (flag) error->one(FLERR,"Out of range atoms - cannot compute PPPM");
}

/* ----------------------------------------------------------------------
   create discretized "density" on section of global grid due to my particles
   density(x,y,z) = charge "density" at grid points of my 3d brick
   (nxlo:nxhi,nylo:nyhi,nzlo:nzhi) is extent of my brick (including ghosts)
   in global grid
------------------------------------------------------------------------- */

void PPPMOMPSS::make_rho()
{
  FFT_SCALAR *arr = &(density_brick[nzlo_out][nylo_out][nxlo_out]);
  int nt = comm->nworkers;
# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) shared(nt,arr)
# elif _OMPSS == 2
# pragma oss task for default(none) shared(nt,arr)
# endif
  for (int t=0; t< nt; t++ ) {
    int st = ngrid/nt;
    int mo = ngrid%nt;
    int ini  = t*st + ( t<mo ? t : mo );
    int size = st + ( t < mo ? 1 : 0 );
    memset( arr+ini, 0, size*sizeof(FFT_SCALAR) );
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif

  int *centinel = ompss->centinel;
# if defined(_OPENMP) && !defined(_OMPSS)
# pragma omp parallel for default(none) shared(arr)
# endif
  for (int tt=0; tt< ompss->GetNTask( ); tt++ ) {
    int        t = ompss->GetTask( tt );
    int  nneight = ompss->nneight[t];
    int *pneight = ompss->pneight[t];
#   if _OMPSS == 1
#   pragma omp task default(none) firstprivate(t) shared(arr) \
    commutative( { centinel[pneight[ind]], ind=0;nneight } )
#   elif _OMPSS == 2
#   pragma oss task default(none) firstprivate(t) shared(arr) \
    commutative( { centinel[pneight[ind]], ind=0;nneight } )
#   endif
    {
#     if _OMPSS == 1 || defined(_OPENMP)
      int id = omp_get_thread_num();
#     elif _OMPSS == 2
      int id = nanos6_get_current_virtual_cpu( );
#     else
      int id = 0;
#     endif
      FFT_SCALAR *density = ompss->shared_mem || t==0 ? arr : th_density + (t-1)*ngrid ;
      FFT_SCALAR **loc_rho1d = th_rho1d[id];
      int *index = ompss->t_list[t];
      int natoms = ompss->t_natoms[t];
      double *q = atom->q;
      double **x = atom->x;
      for (int ii = 0; ii < natoms; ii++) {
        int i = index[ii];
        int nx = part2grid[i][0];
        int ny = part2grid[i][1];
        int nz = part2grid[i][2];
        double dx = nx+shiftone - (x[i][0]-boxlo[0])*delxinv;
        double dy = ny+shiftone - (x[i][1]-boxlo[1])*delyinv;
        double dz = nz+shiftone - (x[i][2]-boxlo[2])*delzinv;

        compute_rho1d_thr(dx,dy,dz,loc_rho1d);

        double z0 = delvolinv * q[i];
        int mz = (nz+nlower-nzlo_out)*nz_plane;
        for (int n = nlower; n <= nupper; n++, mz+=nz_plane) {
          int my = (ny+nlower-nylo_out)*nx_size + mz;
          double y0 = z0*loc_rho1d[2][n];
          for (int m = nlower; m <= nupper; m++, my+=nx_size) {
            int id = my + nx + nlower- nxlo_out;
            double x0 = y0*loc_rho1d[1][m];
            for (int l = nlower; l <= nupper; l++, id++) {
              density[id] += x0*loc_rho1d[0][l];
            }
          }
        }
      }
    }
  }
# if   _OMPSS == 1
# pragma omp taskwait
# elif _OMPSS == 2
# pragma oss taskwait
# endif
  if (!ompss->shared_mem) reduce_arr_thr( comm->nworkers, ngrid, arr, th_density  );
}

/* ----------------------------------------------------------------------
   remap density from 3d brick decomposition to FFT decomposition
------------------------------------------------------------------------- */

void PPPMOMPSS::brick2fft()
{
  // copy grabs inner portion of density from 3d brick
  // remap could be done as pre-stage of FFT,
  //   but this works optimally on only double values, not complex values
  int nz = nzhi_in - nzlo_in + 1;
  int ny = nyhi_in - nylo_in + 1;
  int nx = nxhi_in - nxlo_in + 1;

  int nmax = nx*ny*nz;
  int ntask = comm->nworkers;
  int st = nmax/ntask;
  int mo = nmax%ntask;
# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) \
  shared(st,mo,ntask,nx,ny,nz)
# elif _OMPSS == 2
# pragma oss task for default(none) \
  shared(st,mo,ntask,nx,ny,nz)
# endif
  for (int t=0; t< ntask; t++ ) {
    int ini = t*st + ( t < mo ? t : mo );
    int size = st + ( t < mo ? 1 : 0 );
    int ix = nxlo_in + ini%nx;
    int iy = nylo_in + (ini/nx)%ny;
    int iz = nzlo_in + (ini/(nx*ny));
    for (int i=ini; i< ini+size; i++ ) {
      density_fft[i] = density_brick[iz][iy][ix++];
      if (ix>nxhi_in) {
        ix = nxlo_in;
        iy++;
        if (iy>nyhi_in) {
          iy = nylo_in;
          iz++;
        }
      }
    }
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif
  remap->perform(density_fft,density_fft,work1);
}


/* ----------------------------------------------------------------------
   FFT-based Poisson solver for ik
------------------------------------------------------------------------- */
void PPPMOMPSS::poisson_ik()
{
  // transform charge density (r -> k)
# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none)
# elif _OMPSS == 2
# pragma oss task for default(none)
# endif
  for (int i = 0; i < nfft; i++) {
    int n = i << 1;
    work1[n] = density_fft[i];
    work1[n+1] = ZEROF;
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif

  fft1->compute(work1,work1,1);

  // global energy and virial contribution

  double scaleinv = 1.0/(nx_pppm*ny_pppm*nz_pppm);
  double s2 = scaleinv*scaleinv;

  if (eflag_global || vflag_global) {
    if (vflag_global) {
      double loc_virial[6] = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
      double loc_energy = 0.0;
#     if _OMPSS == 1 || defined(_OPENMP)
#     pragma omp parallel for default(none) shared(s2) \
      reduction(+:loc_virial) reduction(+:loc_energy)
# 	  elif _OMPSS == 2
#     pragma oss task for default(none) shared(s2) \
      reduction(+:loc_virial) reduction(+:loc_energy)
#     endif
      for (int i = 0; i < nfft; i++) {
        int n = i << 1;
        double eng = s2 * greensfn[i] * (work1[n]*work1[n] + work1[n+1]*work1[n+1]);
        for (int j = 0; j < 6; j++) loc_virial[j] += eng*vg[i][j];
        if (eflag_global) loc_energy += eng;
      }
# 	  if _OMPSS == 2
#     pragma oss taskwait
#     endif

      for (int j = 0; j < 6; j++) virial[j] += loc_virial[j];
      energy += loc_energy;
    } else {
      double loc_energy = 0.0;
#     if _OMPSS == 1 || defined(_OPENMP)
#     pragma omp parallel for default(none) shared(s2) reduction(+:loc_energy)
#     elif _OMPSS == 2
#     pragma oss task for default(none) shared(s2) reduction(+:loc_energy)
#     endif
      for (int i = 0; i < nfft; i++) {
        int n = i << 1;
        loc_energy += s2 * greensfn[i] * (work1[n]*work1[n] + work1[n+1]*work1[n+1]);
      }
# 	  if _OMPSS == 2
#     pragma oss taskwait
#     endif

      energy += loc_energy;
    }
  }

  // scale by 1/total-grid-pts to get rho(k)
  // multiply by Green's function to get V(k)
# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) shared(scaleinv)
# elif _OMPSS == 2
# pragma oss task for default(none) shared(scaleinv)
# endif
  for (int i = 0; i < nfft; i++) {
    int n = i << 1;
    double factor = scaleinv * greensfn[i];
    work1[n]   *= factor;
    work1[n+1] *= factor;
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif

  // extra FFTs for per-atom energy/virial
  if (evflag_atom) poisson_peratom();

  // triclinic system

  if (triclinic) {
    poisson_ik_triclinic();
    return;
  }

  // compute gradients of V(r) in each of 3 dims by transformimg -ik*V(k)
  // FFT leaves data in 3d brick decomposition
  // copy it into inner portion of vdx,vdy,vdz arrays

  // x direction gradient

  int nz_fft = nzhi_fft - nzlo_fft + 1;
  int ny_fft = nyhi_fft - nylo_fft + 1;
  int nx_fft = nxhi_fft - nxlo_fft + 1;
  int niters_fft = nx_fft*ny_fft*nz_fft;
  int ntask = comm->nworkers;
  int st = niters_fft/ntask;
  int mo = niters_fft%ntask;
# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) \
  shared(st,mo,ntask,nx_fft,ny_fft)
# elif _OMPSS == 2
# pragma oss task for default(none) \
  shared(st,mo,ntask,nx_fft,ny_fft)
# endif
  for (int t=0; t< ntask; t++ ) {
    int ini = t*st + ( t < mo ? t : mo );
    int size = st + ( t < mo ? 1 : 0 );
    int k = ini/(nx_fft*ny_fft) + nzlo_fft;
    int j = (ini/nx_fft)%ny_fft + nylo_fft;
    int i = ini%nx_fft + nxlo_fft;
    int n = ini << 1;
    for (int nn=ini; nn< ini+size; nn++) {
      work2[n] = fkx[i]*work1[n+1];
      work2[n+1] = -fkx[i]*work1[n];
      n += 2;
      if (++i>nxhi_fft) {
        i = nxlo_fft;
        if (++j>nyhi_fft) {
          j = nylo_fft;
          k++;
        }
      }
    }
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif

  fft2->compute(work2,work2,-1);

  int nz_in = nzhi_in - nzlo_in + 1;
  int ny_in = nyhi_in - nylo_in + 1;
  int nx_in = nxhi_in - nxlo_in + 1;
  int niters_in = nx_in*ny_in*nz_in;
  st = niters_in/ntask;
  mo = niters_in%ntask;
# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) \
  shared(st,mo,ntask,nx_in,ny_in)
# elif _OMPSS == 2
# pragma oss task for default(none) \
  shared(st,mo,ntask,nx_in,ny_in)
# endif
  for (int t=0; t< ntask; t++ ) {
    int ini = t*st + ( t < mo ? t : mo );
    int size = st + ( t < mo ? 1 : 0 );
    int k = ini/(nx_in*ny_in) + nzlo_in;
    int j = (ini/nx_in)%ny_in + nylo_in;
    int i = ini%nx_in + nxlo_in;
    int n = ini << 1;
    for (int nn=ini; nn< ini+size; nn++) {
      vdx_brick[k][j][i] = work2[n];
      n += 2;
      if (++i>nxhi_in) {
        i = nxlo_in;
        if (++j>nyhi_in) {
          j = nylo_in;
          k++;
        }
      }
    }
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif

  // y direction gradient
  st = niters_fft/ntask;
  mo = niters_fft%ntask;
# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) \
  shared(st,mo,ntask,nx_fft,ny_fft)
# elif _OMPSS == 2
# pragma oss task for default(none) \
  shared(st,mo,ntask,nx_fft,ny_fft)
# endif
  for (int t=0; t< ntask; t++ ) {
    int ini = t*st + ( t < mo ? t : mo );
    int size = st + ( t < mo ? 1 : 0 );
    int k = ini/(nx_fft*ny_fft) + nzlo_fft;
    int j = (ini/nx_fft)%ny_fft + nylo_fft;
    int i = ini%nx_fft + nxlo_fft;
    int n = ini << 1;
    for (int nn=ini; nn< ini+size; nn++) {
      work2[n] = fky[j]*work1[n+1];
      work2[n+1] = -fky[j]*work1[n];
      n += 2;
      if (++i>nxhi_fft) {
        i = nxlo_fft;
        if (++j>nyhi_fft) {
          j = nylo_fft;
          k++;
        }
      }
    }
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif

  fft2->compute(work2,work2,-1);

  st = niters_in/ntask;
  mo = niters_in%ntask;
# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) \
  shared(st,mo,ntask,nx_in,ny_in)
# elif _OMPSS == 2
# pragma oss task for default(none) \
  shared(st,mo,ntask,nx_in,ny_in)
# endif
  for (int t=0; t< ntask; t++ ) {
    int ini = t*st + ( t < mo ? t : mo );
    int size = st + ( t < mo ? 1 : 0 );
    int k = ini/(nx_in*ny_in) + nzlo_in;
    int j = (ini/nx_in)%ny_in + nylo_in;
    int i = ini%nx_in + nxlo_in;
    int n = ini << 1;
    for (int nn=ini; nn< ini+size; nn++) {
      vdy_brick[k][j][i] = work2[n];
      n += 2;
      if (++i>nxhi_in) {
        i = nxlo_in;
        if (++j>nyhi_in) {
          j = nylo_in;
          k++;
        }
      }
    }
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif

  // z direction gradient
  st = niters_fft/ntask;
  mo = niters_fft%ntask;
# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) \
  shared(st,mo,ntask,nx_fft,ny_fft)
# elif _OMPSS == 2
# pragma oss task for default(none) \
  shared(st,mo,ntask,nx_fft,ny_fft)
# endif
  for (int t=0; t< ntask; t++ ) {
    int ini = t*st + ( t < mo ? t : mo );
    int size = st + ( t < mo ? 1 : 0 );
    
    int k = ini/(nx_fft*ny_fft) + nzlo_fft;
    int j = (ini/nx_fft)%ny_fft + nylo_fft;
    int i = ini%nx_fft + nxlo_fft;
    int n = ini << 1;
    for (int nn=ini; nn< ini+size; nn++) {
      work2[n] = fkz[k]*work1[n+1];
      work2[n+1] = -fkz[k]*work1[n];
      n += 2;
      if (++i>nxhi_fft) {
        i = nxlo_fft;
        if (++j>nyhi_fft) {
          j = nylo_fft;
          k++;
        }
      }
    }
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif

  fft2->compute(work2,work2,-1);

  st = niters_in/ntask;
  mo = niters_in%ntask;
# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) \
  shared(st,mo,ntask,nx_in,ny_in)
# elif _OMPSS == 2
# pragma oss task for default(none) \
  shared(st,mo,ntask,nx_in,ny_in)
# endif
  for (int t=0; t< ntask; t++ ) {
    int ini = t*st + ( t < mo ? t : mo );
    int size = st + ( t < mo ? 1 : 0 );
    
    int k = ini/(nx_in*ny_in) + nzlo_in;
    int j = (ini/nx_in)%ny_in + nylo_in;
    int i = ini%nx_in + nxlo_in;
    int n = ini << 1;
    for (int nn=ini; nn< ini+size; nn++) {
      vdz_brick[k][j][i] = work2[n];
      n += 2;
      if (++i>nxhi_in) {
        i = nxlo_in;
        if (++j>nyhi_in) {
          j = nylo_in;
          k++;
        }
      }
    }
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif
}


/* ----------------------------------------------------------------------
   interpolate from grid to get electric field & force on my particles for ik
------------------------------------------------------------------------- */

void PPPMOMPSS::fieldforce_ik()
{
  // loop over my charges, interpolate electric field from nearby grid points
  // (nx,ny,nz) = global coords of grid pt to "lower left" of charge
  // (dx,dy,dz) = distance to "lower left" grid pt
  // (mx,my,mz) = global coords of moving stencil pt
  // ek = 3 components of E-field on particle

  int nlocal = atom->nlocal;
  int ntask = comm->nworkers;
  int st = nlocal/ntask;
  int mo = nlocal%ntask;
# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) \
  shared(st,mo,ntask)
# elif _OMPSS == 2
# pragma oss task for default(none) \
  shared(st,mo,ntask)
# endif
  for (int t=0; t< ntask; t++ ) {
    int ini = t*st + ( t < mo ? t : mo );
    int size = st + ( t < mo ? 1 : 0 );
#   if _OMPSS == 1 || defined(_OPENMP)
    int id = omp_get_thread_num();
#   elif _OMPSS == 2
    int id = nanos6_get_current_virtual_cpu( );
#   else
    int id = 0;
#   endif

    FFT_SCALAR **loc_rho1d = th_rho1d[id];
    double *q = atom->q;
    double **x = atom->x;
    double **f = atom->f;

    for (int i = ini; i < ini+size; i++) {
      int nx = part2grid[i][0];
      int ny = part2grid[i][1];
      int nz = part2grid[i][2];
      FFT_SCALAR dx = nx+shiftone - (x[i][0]-boxlo[0])*delxinv;
      FFT_SCALAR dy = ny+shiftone - (x[i][1]-boxlo[1])*delyinv;
      FFT_SCALAR dz = nz+shiftone - (x[i][2]-boxlo[2])*delzinv;

      compute_rho1d_thr(dx,dy,dz,loc_rho1d);

      FFT_SCALAR ekx = ZEROF;
      FFT_SCALAR eky = ZEROF;
      FFT_SCALAR ekz = ZEROF;
      for (int n = nlower; n <= nupper; n++) {
        int mz = n+nz;
        FFT_SCALAR z0 = loc_rho1d[2][n];
        for (int m = nlower; m <= nupper; m++) {
          int my = m+ny;
          FFT_SCALAR y0 = z0*loc_rho1d[1][m];
          for (int l = nlower; l <= nupper; l++) {
            int mx = l+nx;
            FFT_SCALAR x0 = y0*loc_rho1d[0][l];
            ekx -= x0*vdx_brick[mz][my][mx];
            eky -= x0*vdy_brick[mz][my][mx];
            ekz -= x0*vdz_brick[mz][my][mx];
          }
        }
      }
      // convert E-field to force
      const double qfactor = qqrd2e * scale * q[i];
      f[i][0] += qfactor*ekx;
      f[i][1] += qfactor*eky;
      if (slabflag != 2) f[i][2] += qfactor*ekz;
    }
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif
}

void PPPMOMPSS::compute_rho1d_thr( const FFT_SCALAR &dx, const FFT_SCALAR &dy,
                                   const FFT_SCALAR &dz, FFT_SCALAR **loc_rho1d )
{
  int k,l;
  FFT_SCALAR r1,r2,r3;

  for (k = (1-order)/2; k <= order/2; k++) {
    r1 = r2 = r3 = ZEROF;

    for (l = order-1; l >= 0; l--) {
      r1 = rho_coeff[l][k] + r1*dx;
      r2 = rho_coeff[l][k] + r2*dy;
      r3 = rho_coeff[l][k] + r3*dz;
    }
    loc_rho1d[0][k] = r1;
    loc_rho1d[1][k] = r2;
    loc_rho1d[2][k] = r3;
  }
}
