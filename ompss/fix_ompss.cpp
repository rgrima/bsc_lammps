/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------
------------------------------------------------------------------------- */


#include "fix_ompss.h"

#include <string.h>
#include <stdlib.h>

#include "atom.h"
#include "comm.h"
#include "universe.h"
#include "update.h"
#include "force.h"
#include "neighbor.h"
#include "neigh_request.h"
#include "error.h"
#include "accelerator_ompss.h"
#include "thr_ompss.h"

#include "pair_hybrid.h"
#include "bond_hybrid.h"
#include "angle_hybrid.h"
#include "dihedral_hybrid.h"
#include "improper_hybrid.h"
#include "kspace.h"

#include "suffix.h"

using namespace LAMMPS_NS;
using namespace FixConst;

/* ---------------------------------------------------------------------- */

FixOMPSS::FixOMPSS(LAMMPS *lmp, int narg, char **arg)
  :  Fix(lmp, narg, arg),
     last_ompss_style(NULL), last_pair_hybrid(NULL),
     _reset(false)
{ }

/* ---------------------------------------------------------------------- */

FixOMPSS::~FixOMPSS()
{
}

/* ---------------------------------------------------------------------- */

int FixOMPSS::setmask()
{
  int mask = 0;
  mask |= PRE_FORCE;
  mask |= PRE_FORCE_RESPA;
  mask |= MIN_PRE_FORCE;
  return mask;
}

/* ---------------------------------------------------------------------- */

void FixOMPSS::init()
{
  int kspace_split;
  
  // support for verlet/split operation.
  // kspace_split == 0 : regular processing
  // kspace_split < 0  : master partition, does not do kspace
  // kspace_split > 0  : slave partition, only does kspace

  if (strstr(update->integrate_style,"verlet/split") != NULL) {
    if (universe->iworld == 0) kspace_split = -1;
    else kspace_split = 1;
  } else {
    kspace_split = 0;
  }

  int check_hybrid;
  last_pair_hybrid = NULL;
  last_ompss_style = NULL;
  const char *last_ompss_name = NULL;
  const char *last_hybrid_name = NULL;
  const char *last_force_name = NULL;

  if ((strstr(update->integrate_style,"respa") != NULL)
      && (strstr(update->integrate_style,"respa/ompss") == NULL))
    error->all(FLERR,"Need to use respa/ompss for r-RESPA with /ompss styles");

#define CheckStyleForOMPSS(name)                                        \
  check_hybrid = 0;                                                     \
  if (force->name) {                                                    \
    if ( (strcmp(force->name ## _style,"hybrid") == 0) ||               \
         (strcmp(force->name ## _style,"hybrid/overlay") == 0) )        \
      check_hybrid=1;                                                   \
    if (force->name->suffix_flag & Suffix::OMPSS) {                     \
      last_force_name = (const char *) #name;                           \
      last_ompss_name = force->name ## _style;                          \
      last_ompss_style = (void *) force->name;                          \
    }                                                                   \
  }

#define CheckHybridForOMPSS(name,Class)                       \
  if (check_hybrid) {                                         \
    Class ## Hybrid *style = (Class ## Hybrid *) force->name; \
    for (int i=0; i < style->nstyles; i++) {                  \
      if (style->styles[i]->suffix_flag & Suffix::OMPSS) {    \
        last_force_name = (const char *) #name;               \
        last_ompss_name = style->keywords[i];                   \
        last_ompss_style = style->styles[i];                    \
      }                                                       \
    }                                                         \
  }

  if (kspace_split <= 0) {
    CheckStyleForOMPSS(pair);
    CheckHybridForOMPSS(pair,Pair);
    if (check_hybrid) {
      last_pair_hybrid = last_ompss_style;
      last_hybrid_name = last_ompss_name;
    }

    CheckStyleForOMPSS(bond);
    CheckHybridForOMPSS(bond,Bond);

    CheckStyleForOMPSS(angle);
    CheckHybridForOMPSS(angle,Angle);

    CheckStyleForOMPSS(dihedral);
    CheckHybridForOMPSS(dihedral,Dihedral);

    CheckStyleForOMPSS(improper);
    CheckHybridForOMPSS(improper,Improper);
  }

  if (kspace_split >= 0) {
    CheckStyleForOMPSS(kspace);
  }

#undef CheckStyleForOMPSS
#undef CheckHybridForOMPSS
  if (last_ompss_name) set_neighbor_ompss();

  // diagnostic output
  if (comm->me == 0) {
    if (last_ompss_style) {
      if (last_pair_hybrid) {
        if (screen)
          fprintf(screen,"Hybrid pair style last /ompss style %s\n", last_hybrid_name);
        if (logfile)
          fprintf(logfile,"Hybrid pair style last /ompss style %s\n", last_hybrid_name);
      }
      if (screen)
        fprintf(screen,"Last active /ompss style is %s_style %s\n",
                last_force_name, last_ompss_name);
      if (logfile)
        fprintf(logfile,"Last active /ompss style is %s_style %s\n",
                last_force_name, last_ompss_name);
    } else {
      if (screen)
        fprintf(screen,"No /ompss style for force computation currently active\n");
      if (logfile)
        fprintf(logfile,"No /ompss style for force computation currently active\n");
    }
  }
}

/* ---------------------------------------------------------------------- */

void FixOMPSS::set_neighbor_ompss()
{
  // select or deselect multi-threaded neighbor
  // list build depending on setting in package ompss.
  // NOTE: since we are at the top of the list of
  // fixes, we cannot adjust neighbor lists from
  // other fixes. those have to be re-implemented
  // as /ompss fix styles. :-(

  const int nrequest = neighbor->nrequest;

  for (int i = 0; i < nrequest; ++i)
    neighbor->requests[i]->ompss = 1;
}

// adjust size and clear out per thread accumulator arrays
void FixOMPSS::pre_force(int)
{
  int        nthr = comm->nworkers;
  const int  nall = atom->nlocal + atom->nghost;
  const int  nmax = atom->nmax;
  double       **f = atom->f;
  double  **torque = atom->torque;

  if (ompss->shared_mem || _reset) {
    if (f)      ThrOMPSS::reset_arr_thr( nthr, f[0],      3*nall, 0 );
    if (torque) ThrOMPSS::reset_arr_thr( nthr, torque[0], 3*nall, 0 );
  } else {
    if (f)      ThrOMPSS::reset_arr_thr( nthr, f[0],      3*nall, 3*nmax );
    if (torque) ThrOMPSS::reset_arr_thr( nthr, torque[0], 3*nall, 3*nmax );
    _reset = true;
  }
}
