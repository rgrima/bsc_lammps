/* -*- c++ -*- ----------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#ifndef LMP_COMM_BRICK_OMPSS_H
#define LMP_COMM_BRICK_OMPSS_H

#include "comm_brick.h"

namespace LAMMPS_NS {

class CommBrickOMPSS : public CommBrick {
 public:
  CommBrickOMPSS(class LAMMPS *);
  CommBrickOMPSS(CommBrick *);
  ~CommBrickOMPSS();

  void borders();                      // setup list of atoms to comm
  void exchange();
  void reverse_comm();

 protected:

  int **pbuf_sr_list;
  int *pbuf_sr_list_max;
  int *pbuf_sr_list_size;

  MPI_Request *prequest;
  int pmax_send;
  int pmax_recv;

  void init_buffers();

  void grow_pbuf_sr_list(int , int );
};

}

#endif

/* ERROR/WARNING messages:

E: Cannot change to comm_style brick from tiled layout

Self-explanatory.

*/
