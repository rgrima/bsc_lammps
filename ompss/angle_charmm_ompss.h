/* -*- c++ -*- ----------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#ifdef ANGLE_CLASS

AngleStyle(charmm/ompss,AngleCharmmOMPSS)

#else

#ifndef LMP_ANGLE_CHARMM_OMPSS_H
#define LMP_ANGLE_CHARMM_OMPSS_H

#include "angle_charmm.h"
#include "thr_ompss.h"

namespace LAMMPS_NS {

class AngleCharmmOMPSS : public AngleCharmm, public ThrOMPSS {
 public:
  AngleCharmmOMPSS(class LAMMPS *);
  virtual void compute(int, int);

 private:
  template <int EVFLAG, int EFLAG, int NEWTON_BOND>
  void eval( int t, ThrOMPSS * const thr);
};

}

#endif
#endif

/* ERROR/WARNING messages:

E: Incorrect args for angle coefficients

Self-explanatory.  Check the input script or data file.

*/
