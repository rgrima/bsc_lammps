/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#include <mpi.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "fix_shake_ompss.h"
#include "atom.h"
#include "atom_vec.h"
#include "molecule.h"
#include "update.h"
#include "respa.h"
#include "comm.h"
#include "memory.h"
#include "math_const.h"
#include "accelerator_ompss.h"
#include "error.h"
#include "domain.h"
#include "force.h"

using namespace LAMMPS_NS;
using namespace FixConst;
using namespace MathConst;

#define BIG 1.0e20
#define MASSDELTA 0.1

/* ---------------------------------------------------------------------- */

FixShakeOMPSS::FixShakeOMPSS(LAMMPS *lmp, int narg, char **arg) :
  FixShake(lmp, narg, arg),
  ThrOMPSS(lmp)
  { }

void FixShakeOMPSS::setup( int vflag )
{
  pre_neighbor();

  if (output_every) stats();

  // setup SHAKE output

  bigint ntimestep = update->ntimestep;
  if (output_every) {
    next_output = ntimestep + output_every;
    if (ntimestep % output_every != 0)
      next_output = (ntimestep/output_every)*output_every + output_every;
  } else next_output = -1;

  // set respa to 0 if verlet is used and to 1 otherwise

  if (strstr(update->integrate_style,"verlet"))
    respa = 0;
  else
    respa = 1;

  if (!respa) {
    dtv     = update->dt;
    dtfsq   = 0.5 * update->dt * update->dt * force->ftm2v;
    if (!rattle) dtfsq = update->dt * update->dt * force->ftm2v;
  } else {
    dtv = step_respa[0];
    dtf_innerhalf = 0.5 * step_respa[0] * force->ftm2v;
    dtf_inner = dtf_innerhalf;
  }

  // correct geometry of cluster if necessary
  correct_coordinates(vflag);

  // remove velocities along any bonds

  correct_velocities();

  // precalculate constraining forces for first integration step

  shake_end_of_step(vflag);
}

void FixShakeOMPSS::pre_neighbor()
{
  FixShake::pre_neighbor( );
  ompss->buildShakeList( nlist, list, shake_atom );
}

/* ----------------------------------------------------------------------
   compute the force adjustment for SHAKE constraint
------------------------------------------------------------------------- */

void FixShakeOMPSS::post_force(int vflag)
{
  if (update->ntimestep == next_output) stats();

  // xshake = unconstrained move with current v,f
  // communicate results if necessary

  unconstrained_update();
  if (nprocs > 1) comm->forward_comm_fix(this);

  // virial setup

  if (vflag) v_setup(vflag);
  else evflag = 0;

  // loop over clusters to add constraint forces

  int *centinel = ompss->centinel;
# if defined(_OPENMP) && !defined(_OMPSS)
# pragma omp parallel for default(none)
# endif
  for (int tt=0; tt< ompss->GetNTask( ); tt++ ) {
    int        t = ompss->GetTask( tt );
    int  nneight = ompss->nneight[t];
    int *pneight = ompss->pneight[t];
#   if _OMPSS == 1
#   pragma omp task default(none) firstprivate(t) \
    commutative( { centinel[pneight[ind]], ind=0;nneight } )
#   elif _OMPSS == 2
#   pragma oss task default(none) firstprivate(t) \
    commutative( { centinel[pneight[ind]], ind=0;nneight } )
#   endif
    {
      int *l_list = ompss->t_shakelist[t];
      int l_nlist = ompss->t_nshakelist[t];
      dbl3_t * _noalias const frc = (dbl3_t *) ompss->f[t][0];

      ThrOMPSS thr;
      thr.init( (Fix *)this, ompss->shared_mem ? 0 : maxvatom  );
      for (int i = 0; i < l_nlist; i++) {
        int m = l_list[i];
        if (shake_flag[m] == 2) shake( m, frc, &thr );
        else if (shake_flag[m] == 3) shake3( m, frc, &thr );
        else if (shake_flag[m] == 4) shake4( m, frc, &thr );
        else shake3angle( m, frc, &thr );
      }
      thr.reduce_thr( this, vflag_global );
    } // Task End
  } // Task loop
  reduce_arrays( this );

  // store vflag for coordinate_constraints_end_of_step()
  vflag_post_force = vflag;
}

/* ----------------------------------------------------------------------
   enforce SHAKE constraints from rRESPA
   xshake prediction portion is different than Verlet
------------------------------------------------------------------------- */

void FixShakeOMPSS::post_force_respa(int vflag, int ilevel, int iloop)
{
  // call stats only on outermost level

  if (ilevel == nlevels_respa-1 && update->ntimestep == next_output) stats();

  // might be OK to skip enforcing SHAKE constraings
  // on last iteration of inner levels if pressure not requested
  // however, leads to slightly different trajectories

  //if (ilevel < nlevels_respa-1 && iloop == loop_respa[ilevel]-1 && !vflag)
  //  return;

  // xshake = unconstrained move with current v,f as function of level
  // communicate results if necessary

  unconstrained_update_respa(ilevel);
  if (nprocs > 1) comm->forward_comm_fix(this);

  // virial setup only needed on last iteration of innermost level
  //   and if pressure is requested
  // virial accumulation happens via evflag at last iteration of each level

  if (ilevel == 0 && iloop == loop_respa[ilevel]-1 && vflag) v_setup(vflag);
  if (iloop == loop_respa[ilevel]-1) evflag = 1;
  else evflag = 0;

  // loop over clusters to add constraint forces
  int *centinel = ompss->centinel;
# if defined(_OPENMP) && !defined(_OMPSS)
# pragma omp parallel for default(none)
# endif
  for (int t=0; t < ompss->GetNTask( ); t++ ) {
    int  nneight = ompss->nneight[t];
    int *pneight = ompss->pneight[t];
#   if _OMPSS == 1
#   pragma omp task default(none) firstprivate(t) \
    commutative( { centinel[pneight[ind]], ind=0;nneight } )
#   elif _OMPSS == 2
#   pragma oss task default(none) firstprivate(t) \
    commutative( { centinel[pneight[ind]], ind=0;nneight } )
#   endif
    {
      int *l_list = ompss->t_shakelist[t];
      int l_nlist = ompss->t_nshakelist[t];
      ThrOMPSS thr;
      thr.init( (Fix *)this, ompss->shared_mem ? 0 : maxvatom );
      dbl3_t * _noalias const frc = (dbl3_t *) ompss->f[t][0];
      for (int i = 0; i < l_nlist; i++) {
        int m = l_list[i];
        if (shake_flag[m] == 2) shake( m, frc, &thr );
        else if (shake_flag[m] == 3) shake3( m, frc, &thr );
        else if (shake_flag[m] == 4) shake4( m, frc, &thr);
        else shake3angle( m, frc, &thr );
      }
      thr.reduce_thr( this, vflag_global );
    } // Task End
  } // Task loop
  reduce_arrays( this );

  // store vflag for coordinate_constraints_end_of_step()
  vflag_post_force = vflag;
}

/* ----------------------------------------------------------------------
   identify whether each atom is in a SHAKE cluster
   only include atoms in fix group and those bonds/angles specified in input
   test whether all clusters are valid
   set shake_flag, shake_atom, shake_type values
   set bond,angle types negative so will be ignored in neighbor lists
------------------------------------------------------------------------- */

void FixShakeOMPSS::find_clusters()
{
  tagint *buf;

  if (me == 0 && screen) {
    if (!rattle) fprintf(screen,"Finding SHAKE clusters ...\n");
    else fprintf(screen,"Finding RATTLE clusters ...\n");
  }

  atommols = atom->avec->onemols;

  tagint *tag = atom->tag;
  int *type = atom->type;
  int *mask = atom->mask;
  double *mass = atom->mass;
  double *rmass = atom->rmass;
  int **nspecial = atom->nspecial;
  tagint **special = atom->special;

  int *molindex = atom->molindex;
  int *molatom = atom->molatom;

  int nlocal = atom->nlocal;
  int angles_allow = atom->avec->angles_allow;

  // setup ring of procs

  int next = me + 1;
  int prev = me -1;
  if (next == nprocs) next = 0;
  if (prev < 0) prev = nprocs - 1;

  // -----------------------------------------------------
  // allocate arrays for self (1d) and bond partners (2d)
  // max = max # of bond partners for owned atoms = 2nd dim of partner arrays
  // npartner[i] = # of bonds attached to atom i
  // nshake[i] = # of SHAKE bonds attached to atom i
  // partner_tag[i][] = global IDs of each partner
  // partner_mask[i][] = mask of each partner
  // partner_type[i][] = type of each partner
  // partner_massflag[i][] = 1 if partner meets mass criterion, 0 if not
  // partner_bondtype[i][] = type of bond attached to each partner
  // partner_shake[i][] = 1 if SHAKE bonded to partner, 0 if not
  // partner_nshake[i][] = nshake value for each partner
  // -----------------------------------------------------

  int max = 0;
  if (molecular == 1) {
#   if _OMPSS == 1 || defined(_OPENMP)
#   pragma omp parallel for reduction(max:max) shared(nlocal,nspecial)
#   elif _OMPSS == 2
#   pragma oss task for reduction(max:max) shared(nlocal,nspecial)
#   endif
    for (int i = 0; i < nlocal; i++) max = MAX(max,nspecial[i][0]);
  } else {
#   if _OMPSS == 1 || defined(_OPENMP)
#   pragma omp parallel for reduction(max:max) shared(nlocal,molindex,molatom)
#   elif _OMPSS == 2
#   pragma oss task for reduction(max:max) shared(nlocal,molindex,molatom)
#   endif
    for (int i = 0; i < nlocal; i++) {
      int imol = molindex[i];
      if (imol < 0) continue;
      int iatom = molatom[i];
      max = MAX(max,atommols[imol]->nspecial[iatom][0]);
    }
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif

  int *npartner;
  tagint **partner_tag;
  int **partner_mask,**partner_type,**partner_massflag;
  int **partner_bondtype,**partner_shake,**partner_nshake;

  memory->create(npartner,nlocal,"shake:npartner");
  memory->create(nshake,nlocal,"shake:nshake");
  memory->create(partner_tag,nlocal,max,"shake:partner_tag");
  memory->create(partner_mask,nlocal,max,"shake:partner_mask");
  memory->create(partner_type,nlocal,max,"shake:partner_type");
  memory->create(partner_massflag,nlocal,max,"shake:partner_massflag");
  memory->create(partner_bondtype,nlocal,max,"shake:partner_bondtype");
  memory->create(partner_shake,nlocal,max,"shake:partner_shake");
  memory->create(partner_nshake,nlocal,max,"shake:partner_nshake");

  // -----------------------------------------------------
  // set npartner and partner_tag from special arrays
  // -----------------------------------------------------

  if (molecular == 1) {
#   if _OMPSS == 1 || defined(_OPENMP)
#   pragma omp parallel for default(none) \
    shared(nlocal,npartner,nspecial,special,partner_tag)
#   elif _OMPSS == 2
#   pragma oss task for default(none) \
    shared(nlocal,npartner,nspecial,special,partner_tag)
#   endif
    for (int i = 0; i < nlocal; i++) {
      npartner[i] = nspecial[i][0];
      for (int j = 0; j < npartner[i]; j++)
        partner_tag[i][j] = special[i][j];
    }
  } else {
#   if _OMPSS == 1 || defined(_OPENMP)
#   pragma omp parallel for default(none) \
    shared(nlocal,npartner,nspecial,partner_tag,molindex,molatom,tag)
#   elif _OMPSS == 2
#   pragma oss task for default(none) \
    shared(nlocal,npartner,nspecial,partner_tag,molindex,molatom,tag)
#   endif
    for (int i = 0; i < nlocal; i++) {
      int imol = molindex[i];
      if (imol < 0) continue;
      int iatom = molatom[i];
      tagint tagprev = tag[i] - iatom - 1;
      npartner[i] = atommols[imol]->nspecial[iatom][0];
      for (int j = 0; j < npartner[i]; j++)
        partner_tag[i][j] = atommols[imol]->special[iatom][j] + tagprev;;
    }
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif
  // -----------------------------------------------------
  // set partner_mask, partner_type, partner_massflag, partner_bondtype
  //   for bonded partners
  // requires communication for off-proc partners
  // -----------------------------------------------------

  // fill in mask, type, massflag, bondtype if own bond partner
  // info to store in buf for each off-proc bond = nper = 6
  //   2 atoms IDs in bond, space for mask, type, massflag, bondtype
  // nbufmax = largest buffer needed to hold info from any proc

  int nper = 6;

  int ntask = comm->nworkers;
  int *nbufarr = new int[ntask];
  int nbuf = 0;
# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) reduction(+:nbuf) \
  shared(nlocal,ntask,npartner,partner_mask,partner_type,    \
         partner_massflag,partner_bondtype,partner_tag,mask,type, \
         rmass,mass,tag,nper,nbufarr)
# elif _OMPSS == 2
# pragma oss task for default(none) reduction(+:nbuf) \
  shared(nlocal,ntask,npartner,partner_mask,partner_type,    \
         partner_massflag,partner_bondtype,partner_tag,mask,type, \
         rmass,mass,tag,nper,nbufarr)
# endif
  for (int t=0; t< ntask; t++) {
    int st = nlocal/ntask;
    int mo = nlocal%ntask;
    int ini = t*st + ( t < mo ? t : mo );
    int end = ini + st + ( t < mo ? 1 : 0 );

    nbufarr[t] = 0;
    for (int i = ini; i < end; i++) {
      for (int j = 0; j < npartner[i]; j++) {
        partner_mask[i][j] = 0;
        partner_type[i][j] = 0;
        partner_massflag[i][j] = 0;
        partner_bondtype[i][j] = 0;

        int m = atom->map(partner_tag[i][j]);
        if (m >= 0 && m < nlocal) {
          partner_mask[i][j] = mask[m];
          partner_type[i][j] = type[m];
          if (nmass) {
            double massone = rmass ? rmass[m] : mass[type[m]];
            partner_massflag[i][j] = masscheck(massone);
          }
          int n = bondtype_findset(i,tag[i],partner_tag[i][j],0);
          if (n) partner_bondtype[i][j] = n;
          else {
            n = bondtype_findset(m,tag[i],partner_tag[i][j],0);
            if (n) partner_bondtype[i][j] = n;
          }
        } else nbufarr[t] += nper;
      }
    }
    nbuf += nbufarr[t];
  }  // Task loop
# if _OMPSS == 2
# pragma oss taskwait
# endif

  memory->create(buf,nbuf,"shake:buf");

  // fill buffer with info
# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) \
  shared(nlocal,ntask,npartner,partner_tag,buf,tag,nper,nbufarr)
# elif _OMPSS == 2
# pragma oss task for default(none) \
  shared(nlocal,ntask,npartner,partner_tag,buf,tag,nper,nbufarr)
# endif
  for (int t=0; t< ntask; t++) {
    int size = 0;
    for (int tt=0; tt< t; tt++) size += nbufarr[tt];
    int st = nlocal/ntask;
    int mo = nlocal%ntask;
    int ini = t*st + ( t < mo ? t : mo );
    int end = ini + st + ( t < mo ? 1 : 0 );
    for (int i = ini; i < end; i++) {
      for (int j = 0; j < npartner[i]; j++) {
        int m = atom->map(partner_tag[i][j]);
        if (m < 0 || m >= nlocal) {
          buf[size] = tag[i];
          buf[size+1] = partner_tag[i][j];
          buf[size+2] = 0;
          buf[size+3] = 0;
          buf[size+4] = 0;
          int n = bondtype_findset(i,tag[i],partner_tag[i][j],0);
          if (n) buf[size+5] = n;
          else buf[size+5] = 0;
          size += nper;
        }
      }
    }
  }  // Task loop
# if _OMPSS == 2
# pragma oss taskwait
# endif

  // cycle buffer around ring of procs back to self

  comm->ring(nbuf,sizeof(tagint),buf,1,ring_bonds,buf,(void *)this);

  // store partner info returned to me
# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) \
  shared(ntask,nbuf,nper,buf,npartner,partner_tag,partner_mask, \
         partner_type,partner_massflag,partner_bondtype)
# elif _OMPSS == 2
# pragma oss task for default(none) \
  shared(ntask,nbuf,nper,buf,npartner,partner_tag,partner_mask, \
         partner_type,partner_massflag,partner_bondtype)
# endif
  for (int t=0; t< ntask; t++) {
    int nn = nbuf/nper;
    int st = nn/ntask;
    int mo = nn%ntask;
    int ini = t*st + ( t < mo ? t : mo );
    int end = ini + st + ( t < mo ? 1 : 0 );
    for (int m=nper*ini; m< nper*end; m += nper ) {
      int j;
      int i = atom->map(buf[m]);
      for (j = 0; j < npartner[i]; j++)
        if (buf[m+1] == partner_tag[i][j]) break;
      partner_mask[i][j] = buf[m+2];
      partner_type[i][j] = buf[m+3];
      partner_massflag[i][j] = buf[m+4];
      partner_bondtype[i][j] = buf[m+5];
    }
  }  // Task loop
# if _OMPSS == 2
# pragma oss taskwait
# endif

  memory->destroy(buf);

  // error check for unfilled partner info
  // if partner_type not set, is an error
  // partner_bondtype may not be set if special list is not consistent
  //   with bondatom (e.g. due to delete_bonds command)
  // this is OK if one or both atoms are not in fix group, since
  //   bond won't be SHAKEn anyway
  // else it's an error

  int flag = 0;
# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) reduction(max:flag) \
  shared(nlocal,npartner,partner_type,mask,partner_mask, \
         partner_bondtype)
# elif _OMPSS == 2
# pragma oss task for default(none) reduction(max:flag) \
  shared(nlocal,npartner,partner_type,mask,partner_mask, \
         partner_bondtype)
# endif
  for (int i = 0; i < nlocal; i++)
    for (int j = 0; j < npartner[i]; j++) {
      if (partner_type[i][j] == 0) flag = 1;
      if (!(mask[i] & groupbit)) continue;
      if (!(partner_mask[i][j] & groupbit)) continue;
      if (partner_bondtype[i][j] == 0) flag = 1;
    }
# if _OMPSS == 2
# pragma oss taskwait
# endif

  int flag_all;
  MPI_Allreduce(&flag,&flag_all,1,MPI_INT,MPI_SUM,world);
  if (flag_all) error->all(FLERR,"Did not find fix shake partner info");

  // -----------------------------------------------------
  // identify SHAKEable bonds
  // set nshake[i] = # of SHAKE bonds attached to atom i
  // set partner_shake[i][] = 1 if SHAKE bonded to partner, 0 if not
  // both atoms must be in group, bondtype must be > 0
  // check if bondtype is in input bond_flag
  // check if type of either atom is in input type_flag
  // check if mass of either atom is in input mass_list
  // -----------------------------------------------------
# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) \
  shared(nlocal,ntask,npartner,partner_shake,mask,partner_mask, \
  partner_bondtype,type,partner_type,partner_massflag,rmass,mass)
# elif _OMPSS == 2
# pragma oss task for default(none) \
  shared(nlocal,ntask,npartner,partner_shake,mask,partner_mask, \
  partner_bondtype,type,partner_type,partner_massflag,rmass,mass)
# endif
  for (int t=0; t< ntask; t++) {
    int st = nlocal/ntask;
    int mo = nlocal%ntask;
    int ini = t*st + ( t < mo ? t : mo );
    int end = ini + st + ( t < mo ? 1 : 0 );
    for (int i = ini; i < end; i++) {
      nshake[i] = 0;
      int np = npartner[i];
      for (int j = 0; j < np; j++) {
        partner_shake[i][j] = 0;

        if (!(mask[i] & groupbit)) continue;
        if (!(partner_mask[i][j] & groupbit)) continue;
        if (partner_bondtype[i][j] <= 0) continue;

        if (bond_flag[partner_bondtype[i][j]]) {
          partner_shake[i][j] = 1;
          nshake[i]++;
          continue;
        }
        if (type_flag[type[i]] || type_flag[partner_type[i][j]]) {
          partner_shake[i][j] = 1;
          nshake[i]++;
          continue;
        }
        if (nmass) {
          if (partner_massflag[i][j]) {
            partner_shake[i][j] = 1;
            nshake[i]++;
            continue;
          } else {
            double massone = rmass? rmass[i] : mass[type[i]];
            if (masscheck(massone)) {
              partner_shake[i][j] = 1;
              nshake[i]++;
              continue;
            }
          }
        }
      }
    }
  }  // Task loop
# if _OMPSS == 2
# pragma oss taskwait
# endif

  // -----------------------------------------------------
  // set partner_nshake for bonded partners
  // requires communication for off-proc partners
  // -----------------------------------------------------

  // fill in partner_nshake if own bond partner
  // info to store in buf for each off-proc bond =
  //   2 atoms IDs in bond, space for nshake value
  // nbufmax = largest buffer needed to hold info from any proc
  nbuf = 0;
# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) reduction(+:nbuf) \
  shared(ntask,nlocal,nbufarr,npartner,partner_tag,partner_nshake)
# elif _OMPSS == 2
# pragma oss task for default(none) reduction(+:nbuf) \
  shared(ntask,nlocal,nbufarr,npartner,partner_tag,partner_nshake)
# endif
  for (int t=0; t< ntask; t++) {
    int st = nlocal/ntask;
    int mo = nlocal%ntask;
    int ini = t*st + ( t < mo ? t : mo );
    int end = ini + st + ( t < mo ? 1 : 0 );
    nbufarr[t] = 0;
    for (int i = ini; i < end; i++) {
      for (int j = 0; j < npartner[i]; j++) {
        int m = atom->map(partner_tag[i][j]);
        if (m >= 0 && m < nlocal) partner_nshake[i][j] = nshake[m];
        else nbufarr[t] += 3;
      }
    }
    nbuf += nbufarr[t];
  }  // Task loop
# if _OMPSS == 2
# pragma oss taskwait
# endif

  memory->create(buf,nbuf,"shake:buf");

  // fill buffer with info
# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp task default(none) \
  shared(nlocal,ntask,npartner,partner_tag,buf,tag,nbufarr)
# elif _OMPSS == 2
# pragma oss task default(none) \
  shared(nlocal,ntask,npartner,partner_tag,buf,tag,nbufarr)
# endif
  for (int t=0; t< ntask; t++) {
  	int size = 0;
    for (int tt=0; tt< t; tt++) size += nbufarr[tt];
    
    int st = nlocal/ntask;
    int mo = nlocal%ntask;
    int ini = t*st + ( t < mo ? t : mo );
    int end = ini + st + ( t < mo ? 1 : 0 );
    for (int i = ini; i < end; i++) {
      for (int j = 0; j < npartner[i]; j++) {
        int m = atom->map(partner_tag[i][j]);
        if (m < 0 || m >= nlocal) {
          buf[size] = tag[i];
          buf[size+1] = partner_tag[i][j];
          size += 3;
        }
      }
    }
  }  // Task loop
# if _OMPSS == 2
# pragma oss taskwait
# endif
  delete [] nbufarr;

  // cycle buffer around ring of procs back to self

  comm->ring(nbuf,sizeof(tagint),buf,2,ring_nshake,buf,(void *)this);

  // store partner info returned to me
# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) \
  shared(nbuf,ntask,npartner,buf,partner_tag,partner_nshake)
# elif _OMPSS == 2
# pragma oss task for default(none) \
  shared(nbuf,ntask,npartner,buf,partner_tag,partner_nshake)
# endif
  for (int t=0; t< ntask; t++) {
    int nn = nbuf/3;
    int st = nn/ntask;
    int mo = nn%ntask;
    int ini = t*st + ( t < mo ? t : mo );
    int end = ini + st + ( t < mo ? 1 : 0 );
    for (int m=3*ini; m< 3*end; m += 3 ) {
      int i = atom->map(buf[m]);
      int j;
      for (j = 0; j < npartner[i]; j++)
        if (buf[m+1] == partner_tag[i][j]) break;
      partner_nshake[i][j] = buf[m+2];
    }
  }  // Task loop
# if _OMPSS == 2
# pragma oss taskwait
# endif

  memory->destroy(buf);

  // -----------------------------------------------------
  // error checks
  // no atom with nshake > 3
  // no connected atoms which both have nshake > 1
  // -----------------------------------------------------

  flag = 0;
# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) reduction(max:flag) \
  shared(nlocal)
# elif _OMPSS == 2
# pragma oss task for default(none) reduction(max:flag) \
  shared(nlocal)
# endif
  for (int i = 0; i < nlocal; i++)
    if (nshake[i] > 3) flag = 1;
# if _OMPSS == 2
# pragma oss taskwait
# endif

  MPI_Allreduce(&flag,&flag_all,1,MPI_INT,MPI_SUM,world);
  if (flag_all) error->all(FLERR,"Shake cluster of more than 4 atoms");

  flag = 0;
# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) reduction(max:flag) \
  shared(nlocal,npartner,partner_shake,partner_nshake)
# elif _OMPSS == 2
# pragma oss task for default(none) reduction(max:flag) \
  shared(nlocal,npartner,partner_shake,partner_nshake)
# endif
  for (int i = 0; i < nlocal; i++) {
    if (nshake[i] <= 1) continue;
    for (int j = 0; j < npartner[i]; j++)
      if (partner_shake[i][j] && partner_nshake[i][j] > 1) flag = 1;
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif

  MPI_Allreduce(&flag,&flag_all,1,MPI_INT,MPI_SUM,world);
  if (flag_all) error->all(FLERR,"Shake clusters are connected");

  // -----------------------------------------------------
  // set SHAKE arrays that are stored with atoms & add angle constraints
  // zero shake arrays for all owned atoms
  // if I am central atom set shake_flag & shake_atom & shake_type
  // for 2-atom clusters, I am central atom if my atom ID < partner ID
  // for 3-atom clusters, test for angle constraint
  //   angle will be stored by this atom if it exists
  //   if angle type matches angle_flag, then it is angle-constrained
  // shake_flag[] = 0 if atom not in SHAKE cluster
  //                2,3,4 = size of bond-only cluster
  //                1 = 3-atom angle cluster
  // shake_atom[][] = global IDs of 2,3,4 atoms in cluster
  //                  central atom is 1st
  //                  for 2-atom cluster, lowest ID is 1st
  // shake_type[][] = bondtype of each bond in cluster
  //                  for 3-atom angle cluster, 3rd value is angletype
  // -----------------------------------------------------

# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) \
  shared(nlocal,npartner,partner_shake,partner_nshake,tag,partner_tag, \
  partner_bondtype,angles_allow)
# elif _OMPSS == 2
# pragma oss task for default(none) \
  shared(nlocal,npartner,partner_shake,partner_nshake,tag,partner_tag,\
  partner_bondtype,angles_allow)
# endif
  for (int i = 0; i < nlocal; i++) {
    shake_flag[i] = 0;
    shake_atom[i][0] = 0;
    shake_atom[i][1] = 0;
    shake_atom[i][2] = 0;
    shake_atom[i][3] = 0;
    shake_type[i][0] = 0;
    shake_type[i][1] = 0;
    shake_type[i][2] = 0;

    if (nshake[i] == 1) {
      for (int j = 0; j < npartner[i]; j++)
        if (partner_shake[i][j]) {
          if (partner_nshake[i][j] == 1 && tag[i] < partner_tag[i][j]) {
            shake_flag[i] = 2;
            shake_atom[i][0] = tag[i];
            shake_atom[i][1] = partner_tag[i][j];
            shake_type[i][0] = partner_bondtype[i][j];
          }
          break;
        }
    }

    if (nshake[i] > 1) {
      shake_flag[i] = 1;
      shake_atom[i][0] = tag[i];
      for (int j = 0; j < npartner[i]; j++)
        if (partner_shake[i][j]) {
          int m = shake_flag[i];
          shake_atom[i][m] = partner_tag[i][j];
          shake_type[i][m-1] = partner_bondtype[i][j];
          shake_flag[i]++;
        }
    }

    if (nshake[i] == 2 && angles_allow) {
      int n = angletype_findset(i,shake_atom[i][1],shake_atom[i][2],0);
      if (n <= 0) continue;
      if (angle_flag[n]) {
        shake_flag[i] = 1;
        shake_type[i][2] = n;
      }
    }
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif

  // -----------------------------------------------------
  // set shake_flag,shake_atom,shake_type for non-central atoms
  // requires communication for off-proc atoms
  // -----------------------------------------------------

  // fill in shake arrays for each bond partner I own
  // info to store in buf for each off-proc bond =
  //   all values from shake_flag, shake_atom, shake_type
  // nbufmax = largest buffer needed to hold info from any proc

  nbuf = 0;
  for (int i = 0; i < nlocal; i++) {
    if (shake_flag[i] == 0) continue;
    for (int j = 0; j < npartner[i]; j++) {
      if (partner_shake[i][j] == 0) continue;
      int m = atom->map(partner_tag[i][j]);
      if (m >= 0 && m < nlocal) {
        shake_flag[m] = shake_flag[i];
        shake_atom[m][0] = shake_atom[i][0];
        shake_atom[m][1] = shake_atom[i][1];
        shake_atom[m][2] = shake_atom[i][2];
        shake_atom[m][3] = shake_atom[i][3];
        shake_type[m][0] = shake_type[i][0];
        shake_type[m][1] = shake_type[i][1];
        shake_type[m][2] = shake_type[i][2];
      } else nbuf += 9;
    }
  }

  memory->create(buf,nbuf,"shake:buf");

  // fill buffer with info

  int size = 0;
  for (int i = 0; i < nlocal; i++) {
    if (shake_flag[i] == 0) continue;
    for (int j = 0; j < npartner[i]; j++) {
      if (partner_shake[i][j] == 0) continue;
      int m = atom->map(partner_tag[i][j]);
      if (m < 0 || m >= nlocal) {
        buf[size] = partner_tag[i][j];
        buf[size+1] = shake_flag[i];
        buf[size+2] = shake_atom[i][0];
        buf[size+3] = shake_atom[i][1];
        buf[size+4] = shake_atom[i][2];
        buf[size+5] = shake_atom[i][3];
        buf[size+6] = shake_type[i][0];
        buf[size+7] = shake_type[i][1];
        buf[size+8] = shake_type[i][2];
        size += 9;
      }
    }
  }

  // cycle buffer around ring of procs back to self

  comm->ring(size,sizeof(tagint),buf,3,ring_shake,NULL,(void *)this);

  memory->destroy(buf);

  // -----------------------------------------------------
  // free local memory
  // -----------------------------------------------------

  memory->destroy(npartner);
  memory->destroy(nshake);
  memory->destroy(partner_tag);
  memory->destroy(partner_mask);
  memory->destroy(partner_type);
  memory->destroy(partner_massflag);
  memory->destroy(partner_bondtype);
  memory->destroy(partner_shake);
  memory->destroy(partner_nshake);

  // -----------------------------------------------------
  // set bond_type and angle_type negative for SHAKE clusters
  // must set for all SHAKE bonds and angles stored by each atom
  // -----------------------------------------------------

# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) \
  shared(nlocal)
# elif _OMPSS == 2
# pragma oss task for default(none) \
  shared(nlocal)
# endif
  for (int i = 0; i < nlocal; i++) {
    if (shake_flag[i] == 0) continue;
    else if (shake_flag[i] == 1) {
      bondtype_findset(i,shake_atom[i][0],shake_atom[i][1],-1);
      bondtype_findset(i,shake_atom[i][0],shake_atom[i][2],-1);
      angletype_findset(i,shake_atom[i][1],shake_atom[i][2],-1);
    } else if (shake_flag[i] == 2) {
      bondtype_findset(i,shake_atom[i][0],shake_atom[i][1],-1);
    } else if (shake_flag[i] == 3) {
      bondtype_findset(i,shake_atom[i][0],shake_atom[i][1],-1);
      bondtype_findset(i,shake_atom[i][0],shake_atom[i][2],-1);
    } else if (shake_flag[i] == 4) {
      bondtype_findset(i,shake_atom[i][0],shake_atom[i][1],-1);
      bondtype_findset(i,shake_atom[i][0],shake_atom[i][2],-1);
      bondtype_findset(i,shake_atom[i][0],shake_atom[i][3],-1);
    }
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif

  // -----------------------------------------------------
  // print info on SHAKE clusters
  // -----------------------------------------------------

  int count1,count2,count3,count4;
  count1 = count2 = count3 = count4 = 0;
# if   _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) \
  reduction(+:count1) reduction(+:count2) \
  reduction(+:count3) reduction(+:count4) \
  shared(nlocal)
# elif _OMPSS == 2
# pragma oss task for default(none) \
  reduction(+:count1) reduction(+:count2) \
  reduction(+:count3) reduction(+:count4) \
  shared(nlocal)
# endif
  for (int i = 0; i < nlocal; i++) {
    if (shake_flag[i] == 1) count1++;
    else if (shake_flag[i] == 2) count2++;
    else if (shake_flag[i] == 3) count3++;
    else if (shake_flag[i] == 4) count4++;
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif

  int tmp;
  tmp = count1;
  MPI_Allreduce(&tmp,&count1,1,MPI_INT,MPI_SUM,world);
  tmp = count2;
  MPI_Allreduce(&tmp,&count2,1,MPI_INT,MPI_SUM,world);
  tmp = count3;
  MPI_Allreduce(&tmp,&count3,1,MPI_INT,MPI_SUM,world);
  tmp = count4;
  MPI_Allreduce(&tmp,&count4,1,MPI_INT,MPI_SUM,world);

  if (me == 0) {
    if (screen) {
      fprintf(screen,"  %d = # of size 2 clusters\n",count2/2);
      fprintf(screen,"  %d = # of size 3 clusters\n",count3/3);
      fprintf(screen,"  %d = # of size 4 clusters\n",count4/4);
      fprintf(screen,"  %d = # of frozen angles\n",count1/3);
    }
    if (logfile) {
      fprintf(logfile,"  %d = # of size 2 clusters\n",count2/2);
      fprintf(logfile,"  %d = # of size 3 clusters\n",count3/3);
      fprintf(logfile,"  %d = # of size 4 clusters\n",count4/4);
      fprintf(logfile,"  %d = # of frozen angles\n",count1/3);
    }
  }
}

/* ----------------------------------------------------------------------
   update the unconstrained position of each atom
   only for SHAKE clusters, else set to 0.0
   assumes NVE update, seems to be accurate enough for NVT,NPT,NPH as well
------------------------------------------------------------------------- */

void FixShakeOMPSS::unconstrained_update()
{
  if (rmass) {
# 	if _OMPSS == 1 || defined(_OPENMP)
#   pragma omp parallel for default(none)
#   elif _OMPSS == 2
#   pragma oss task for default(none)
# 	endif
    for (int i = 0; i < nlocal; i++) {
      if (shake_flag[i]) {
        double dtfmsq = dtfsq / rmass[i];
        xshake[i][0] = x[i][0] + dtv*v[i][0] + dtfmsq*f[i][0];
        xshake[i][1] = x[i][1] + dtv*v[i][1] + dtfmsq*f[i][1];
        xshake[i][2] = x[i][2] + dtv*v[i][2] + dtfmsq*f[i][2];
      } else xshake[i][2] = xshake[i][1] = xshake[i][0] = 0.0;
    }
  } else {
# 	if _OMPSS == 1 || defined(_OPENMP)
#   pragma omp parallel for default(none)
# 	elif _OMPSS == 2
#   pragma oss task for default(none)
#   endif
    for (int i = 0; i < nlocal; i++) {
      if (shake_flag[i]) {
        double dtfmsq = dtfsq / mass[type[i]];
        xshake[i][0] = x[i][0] + dtv*v[i][0] + dtfmsq*f[i][0];
        xshake[i][1] = x[i][1] + dtv*v[i][1] + dtfmsq*f[i][1];
        xshake[i][2] = x[i][2] + dtv*v[i][2] + dtfmsq*f[i][2];
      } else xshake[i][2] = xshake[i][1] = xshake[i][0] = 0.0;
    }
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif
}

/* ---------------------------------------------------------------------- */

void FixShakeOMPSS::shake(int m, dbl3_t *frc, ThrOMPSS *thr )
{
  int nlist,list[2];
  double v[6];
  double invmass0,invmass1;

  // local atom IDs and constraint distances

  int i0 = atom->map(shake_atom[m][0]);
  int i1 = atom->map(shake_atom[m][1]);
  double bond1 = bond_distance[shake_type[m][0]];

  // r01 = distance vec between atoms, with PBC

  double r01[3];
  r01[0] = x[i0][0] - x[i1][0];
  r01[1] = x[i0][1] - x[i1][1];
  r01[2] = x[i0][2] - x[i1][2];
  domain->minimum_image(r01);

  // s01 = distance vec after unconstrained update, with PBC
  // use Domain::minimum_image_once(), not minimum_image()
  // b/c xshake values might be huge, due to e.g. fix gcmc

  double s01[3];
  s01[0] = xshake[i0][0] - xshake[i1][0];
  s01[1] = xshake[i0][1] - xshake[i1][1];
  s01[2] = xshake[i0][2] - xshake[i1][2];
  domain->minimum_image_once(s01);

  // scalar distances between atoms

  double r01sq = r01[0]*r01[0] + r01[1]*r01[1] + r01[2]*r01[2];
  double s01sq = s01[0]*s01[0] + s01[1]*s01[1] + s01[2]*s01[2];

  // a,b,c = coeffs in quadratic equation for lamda

  if (rmass) {
    invmass0 = 1.0/rmass[i0];
    invmass1 = 1.0/rmass[i1];
  } else {
    invmass0 = 1.0/mass[type[i0]];
    invmass1 = 1.0/mass[type[i1]];
  }

  double a = (invmass0+invmass1)*(invmass0+invmass1) * r01sq;
  double b = 2.0 * (invmass0+invmass1) *
    (s01[0]*r01[0] + s01[1]*r01[1] + s01[2]*r01[2]);
  double c = s01sq - bond1*bond1;

  // error check

  double determ = b*b - 4.0*a*c;
  if (determ < 0.0) {
    error->warning(FLERR,"Shake determinant < 0.0",0);
    determ = 0.0;
  }

  // exact quadratic solution for lamda

  double lamda,lamda1,lamda2;
  lamda1 = (-b+sqrt(determ)) / (2.0*a);
  lamda2 = (-b-sqrt(determ)) / (2.0*a);

  if (fabs(lamda1) <= fabs(lamda2)) lamda = lamda1;
  else lamda = lamda2;

  // update forces if atom is owned by this processor

  lamda /= dtfsq;

  if (i0 < nlocal) {
    frc[i0].x += lamda*r01[0];
    frc[i0].y += lamda*r01[1];
    frc[i0].z += lamda*r01[2];
  }

  if (i1 < nlocal) {
    frc[i1].x -= lamda*r01[0];
    frc[i1].y -= lamda*r01[1];
    frc[i1].z -= lamda*r01[2];
  }

  if (evflag) {
    nlist = 0;
    if (i0 < nlocal) list[nlist++] = i0;
    if (i1 < nlocal) list[nlist++] = i1;

    v[0] = lamda*r01[0]*r01[0];
    v[1] = lamda*r01[1]*r01[1];
    v[2] = lamda*r01[2]*r01[2];
    v[3] = lamda*r01[0]*r01[1];
    v[4] = lamda*r01[0]*r01[2];
    v[5] = lamda*r01[1]*r01[2];

    thr->v_tally_thr( vflag_global, vflag_atom, nlist, list, 2.0, v );
  }
}

/* ---------------------------------------------------------------------- */

void FixShakeOMPSS::shake3(int m, dbl3_t *frc, ThrOMPSS *thr )
{
  int nlist,list[3];
  double v[6];
  double invmass0,invmass1,invmass2;

  // local atom IDs and constraint distances

  int i0 = atom->map(shake_atom[m][0]);
  int i1 = atom->map(shake_atom[m][1]);
  int i2 = atom->map(shake_atom[m][2]);
  double bond1 = bond_distance[shake_type[m][0]];
  double bond2 = bond_distance[shake_type[m][1]];

  // r01,r02 = distance vec between atoms, with PBC

  double r01[3];
  r01[0] = x[i0][0] - x[i1][0];
  r01[1] = x[i0][1] - x[i1][1];
  r01[2] = x[i0][2] - x[i1][2];
  domain->minimum_image(r01);

  double r02[3];
  r02[0] = x[i0][0] - x[i2][0];
  r02[1] = x[i0][1] - x[i2][1];
  r02[2] = x[i0][2] - x[i2][2];
  domain->minimum_image(r02);

  // s01,s02 = distance vec after unconstrained update, with PBC
  // use Domain::minimum_image_once(), not minimum_image()
  // b/c xshake values might be huge, due to e.g. fix gcmc

  double s01[3];
  s01[0] = xshake[i0][0] - xshake[i1][0];
  s01[1] = xshake[i0][1] - xshake[i1][1];
  s01[2] = xshake[i0][2] - xshake[i1][2];
  domain->minimum_image_once(s01);

  double s02[3];
  s02[0] = xshake[i0][0] - xshake[i2][0];
  s02[1] = xshake[i0][1] - xshake[i2][1];
  s02[2] = xshake[i0][2] - xshake[i2][2];
  domain->minimum_image_once(s02);

  // scalar distances between atoms

  double r01sq = r01[0]*r01[0] + r01[1]*r01[1] + r01[2]*r01[2];
  double r02sq = r02[0]*r02[0] + r02[1]*r02[1] + r02[2]*r02[2];
  double s01sq = s01[0]*s01[0] + s01[1]*s01[1] + s01[2]*s01[2];
  double s02sq = s02[0]*s02[0] + s02[1]*s02[1] + s02[2]*s02[2];

  // matrix coeffs and rhs for lamda equations

  if (rmass) {
    invmass0 = 1.0/rmass[i0];
    invmass1 = 1.0/rmass[i1];
    invmass2 = 1.0/rmass[i2];
  } else {
    invmass0 = 1.0/mass[type[i0]];
    invmass1 = 1.0/mass[type[i1]];
    invmass2 = 1.0/mass[type[i2]];
  }

  double a11 = 2.0 * (invmass0+invmass1) *
    (s01[0]*r01[0] + s01[1]*r01[1] + s01[2]*r01[2]);
  double a12 = 2.0 * invmass0 *
    (s01[0]*r02[0] + s01[1]*r02[1] + s01[2]*r02[2]);
  double a21 = 2.0 * invmass0 *
    (s02[0]*r01[0] + s02[1]*r01[1] + s02[2]*r01[2]);
  double a22 = 2.0 * (invmass0+invmass2) *
    (s02[0]*r02[0] + s02[1]*r02[1] + s02[2]*r02[2]);

  // inverse of matrix

  double determ = a11*a22 - a12*a21;
  if (determ == 0.0) error->one(FLERR,"Shake determinant = 0.0");
  double determinv = 1.0/determ;

  double a11inv = a22*determinv;
  double a12inv = -a12*determinv;
  double a21inv = -a21*determinv;
  double a22inv = a11*determinv;

  // quadratic correction coeffs

  double r0102 = (r01[0]*r02[0] + r01[1]*r02[1] + r01[2]*r02[2]);

  double quad1_0101 = (invmass0+invmass1)*(invmass0+invmass1) * r01sq;
  double quad1_0202 = invmass0*invmass0 * r02sq;
  double quad1_0102 = 2.0 * (invmass0+invmass1)*invmass0 * r0102;

  double quad2_0202 = (invmass0+invmass2)*(invmass0+invmass2) * r02sq;
  double quad2_0101 = invmass0*invmass0 * r01sq;
  double quad2_0102 = 2.0 * (invmass0+invmass2)*invmass0 * r0102;

  // iterate until converged

  double lamda01 = 0.0;
  double lamda02 = 0.0;
  int niter = 0;
  int done = 0;

  double quad1,quad2,b1,b2,lamda01_new,lamda02_new;

  while (!done && niter < max_iter) {
    quad1 = quad1_0101 * lamda01*lamda01 + quad1_0202 * lamda02*lamda02 +
      quad1_0102 * lamda01*lamda02;
    quad2 = quad2_0101 * lamda01*lamda01 + quad2_0202 * lamda02*lamda02 +
      quad2_0102 * lamda01*lamda02;

    b1 = bond1*bond1 - s01sq - quad1;
    b2 = bond2*bond2 - s02sq - quad2;

    lamda01_new = a11inv*b1 + a12inv*b2;
    lamda02_new = a21inv*b1 + a22inv*b2;

    done = 1;
    if (fabs(lamda01_new-lamda01) > tolerance) done = 0;
    if (fabs(lamda02_new-lamda02) > tolerance) done = 0;

    lamda01 = lamda01_new;
    lamda02 = lamda02_new;

    // stop iterations before we have a floating point overflow
    // max double is < 1.0e308, so 1e150 is a reasonable cutoff

    if (fabs(lamda01) > 1e150 || fabs(lamda02) > 1e150) done = 1;

    niter++;
  }

  // update forces if atom is owned by this processor

  lamda01 = lamda01/dtfsq;
  lamda02 = lamda02/dtfsq;

  if (i0 < nlocal) {
    frc[i0].x += lamda01*r01[0] + lamda02*r02[0];
    frc[i0].y += lamda01*r01[1] + lamda02*r02[1];
    frc[i0].z += lamda01*r01[2] + lamda02*r02[2];
  }

  if (i1 < nlocal) {
    frc[i1].x -= lamda01*r01[0];
    frc[i1].y -= lamda01*r01[1];
    frc[i1].z -= lamda01*r01[2];
  }

  if (i2 < nlocal) {
    frc[i2].x -= lamda02*r02[0];
    frc[i2].y -= lamda02*r02[1];
    frc[i2].z -= lamda02*r02[2];
  }

  if (evflag) {
    nlist = 0;
    if (i0 < nlocal) list[nlist++] = i0;
    if (i1 < nlocal) list[nlist++] = i1;
    if (i2 < nlocal) list[nlist++] = i2;

    v[0] = lamda01*r01[0]*r01[0] + lamda02*r02[0]*r02[0];
    v[1] = lamda01*r01[1]*r01[1] + lamda02*r02[1]*r02[1];
    v[2] = lamda01*r01[2]*r01[2] + lamda02*r02[2]*r02[2];
    v[3] = lamda01*r01[0]*r01[1] + lamda02*r02[0]*r02[1];
    v[4] = lamda01*r01[0]*r01[2] + lamda02*r02[0]*r02[2];
    v[5] = lamda01*r01[1]*r01[2] + lamda02*r02[1]*r02[2];

    thr->v_tally_thr( vflag_global, vflag_atom, nlist, list, 3.0, v );
  }
}

/* ---------------------------------------------------------------------- */

void FixShakeOMPSS::shake4( int m, dbl3_t *frc, ThrOMPSS *thr )
{
  int nlist,list[4];
  double v[6];
  double invmass0,invmass1,invmass2,invmass3;

  // local atom IDs and constraint distances

  int i0 = atom->map(shake_atom[m][0]);
  int i1 = atom->map(shake_atom[m][1]);
  int i2 = atom->map(shake_atom[m][2]);
  int i3 = atom->map(shake_atom[m][3]);
  double bond1 = bond_distance[shake_type[m][0]];
  double bond2 = bond_distance[shake_type[m][1]];
  double bond3 = bond_distance[shake_type[m][2]];

  // r01,r02,r03 = distance vec between atoms, with PBC

  double r01[3];
  r01[0] = x[i0][0] - x[i1][0];
  r01[1] = x[i0][1] - x[i1][1];
  r01[2] = x[i0][2] - x[i1][2];
  domain->minimum_image(r01);

  double r02[3];
  r02[0] = x[i0][0] - x[i2][0];
  r02[1] = x[i0][1] - x[i2][1];
  r02[2] = x[i0][2] - x[i2][2];
  domain->minimum_image(r02);

  double r03[3];
  r03[0] = x[i0][0] - x[i3][0];
  r03[1] = x[i0][1] - x[i3][1];
  r03[2] = x[i0][2] - x[i3][2];
  domain->minimum_image(r03);

  // s01,s02,s03 = distance vec after unconstrained update, with PBC
  // use Domain::minimum_image_once(), not minimum_image()
  // b/c xshake values might be huge, due to e.g. fix gcmc

  double s01[3];
  s01[0] = xshake[i0][0] - xshake[i1][0];
  s01[1] = xshake[i0][1] - xshake[i1][1];
  s01[2] = xshake[i0][2] - xshake[i1][2];
  domain->minimum_image_once(s01);

  double s02[3];
  s02[0] = xshake[i0][0] - xshake[i2][0];
  s02[1] = xshake[i0][1] - xshake[i2][1];
  s02[2] = xshake[i0][2] - xshake[i2][2];
  domain->minimum_image_once(s02);

  double s03[3];
  s03[0] = xshake[i0][0] - xshake[i3][0];
  s03[1] = xshake[i0][1] - xshake[i3][1];
  s03[2] = xshake[i0][2] - xshake[i3][2];
  domain->minimum_image_once(s03);

  // scalar distances between atoms

  double r01sq = r01[0]*r01[0] + r01[1]*r01[1] + r01[2]*r01[2];
  double r02sq = r02[0]*r02[0] + r02[1]*r02[1] + r02[2]*r02[2];
  double r03sq = r03[0]*r03[0] + r03[1]*r03[1] + r03[2]*r03[2];
  double s01sq = s01[0]*s01[0] + s01[1]*s01[1] + s01[2]*s01[2];
  double s02sq = s02[0]*s02[0] + s02[1]*s02[1] + s02[2]*s02[2];
  double s03sq = s03[0]*s03[0] + s03[1]*s03[1] + s03[2]*s03[2];

  // matrix coeffs and rhs for lamda equations

  if (rmass) {
    invmass0 = 1.0/rmass[i0];
    invmass1 = 1.0/rmass[i1];
    invmass2 = 1.0/rmass[i2];
    invmass3 = 1.0/rmass[i3];
  } else {
    invmass0 = 1.0/mass[type[i0]];
    invmass1 = 1.0/mass[type[i1]];
    invmass2 = 1.0/mass[type[i2]];
    invmass3 = 1.0/mass[type[i3]];
  }

  double a11 = 2.0 * (invmass0+invmass1) *
    (s01[0]*r01[0] + s01[1]*r01[1] + s01[2]*r01[2]);
  double a12 = 2.0 * invmass0 *
    (s01[0]*r02[0] + s01[1]*r02[1] + s01[2]*r02[2]);
  double a13 = 2.0 * invmass0 *
    (s01[0]*r03[0] + s01[1]*r03[1] + s01[2]*r03[2]);
  double a21 = 2.0 * invmass0 *
    (s02[0]*r01[0] + s02[1]*r01[1] + s02[2]*r01[2]);
  double a22 = 2.0 * (invmass0+invmass2) *
    (s02[0]*r02[0] + s02[1]*r02[1] + s02[2]*r02[2]);
  double a23 = 2.0 * invmass0 *
    (s02[0]*r03[0] + s02[1]*r03[1] + s02[2]*r03[2]);
  double a31 = 2.0 * invmass0 *
    (s03[0]*r01[0] + s03[1]*r01[1] + s03[2]*r01[2]);
  double a32 = 2.0 * invmass0 *
    (s03[0]*r02[0] + s03[1]*r02[1] + s03[2]*r02[2]);
  double a33 = 2.0 * (invmass0+invmass3) *
    (s03[0]*r03[0] + s03[1]*r03[1] + s03[2]*r03[2]);

  // inverse of matrix;

  double determ = a11*a22*a33 + a12*a23*a31 + a13*a21*a32 -
    a11*a23*a32 - a12*a21*a33 - a13*a22*a31;
  if (determ == 0.0) error->one(FLERR,"Shake determinant = 0.0");
  double determinv = 1.0/determ;

  double a11inv = determinv * (a22*a33 - a23*a32);
  double a12inv = -determinv * (a12*a33 - a13*a32);
  double a13inv = determinv * (a12*a23 - a13*a22);
  double a21inv = -determinv * (a21*a33 - a23*a31);
  double a22inv = determinv * (a11*a33 - a13*a31);
  double a23inv = -determinv * (a11*a23 - a13*a21);
  double a31inv = determinv * (a21*a32 - a22*a31);
  double a32inv = -determinv * (a11*a32 - a12*a31);
  double a33inv = determinv * (a11*a22 - a12*a21);

  // quadratic correction coeffs

  double r0102 = (r01[0]*r02[0] + r01[1]*r02[1] + r01[2]*r02[2]);
  double r0103 = (r01[0]*r03[0] + r01[1]*r03[1] + r01[2]*r03[2]);
  double r0203 = (r02[0]*r03[0] + r02[1]*r03[1] + r02[2]*r03[2]);

  double quad1_0101 = (invmass0+invmass1)*(invmass0+invmass1) * r01sq;
  double quad1_0202 = invmass0*invmass0 * r02sq;
  double quad1_0303 = invmass0*invmass0 * r03sq;
  double quad1_0102 = 2.0 * (invmass0+invmass1)*invmass0 * r0102;
  double quad1_0103 = 2.0 * (invmass0+invmass1)*invmass0 * r0103;
  double quad1_0203 = 2.0 * invmass0*invmass0 * r0203;

  double quad2_0101 = invmass0*invmass0 * r01sq;
  double quad2_0202 = (invmass0+invmass2)*(invmass0+invmass2) * r02sq;
  double quad2_0303 = invmass0*invmass0 * r03sq;
  double quad2_0102 = 2.0 * (invmass0+invmass2)*invmass0 * r0102;
  double quad2_0103 = 2.0 * invmass0*invmass0 * r0103;
  double quad2_0203 = 2.0 * (invmass0+invmass2)*invmass0 * r0203;

  double quad3_0101 = invmass0*invmass0 * r01sq;
  double quad3_0202 = invmass0*invmass0 * r02sq;
  double quad3_0303 = (invmass0+invmass3)*(invmass0+invmass3) * r03sq;
  double quad3_0102 = 2.0 * invmass0*invmass0 * r0102;
  double quad3_0103 = 2.0 * (invmass0+invmass3)*invmass0 * r0103;
  double quad3_0203 = 2.0 * (invmass0+invmass3)*invmass0 * r0203;

  // iterate until converged

  double lamda01 = 0.0;
  double lamda02 = 0.0;
  double lamda03 = 0.0;
  int niter = 0;
  int done = 0;

  double quad1,quad2,quad3,b1,b2,b3,lamda01_new,lamda02_new,lamda03_new;

  while (!done && niter < max_iter) {
    quad1 = quad1_0101 * lamda01*lamda01 +
      quad1_0202 * lamda02*lamda02 +
      quad1_0303 * lamda03*lamda03 +
      quad1_0102 * lamda01*lamda02 +
      quad1_0103 * lamda01*lamda03 +
      quad1_0203 * lamda02*lamda03;

    quad2 = quad2_0101 * lamda01*lamda01 +
      quad2_0202 * lamda02*lamda02 +
      quad2_0303 * lamda03*lamda03 +
      quad2_0102 * lamda01*lamda02 +
      quad2_0103 * lamda01*lamda03 +
      quad2_0203 * lamda02*lamda03;

    quad3 = quad3_0101 * lamda01*lamda01 +
      quad3_0202 * lamda02*lamda02 +
      quad3_0303 * lamda03*lamda03 +
      quad3_0102 * lamda01*lamda02 +
      quad3_0103 * lamda01*lamda03 +
      quad3_0203 * lamda02*lamda03;

    b1 = bond1*bond1 - s01sq - quad1;
    b2 = bond2*bond2 - s02sq - quad2;
    b3 = bond3*bond3 - s03sq - quad3;

    lamda01_new = a11inv*b1 + a12inv*b2 + a13inv*b3;
    lamda02_new = a21inv*b1 + a22inv*b2 + a23inv*b3;
    lamda03_new = a31inv*b1 + a32inv*b2 + a33inv*b3;

    done = 1;
    if (fabs(lamda01_new-lamda01) > tolerance) done = 0;
    if (fabs(lamda02_new-lamda02) > tolerance) done = 0;
    if (fabs(lamda03_new-lamda03) > tolerance) done = 0;

    lamda01 = lamda01_new;
    lamda02 = lamda02_new;
    lamda03 = lamda03_new;

    // stop iterations before we have a floating point overflow
    // max double is < 1.0e308, so 1e150 is a reasonable cutoff

    if (fabs(lamda01) > 1e150 || fabs(lamda02) > 1e150
        || fabs(lamda03) > 1e150) done = 1;

    niter++;
  }

  // update forces if atom is owned by this processor

  lamda01 = lamda01/dtfsq;
  lamda02 = lamda02/dtfsq;
  lamda03 = lamda03/dtfsq;

  if (i0 < nlocal) {
    frc[i0].x += lamda01*r01[0] + lamda02*r02[0] + lamda03*r03[0];
    frc[i0].y += lamda01*r01[1] + lamda02*r02[1] + lamda03*r03[1];
    frc[i0].z += lamda01*r01[2] + lamda02*r02[2] + lamda03*r03[2];
  }

  if (i1 < nlocal) {
    frc[i1].x -= lamda01*r01[0];
    frc[i1].y -= lamda01*r01[1];
    frc[i1].z -= lamda01*r01[2];
  }

  if (i2 < nlocal) {
    frc[i2].x -= lamda02*r02[0];
    frc[i2].y -= lamda02*r02[1];
    frc[i2].z -= lamda02*r02[2];
  }

  if (i3 < nlocal) {
    frc[i3].x -= lamda03*r03[0];
    frc[i3].y -= lamda03*r03[1];
    frc[i3].z -= lamda03*r03[2];
  }

  if (evflag) {
    nlist = 0;
    if (i0 < nlocal) list[nlist++] = i0;
    if (i1 < nlocal) list[nlist++] = i1;
    if (i2 < nlocal) list[nlist++] = i2;
    if (i3 < nlocal) list[nlist++] = i3;

    v[0] = lamda01*r01[0]*r01[0]+lamda02*r02[0]*r02[0]+lamda03*r03[0]*r03[0];
    v[1] = lamda01*r01[1]*r01[1]+lamda02*r02[1]*r02[1]+lamda03*r03[1]*r03[1];
    v[2] = lamda01*r01[2]*r01[2]+lamda02*r02[2]*r02[2]+lamda03*r03[2]*r03[2];
    v[3] = lamda01*r01[0]*r01[1]+lamda02*r02[0]*r02[1]+lamda03*r03[0]*r03[1];
    v[4] = lamda01*r01[0]*r01[2]+lamda02*r02[0]*r02[2]+lamda03*r03[0]*r03[2];
    v[5] = lamda01*r01[1]*r01[2]+lamda02*r02[1]*r02[2]+lamda03*r03[1]*r03[2];

    thr->v_tally_thr( vflag_global, vflag_atom, nlist, list, 4.0, v );
  }
}

/* ---------------------------------------------------------------------- */

void FixShakeOMPSS::shake3angle(int m, dbl3_t *frc, ThrOMPSS *thr )
{
  int nlist,list[3];
  double v[6];
  double invmass0,invmass1,invmass2;

  // local atom IDs and constraint distances

  int i0 = atom->map(shake_atom[m][0]);
  int i1 = atom->map(shake_atom[m][1]);
  int i2 = atom->map(shake_atom[m][2]);
  double bond1 = bond_distance[shake_type[m][0]];
  double bond2 = bond_distance[shake_type[m][1]];
  double bond12 = angle_distance[shake_type[m][2]];

  // r01,r02,r12 = distance vec between atoms, with PBC

  double r01[3];
  r01[0] = x[i0][0] - x[i1][0];
  r01[1] = x[i0][1] - x[i1][1];
  r01[2] = x[i0][2] - x[i1][2];
  domain->minimum_image(r01);

  double r02[3];
  r02[0] = x[i0][0] - x[i2][0];
  r02[1] = x[i0][1] - x[i2][1];
  r02[2] = x[i0][2] - x[i2][2];
  domain->minimum_image(r02);

  double r12[3];
  r12[0] = x[i1][0] - x[i2][0];
  r12[1] = x[i1][1] - x[i2][1];
  r12[2] = x[i1][2] - x[i2][2];
  domain->minimum_image(r12);

  // s01,s02,s12 = distance vec after unconstrained update, with PBC
  // use Domain::minimum_image_once(), not minimum_image()
  // b/c xshake values might be huge, due to e.g. fix gcmc

  double s01[3];
  s01[0] = xshake[i0][0] - xshake[i1][0];
  s01[1] = xshake[i0][1] - xshake[i1][1];
  s01[2] = xshake[i0][2] - xshake[i1][2];
  domain->minimum_image_once(s01);

  double s02[3];
  s02[0] = xshake[i0][0] - xshake[i2][0];
  s02[1] = xshake[i0][1] - xshake[i2][1];
  s02[2] = xshake[i0][2] - xshake[i2][2];
  domain->minimum_image_once(s02);

  double s12[3];
  s12[0] = xshake[i1][0] - xshake[i2][0];
  s12[1] = xshake[i1][1] - xshake[i2][1];
  s12[2] = xshake[i1][2] - xshake[i2][2];
  domain->minimum_image_once(s12);

  // scalar distances between atoms

  double r01sq = r01[0]*r01[0] + r01[1]*r01[1] + r01[2]*r01[2];
  double r02sq = r02[0]*r02[0] + r02[1]*r02[1] + r02[2]*r02[2];
  double r12sq = r12[0]*r12[0] + r12[1]*r12[1] + r12[2]*r12[2];
  double s01sq = s01[0]*s01[0] + s01[1]*s01[1] + s01[2]*s01[2];
  double s02sq = s02[0]*s02[0] + s02[1]*s02[1] + s02[2]*s02[2];
  double s12sq = s12[0]*s12[0] + s12[1]*s12[1] + s12[2]*s12[2];

  // matrix coeffs and rhs for lamda equations

  if (rmass) {
    invmass0 = 1.0/rmass[i0];
    invmass1 = 1.0/rmass[i1];
    invmass2 = 1.0/rmass[i2];
  } else {
    invmass0 = 1.0/mass[type[i0]];
    invmass1 = 1.0/mass[type[i1]];
    invmass2 = 1.0/mass[type[i2]];
  }

  double a11 = 2.0 * (invmass0+invmass1) *
    (s01[0]*r01[0] + s01[1]*r01[1] + s01[2]*r01[2]);
  double a12 = 2.0 * invmass0 *
    (s01[0]*r02[0] + s01[1]*r02[1] + s01[2]*r02[2]);
  double a13 = - 2.0 * invmass1 *
    (s01[0]*r12[0] + s01[1]*r12[1] + s01[2]*r12[2]);
  double a21 = 2.0 * invmass0 *
    (s02[0]*r01[0] + s02[1]*r01[1] + s02[2]*r01[2]);
  double a22 = 2.0 * (invmass0+invmass2) *
    (s02[0]*r02[0] + s02[1]*r02[1] + s02[2]*r02[2]);
  double a23 = 2.0 * invmass2 *
    (s02[0]*r12[0] + s02[1]*r12[1] + s02[2]*r12[2]);
  double a31 = - 2.0 * invmass1 *
    (s12[0]*r01[0] + s12[1]*r01[1] + s12[2]*r01[2]);
  double a32 = 2.0 * invmass2 *
    (s12[0]*r02[0] + s12[1]*r02[1] + s12[2]*r02[2]);
  double a33 = 2.0 * (invmass1+invmass2) *
    (s12[0]*r12[0] + s12[1]*r12[1] + s12[2]*r12[2]);

  // inverse of matrix

  double determ = a11*a22*a33 + a12*a23*a31 + a13*a21*a32 -
    a11*a23*a32 - a12*a21*a33 - a13*a22*a31;
  if (determ == 0.0) error->one(FLERR,"Shake determinant = 0.0");
  double determinv = 1.0/determ;

  double a11inv = determinv * (a22*a33 - a23*a32);
  double a12inv = -determinv * (a12*a33 - a13*a32);
  double a13inv = determinv * (a12*a23 - a13*a22);
  double a21inv = -determinv * (a21*a33 - a23*a31);
  double a22inv = determinv * (a11*a33 - a13*a31);
  double a23inv = -determinv * (a11*a23 - a13*a21);
  double a31inv = determinv * (a21*a32 - a22*a31);
  double a32inv = -determinv * (a11*a32 - a12*a31);
  double a33inv = determinv * (a11*a22 - a12*a21);

  // quadratic correction coeffs

  double r0102 = (r01[0]*r02[0] + r01[1]*r02[1] + r01[2]*r02[2]);
  double r0112 = (r01[0]*r12[0] + r01[1]*r12[1] + r01[2]*r12[2]);
  double r0212 = (r02[0]*r12[0] + r02[1]*r12[1] + r02[2]*r12[2]);

  double quad1_0101 = (invmass0+invmass1)*(invmass0+invmass1) * r01sq;
  double quad1_0202 = invmass0*invmass0 * r02sq;
  double quad1_1212 = invmass1*invmass1 * r12sq;
  double quad1_0102 = 2.0 * (invmass0+invmass1)*invmass0 * r0102;
  double quad1_0112 = - 2.0 * (invmass0+invmass1)*invmass1 * r0112;
  double quad1_0212 = - 2.0 * invmass0*invmass1 * r0212;

  double quad2_0101 = invmass0*invmass0 * r01sq;
  double quad2_0202 = (invmass0+invmass2)*(invmass0+invmass2) * r02sq;
  double quad2_1212 = invmass2*invmass2 * r12sq;
  double quad2_0102 = 2.0 * (invmass0+invmass2)*invmass0 * r0102;
  double quad2_0112 = 2.0 * invmass0*invmass2 * r0112;
  double quad2_0212 = 2.0 * (invmass0+invmass2)*invmass2 * r0212;

  double quad3_0101 = invmass1*invmass1 * r01sq;
  double quad3_0202 = invmass2*invmass2 * r02sq;
  double quad3_1212 = (invmass1+invmass2)*(invmass1+invmass2) * r12sq;
  double quad3_0102 = - 2.0 * invmass1*invmass2 * r0102;
  double quad3_0112 = - 2.0 * (invmass1+invmass2)*invmass1 * r0112;
  double quad3_0212 = 2.0 * (invmass1+invmass2)*invmass2 * r0212;

  // iterate until converged

  double lamda01 = 0.0;
  double lamda02 = 0.0;
  double lamda12 = 0.0;
  int niter = 0;
  int done = 0;

  double quad1,quad2,quad3,b1,b2,b3,lamda01_new,lamda02_new,lamda12_new;

  while (!done && niter < max_iter) {

    quad1 = quad1_0101 * lamda01*lamda01 +
      quad1_0202 * lamda02*lamda02 +
      quad1_1212 * lamda12*lamda12 +
      quad1_0102 * lamda01*lamda02 +
      quad1_0112 * lamda01*lamda12 +
      quad1_0212 * lamda02*lamda12;

    quad2 = quad2_0101 * lamda01*lamda01 +
      quad2_0202 * lamda02*lamda02 +
      quad2_1212 * lamda12*lamda12 +
      quad2_0102 * lamda01*lamda02 +
      quad2_0112 * lamda01*lamda12 +
      quad2_0212 * lamda02*lamda12;

    quad3 = quad3_0101 * lamda01*lamda01 +
      quad3_0202 * lamda02*lamda02 +
      quad3_1212 * lamda12*lamda12 +
      quad3_0102 * lamda01*lamda02 +
      quad3_0112 * lamda01*lamda12 +
      quad3_0212 * lamda02*lamda12;

    b1 = bond1*bond1 - s01sq - quad1;
    b2 = bond2*bond2 - s02sq - quad2;
    b3 = bond12*bond12 - s12sq - quad3;

    lamda01_new = a11inv*b1 + a12inv*b2 + a13inv*b3;
    lamda02_new = a21inv*b1 + a22inv*b2 + a23inv*b3;
    lamda12_new = a31inv*b1 + a32inv*b2 + a33inv*b3;

    done = 1;
    if (fabs(lamda01_new-lamda01) > tolerance) done = 0;
    if (fabs(lamda02_new-lamda02) > tolerance) done = 0;
    if (fabs(lamda12_new-lamda12) > tolerance) done = 0;

    lamda01 = lamda01_new;
    lamda02 = lamda02_new;
    lamda12 = lamda12_new;

    // stop iterations before we have a floating point overflow
    // max double is < 1.0e308, so 1e150 is a reasonable cutoff

    if (fabs(lamda01) > 1e150 || fabs(lamda02) > 1e150
        || fabs(lamda12) > 1e150) done = 1;

    niter++;
  }

  // update forces if atom is owned by this processor

  lamda01 = lamda01/dtfsq;
  lamda02 = lamda02/dtfsq;
  lamda12 = lamda12/dtfsq;

  if (i0 < nlocal) {
    frc[i0].x += lamda01*r01[0] + lamda02*r02[0];
    frc[i0].y += lamda01*r01[1] + lamda02*r02[1];
    frc[i0].z += lamda01*r01[2] + lamda02*r02[2];
  }

  if (i1 < nlocal) {
    frc[i1].x -= lamda01*r01[0] - lamda12*r12[0];
    frc[i1].y -= lamda01*r01[1] - lamda12*r12[1];
    frc[i1].z -= lamda01*r01[2] - lamda12*r12[2];
  }

  if (i2 < nlocal) {
    frc[i2].x -= lamda02*r02[0] + lamda12*r12[0];
    frc[i2].y -= lamda02*r02[1] + lamda12*r12[1];
    frc[i2].z -= lamda02*r02[2] + lamda12*r12[2];
  }

  if (evflag) {
    nlist = 0;
    if (i0 < nlocal) list[nlist++] = i0;
    if (i1 < nlocal) list[nlist++] = i1;
    if (i2 < nlocal) list[nlist++] = i2;

    v[0] = lamda01*r01[0]*r01[0]+lamda02*r02[0]*r02[0]+lamda12*r12[0]*r12[0];
    v[1] = lamda01*r01[1]*r01[1]+lamda02*r02[1]*r02[1]+lamda12*r12[1]*r12[1];
    v[2] = lamda01*r01[2]*r01[2]+lamda02*r02[2]*r02[2]+lamda12*r12[2]*r12[2];
    v[3] = lamda01*r01[0]*r01[1]+lamda02*r02[0]*r02[1]+lamda12*r12[0]*r12[1];
    v[4] = lamda01*r01[0]*r01[2]+lamda02*r02[0]*r02[2]+lamda12*r12[0]*r12[2];
    v[5] = lamda01*r01[1]*r01[2]+lamda02*r02[1]*r02[2]+lamda12*r12[1]*r12[2];

    thr->v_tally_thr( vflag_global, vflag_atom, nlist, list, 3.0, v );
  }
}

/* ----------------------------------------------------------------------
   add coordinate constraining forces
   this method is called at the end of a timestep
------------------------------------------------------------------------- */

void FixShakeOMPSS::shake_end_of_step(int vflag) {
  if (!respa) {
    dtv     = update->dt;
    dtfsq   = 0.5 * update->dt * update->dt * force->ftm2v;
    FixShakeOMPSS::post_force(vflag);
    if (!rattle) dtfsq = update->dt * update->dt * force->ftm2v;

  } else {
    dtv = step_respa[0];
    dtf_innerhalf = 0.5 * step_respa[0] * force->ftm2v;
    dtf_inner = dtf_innerhalf;

    // apply correction to all rRESPA levels

    for (int ilevel = 0; ilevel < nlevels_respa; ilevel++) {
      ((Respa *) update->integrate)->copy_flevel_f(ilevel);
      FixShakeOMPSS::post_force_respa(vflag,ilevel,loop_respa[ilevel]-1);
      ((Respa *) update->integrate)->copy_f_flevel(ilevel);
    }
    if (!rattle) dtf_inner = step_respa[0] * force->ftm2v;
  }
}

/* ----------------------------------------------------------------------
   calculate constraining forces based on the current configuration
   change coordinates
------------------------------------------------------------------------- */

void FixShakeOMPSS::correct_coordinates(int vflag) {

  // save current forces and velocities so that you
  // initialise them to zero such that FixShake::unconstrained_coordinate_update has no effect
  int ntask = comm->nworkers;
  int quo = nlocal/ntask;
  int rem = nlocal%ntask;
# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) shared(ntask,quo,rem)
# elif _OMPSS == 2
# pragma oss task for default(none) shared(ntask,quo,rem)
# endif
  for (int t=0; t< ntask; t++) {
    int ini = t*quo + ( t<rem ? t : rem );
    int end = ini + quo + ( t<rem ? 1 : 0 );
    for (int j=ini; j<end; j++) {
      ftmp[j][0] = f[j][0]; f[j][0] = 0.0;
      ftmp[j][1] = f[j][1]; f[j][1] = 0.0;
      ftmp[j][2] = f[j][2]; f[j][2] = 0.0;
      vtmp[j][0] = v[j][0]; v[j][0] = 0.0;
      vtmp[j][1] = v[j][1]; v[j][1] = 0.0;
      vtmp[j][2] = v[j][2]; v[j][2] = 0.0;
    }
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif

  // call SHAKE to correct the coordinates which were updated without constraints
  // IMPORTANT: use 1 as argument and thereby enforce velocity Verlet

  dtfsq   = 0.5 * update->dt * update->dt * force->ftm2v;
  post_force(vflag);

  // integrate coordiantes: x' = xnp1 + dt^2/2m_i * f, where f is the constraining force
  // NOTE: After this command, the coordinates geometry of the molecules will be correct!

  double dtfmsq;
  if (rmass) {
    for (int i = 0; i < nlocal; i++) {
      dtfmsq = dtfsq/ rmass[i];
      x[i][0] = x[i][0] + dtfmsq*f[i][0];
      x[i][1] = x[i][1] + dtfmsq*f[i][1];
      x[i][2] = x[i][2] + dtfmsq*f[i][2];
    }
  }
  else {
    for (int i = 0; i < nlocal; i++) {
      dtfmsq = dtfsq / mass[type[i]];
      x[i][0] = x[i][0] + dtfmsq*f[i][0];
      x[i][1] = x[i][1] + dtfmsq*f[i][1];
      x[i][2] = x[i][2] + dtfmsq*f[i][2];
    }
  }

  // copy forces and velocities back

  for (int j=0; j<nlocal; j++) {
    for (int k=0; k<3; k++) {
      f[j][k] = ftmp[j][k];
      v[j][k] = vtmp[j][k];
    }
  }

  if (!rattle) dtfsq = update->dt * update->dt * force->ftm2v;

  // communicate changes
  // NOTE: for compatibility xshake is temporarily set to x, such that pack/unpack_forward
  //       can be used for communicating the coordinates.

  double **xtmp = xshake;
  xshake = x;
  if (nprocs > 1) {
    comm->forward_comm_fix(this);
  }
  xshake = xtmp;
}

void FixShakeOMPSS::v_setup(int vflag)
{
  int i,n;

  if (!thermo_virial) {
    evflag = 0;
    return;
  }

  evflag = 1;

  vflag_global = vflag % 4;
  vflag_atom = vflag / 4;

  // reallocate per-atom array if necessary

  if (vflag_atom && atom->nlocal > maxvatom) {
    maxvatom = atom->nmax;
    memory->destroy(vatom);
    if ( ompss->shared_mem ) memory->create( vatom, maxvatom, 6, "fix:vatom" );
    else memory->create( vatom, maxvatom*comm->nworkers, 6, "fix:vatom" );
  }

  // zero accumulators

  if (vflag_global) for (i = 0; i < 6; i++) virial[i] = 0.0;
  if (vflag_atom) {
    reset_arr_thr( comm->nworkers, vatom[0], atom->nlocal*6, ompss->shared_mem ? 0 : maxvatom*6 );
  }
}
